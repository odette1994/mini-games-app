﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Carousel : MonoBehaviour {

	ScrollRect panelView;
	public GameObject itemPrefab;
	//private GameObject scrollIns;
	public int itemCount = 0;
	public int columnCount = 1;

	void Start () 
	{
		RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
		RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();

		float width = containerRectTransform.rect.width / columnCount;
		float ratio = width / rowRectTransform.rect.width;
		float height = rowRectTransform.rect.height * ratio;

		//scrollIns = GameObject.Find("scrollCell");
		float scrollWid = itemCount * containerRectTransform.rect.width;

		containerRectTransform.sizeDelta = new Vector2 (scrollWid, height);

//		for (int i = 0; i < 3; i++) {
//			RectTransform rectIns = gameIns.GetComponent<RectTransform> ();
//			GameObject objIns = Instantiate(gameIns, new Vector2(i * rectIns.rect.width, 0), Quaternion.identity) as GameObject;
//			objIns.transform.SetParent(panelView.transform ,false);
//		}
//		scrollIns.transform.SetParent (panelView.transform ,false);
//		scrollIns.transform.GetComponent<RectTransform>().localPosition = new Vector2 (0, 0);
//		scrollIns.transform.GetComponent<RectTransform> ().sizeDelta = new Vector2 (100, 100);

		//Debug.Log (panelView);
		//Debug.Log (scrollIns);




	}

}
