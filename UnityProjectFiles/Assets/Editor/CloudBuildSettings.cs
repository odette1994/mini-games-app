﻿using UnityEditor;
using UnityEditor.Callbacks;
using UnityEditor.iOS.Xcode;
using System.IO;
using UnityEngine;

#if UNITY_IOS
using UnityEditor.iOS.Xcode;
#endif

public class CloudBuildSettings : ScriptableObject
{
	private static int _buildNumberDetail = 0;

#if UNITY_CLOUD_BUILD
    public static void PreExport(UnityEngine.CloudBuild.BuildManifestObject manifest)
	{
		Debug.Log("[UCB] PreExport");
		_buildNumberDetail = int.Parse(manifest.GetValue("buildNumber", "0"));
		string buildIdentifier = $"{_buildNumberDetail}";
#if UNITY_IOS
		PlayerSettings.iOS.buildNumber = buildIdentifier;
#elif UNITY_ANDROID
		PlayerSettings.Android.bundleVersionCode = _buildNumberDetail; 
#endif
		PlayerSettings.bundleVersion = buildIdentifier;
    }

    [PostProcessBuild(9999)]
    public static void OnPostProcessBuild(BuildTarget target, string pathToBuiltProject)
    {
        if (target == BuildTarget.iOS)
            RemoveArm64AndMetalFromRequiredDeviceCapabilities(pathToBuiltProject);
    }

    private static void RemoveArm64AndMetalFromRequiredDeviceCapabilities(string pathToBuiltProject)
    {
        string plistPath = Path.Combine(pathToBuiltProject, "Info.plist");
        var plist = new PlistDocument();
        plist.ReadFromString(File.ReadAllText(plistPath));

        var rootDict = plist.root;

        var capabilities = rootDict["UIRequiredDeviceCapabilities"].AsArray();
        capabilities.values.RemoveAll(item => item.AsString() == "arm64");
        capabilities.values.RemoveAll(item => item.AsString() == "metal");

        File.WriteAllText(plistPath, plist.WriteToString());
    }
#endif
}
