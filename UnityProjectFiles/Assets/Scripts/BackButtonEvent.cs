﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BackButtonEvent : MonoBehaviour {

	public GameObject trivia;
	public GameObject memory;
	public GameObject qModal;
	public GameObject loading;
	public GameObject helpModal;

	GameObject quitModal;

	void Awake () {
		quitModal = GameObject.Find ("Quit") as GameObject;
		OnShow (quitModal, false);
		//OnShow (helpModal, false);
	}

	void Update () {

	#if UNITY_ANDROID
		if (Input.GetKeyDown(KeyCode.Escape))
			Close();
	#endif

	}

	private void Close() {

		if (trivia.activeInHierarchy || memory.activeInHierarchy) {

			if (helpModal.activeInHierarchy)
				OnShow (helpModal, false);

			OnShow (qModal, true);

		} else {

			OnShow (quitModal, true);


			GameObject yesGO = GameObject.Find ("Quit/Window/btnYes") as GameObject;
			Button yesButton = yesGO.GetComponent<Button> ();
			yesButton.onClick.RemoveAllListeners ();

			yesButton.onClick.AddListener (delegate {

				StartCoroutine(WaitBeforeQuit());

			});

		}
	}

	IEnumerator WaitBeforeQuit() {
		OnShow (loading, true);
		yield return new WaitForSeconds (2);
		Input.backButtonLeavesApp = true;
		Application.Quit();
	}
		

	private void OnShow(GameObject _canvas, bool show) {
		_canvas.SetActive (show);
	}

	public void ShowQuit(bool isShown) {
		quitModal.SetActive (isShown);
	}
}
