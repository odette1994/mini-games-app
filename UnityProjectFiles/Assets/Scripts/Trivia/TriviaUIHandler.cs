﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class TriviaUIHandler : MonoBehaviour
{
    //Singleton
    private static TriviaUIHandler instance;

    //Construct
    private TriviaUIHandler() { }

    //Variables
    [SerializeField] List<Canvas> _canvas;
    Enum.TriviaScreenUI _screenShowing;
    [SerializeField] Canvas _modal;
    Canvas _gameOver;
    public AnimationController anim;

    public static TriviaUIHandler Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType(typeof(TriviaUIHandler)) as TriviaUIHandler;

            return instance;
        }
    }

    // Use this for initialization
    void Awake()
    {
        _screenShowing = Enum.TriviaScreenUI.MainGameUI;

        //Hide all canvases aside from MainMenuUI
        for (int i = 0; i < _canvas.Count; i++)
        {
            if (i > 0)
                _canvas[i].gameObject.SetActive(false);
        }
    }

    public void ShowCanvas(Enum.TriviaScreenUI canvasEnum)
    {
        if (_screenShowing != canvasEnum)
        {
            _canvas[(int)canvasEnum].gameObject.SetActive(true);

            _canvas[(int)_screenShowing].gameObject.SetActive(false);
            _screenShowing = canvasEnum;
        }
    }

    public void ShowModal(bool show)
    {
        if (!show)
            anim.OnAnimatorTrigger();

        Analytics.CustomEvent("CloseClick", new Dictionary<string, object> {
            { "Game", "Trivia Game" }
        });
        MyAnalytics.Instance.LogEvent("CloseClick", "Game", "Trivia Game", 1);
    }
}