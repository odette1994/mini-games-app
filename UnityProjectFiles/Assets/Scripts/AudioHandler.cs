﻿using UnityEngine;
using System.Collections;

public class AudioHandler : MonoBehaviour 
{

	public bool isMute;
	public AudioClip[] menuSounds;
	public AudioClip tap_button;
	public AudioClip popup;

	public AudioSource source;
	private AudioSource sfx;
	private AudioClip mainMenuMusic;
	private string filename = "SFX/bgMusic";

	void Awake () 
	{
		SetMusic ();
		PlayMainMenuMusic (true);
		isMute = false;
	}

	void SetMusic()
	{
		source = gameObject.AddComponent<AudioSource> ();
		sfx = gameObject.AddComponent<AudioSource> ();
		source.playOnAwake = false;
		source.loop = true;
		source.mute = isMute;
		source.volume = 1.0f;

		AudioListener.volume = 1.0f;

		mainMenuMusic =  Resources.Load (filename) as AudioClip;
	}

	void Update() {
		
	}

	public void SetMute(bool mute) 
	{
		
		if (source != null) 
		{
			source.mute = mute;
			isMute = mute;
			PlayerPrefs.SetString ("mute",mute.ToString());

		}

		//settings.ShowAllButton ();

	}

	public bool ConvertToBool(string val)
	{

		bool str = false;

		if(val != null)
			str = (val.Equals("True")) ? true : false;

		return str;
	}

	public void PlayMainMenuMusic(bool play) 
	{
		bool mute = ConvertToBool (PlayerPrefs.GetString("mute"));

		if (source != null) 
		{
			source.clip = mainMenuMusic;
			source.mute = mute;

			if (play)
				source.Play ();
			else
				source.Stop ();
		}

	}

	public void SetAudioLevel(float level) {

		if (source != null) 
		{
			source.volume = level;
			SaveAudioSetting (level);
		}


	}

	void SaveAudioSetting(float volumeLevel) {
		PlayerPrefs.SetFloat ("volume", volumeLevel);
	}

	public void TapMenuButton(int index) {
		source.PlayOneShot (menuSounds[index],1f);
	}

	public void TapButton() {
		sfx.PlayOneShot (tap_button,1f);
	}

	public void ShowPopup() {
		sfx.PlayOneShot (popup, 1f);
	}
}
