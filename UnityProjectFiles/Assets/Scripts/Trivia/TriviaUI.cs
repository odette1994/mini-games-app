﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TriviaUI : MonoBehaviour {

	TriviaUIHandler _uiHandler;

	[HideInInspector]public GameObject crown;
	[HideInInspector] public Text _timer;

	void Start () {

		_uiHandler = TriviaUIHandler.Instance;
	}

	public void ShowScreen(string button) {

		Enum.TriviaScreenUI screenToShow;

		switch(button){

		case "Play":
			screenToShow = Enum.TriviaScreenUI.MainGameUI;
			break;

		case "GameOver":
			screenToShow = Enum.TriviaScreenUI.GameOverUI;
			break;

		default:
			screenToShow = Enum.TriviaScreenUI.MainGameUI;
			break;

		}

		_uiHandler.ShowCanvas (screenToShow);

	}

	public void ShowCrown(bool show) {
		crown.SetActive (show);
	}

	public void ResetScore() {
		PlayerPrefs.SetInt ("earned_score",0);
	}

}
