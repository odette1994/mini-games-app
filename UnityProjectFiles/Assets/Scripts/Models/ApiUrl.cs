﻿using System;

namespace Models
{
	public class ApiUrl
	{
		public const string BASE = "http://list.theswanprincess.net";
		public const string BASE_URL = BASE + "/api/";
//		public const string BASE_URL = "http://swan.local/api/";
		public const string TRIVIA = BASE_URL + "trivia";
		public const string EMAIL = BASE_URL + "email/send-invite";
		public const string SHOP_URL = BASE_URL + "stores";
		public const string EMAIL_MESSAGE = BASE_URL + "settings/email-quote";
		public const string PUZZLE_MOVIE_DETAILS = BASE_URL + "puzzles/movie-details";
		public const string PUZZLE_QUESTION_NEXT = BASE_URL + "puzzles/next";

		public ApiUrl ()
		{
		}
	}
}

