﻿using UnityEngine;
using System.Collections;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;

namespace Models.Trivia
{
	[Serializable]
	public class AnswerObj  {

		public string answer;
		public bool isCorrect;
		public string audioUrl;
		public string audioFilename;
	}
}

