﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MainMenuUI : MonoBehaviour {

	UIHandler _uiHandler;


	// Use this for initialization
	void Start () {
		_uiHandler = UIHandler.Instance;
		Debug.Log("I've just been clicked");
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void OnButtonClick(GameObject button){
		Enum.ScreenUI screenToShow;

		switch (button.name) {

		//NOTE:
		//Add other canvases here

		case "BtnCharacters":

			screenToShow = Enum.ScreenUI.CharactersUI;
			break;
		case "BtnShare":
			screenToShow = Enum.ScreenUI.ShareUI;
			break;
		case "BtnPlaytime":
			screenToShow = Enum.ScreenUI.PlaytimeUI;
			break;
		default:
			screenToShow = Enum.ScreenUI.MainMenuUI;
			break;
		}
		_uiHandler.ShowCanvas (screenToShow);
		Debug.Log("I've just been clicked");
		//GameObject.Find (button.name).GetComponent<Image> ().sprite =  Resources.Load("menu_home_active", typeof(Sprite)) as Sprite;

	
	}
	public void OnTransitionScreen(GameObject button){

		switch (button.name) {
		case "BtnShare":
			//Application.LoadLevel ("ShareUI");
			break;
		default:
			break;
		}
	}
}
