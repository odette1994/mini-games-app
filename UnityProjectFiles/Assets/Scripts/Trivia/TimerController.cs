﻿using UnityEngine;
using System.Collections;

public class TimerController : MonoBehaviour {

	//Singleton
	private static TimerController instance;

	//Construct
	private TimerController(){}

	//Variables
	public int timeInSeconds = 5; //Update value
	bool _hasStarted; 

	//Delegates
	public delegate void Timer(string time);
	public event Timer OnTimeUpdate;
	public event Timer OnTimeUp;

	public static TimerController Instance
	{
		get{
			if(instance == null)
				instance = GameObject.FindObjectOfType(typeof(TimerController)) as TimerController;

			return instance;
		}

	}

	// Use this for initialization
	void Awake () {
		DontDestroyOnLoad(this);
		_hasStarted = false;
	}
	
	public void ResetTime()
	{
		StopTime ();
		timeInSeconds = 300;
		_hasStarted = false;
	}

	public void StartTime()
	{
		if (!_hasStarted) 
		{
			InvokeRepeating ("DecrementTime", 1.0f, 1.0f);
			_hasStarted = true;
		}
	}

	public void StopTime()
	{
		CancelInvoke("DecrementTime");
		_hasStarted = false;
	}

	//private methods
	void DecrementTime()
	{
		timeInSeconds--;

		if (timeInSeconds < 0) {
			OnTimeUp ("Time's Up!");
			StopTime ();
		}
		else
			OnTimeUpdate(GetTimeByMinute());
	}

	string GetTimeByMinute()
	{
		int min = timeInSeconds/60;
		int sec = timeInSeconds%60;

		if (min<0)
			min = 0;

		if (sec<0)
			sec = 0;

		string minStr = min.ToString();
		string secStr = sec.ToString();

		if(min<10)
			minStr = "0"+minStr;

		if(sec<10)
			secStr = "0"+secStr;

		string timeStr = minStr + ":" + secStr;

		return timeStr;
	}

	public string GetTime
	{
		get{return GetTimeByMinute();}
	}

	public bool HasStarted
	{
		get{return _hasStarted;}
	}
}
