﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Net;
using Models;
using System.Collections.Generic;
using UnityEngine.Analytics;

public class VideoUI : MonoBehaviour {

	//private const string url = "http://45.55.16.169/uploaded/videos/";

	public ModalManager _modal;

	Button btniTunes, btnAmazon;
	GameObject market;

	private string ITUNES;
	private string AMAZON;

	string sStore;

	GameObject iTunesIcon;
	GameObject amazonIcon;

	void Start () {
		
	}



//	public void PlayVideo(string num) {
//
//		#if UNITY_IOS
//			if(isPlayed) {
//				SetVideo(num);
//			}
//		#endif
//
//		#if UNITY_ANDROID
//			if(isPlayed) {
//				SetVideo(num);
//			}
//		#endif
//
//	}
//
//	public void StreamVideo() {
//
//		if(isPlayed)
//			Video();
//	}
//
//	private void SetVideo(string vidNum) {
//		Handheld.PlayFullScreenMovie("vids/swan_" + vidNum + ".mp4",Color.black,FullScreenMovieControlMode.Minimal);
//
//		Debug.Log ("Starting playing: swan_"+vidNum);
//	}
//
//	private void Video() {
//		Handheld.PlayFullScreenMovie("http://swanprincessgameapp.s3.amazonaws.com/SwanTransforms.mp4",Color.black,FullScreenMovieControlMode.Minimal);
//		//Application.OpenURL("http://swanprincessseries.com/products/swan-princess-movie-two");
//
//	}

	public void SetURL (string url) {

		if (url.Contains ("itunes")) {
			this.ITUNES = url;
		} else if (url.Contains ("amazon")) {
			this.AMAZON = url;
		} else if (url.Contains("iTunes - No link")){
			this.ITUNES = url;
		} else if (url.Contains("Amazon - No link")){
			this.AMAZON = url;
		}
			
	}

	public void OnClickYes () {
		_modal.NextScreen ();
	}

	public void CheckURL() {

		iTunesIcon = GameObject.Find ("ConfirmModal/Panel/ScrollPanel/Market/iTunes");
		amazonIcon = GameObject.Find ("ConfirmModal/Panel/ScrollPanel/Market/Amazon");

		if (ITUNES == "iTunes - No link") {
			iTunesIcon.SetActive (false);
			amazonIcon.transform.localPosition = new Vector3 (-262f, 186f, 0);
		} else {
			iTunesIcon.SetActive (true);
			iTunesIcon.transform.localPosition = new Vector3 (-262f, 186f, 0);
			amazonIcon.transform.localPosition = new Vector3 (-268f, -129f, 0);
		}

		if (AMAZON == "Amazon - No link") {
			amazonIcon.SetActive (false);
		} else {
			amazonIcon.SetActive (true);
		}

		#if UNITY_ANDROID
		amazonIcon.SetActive (true);
		#elif UNITY_IOS
		amazonIcon.SetActive (false);
		#endif
	}


	public void OnOpenURL() {

		market = GameObject.Find ("ConfirmModal");
		btniTunes = GameObject.Find ("ConfirmModal/Panel/ScrollPanel/Market/iTunes").GetComponent<Button> ();
		btnAmazon = GameObject.Find ("ConfirmModal/Panel/ScrollPanel/Market/Amazon").GetComponent<Button> ();

		btniTunes.onClick.RemoveAllListeners ();
		btnAmazon.onClick.RemoveAllListeners ();

		btniTunes.onClick.AddListener (delegate {

			sStore = "iTunes";
			/* Analytics For Games
                     * Game: Which game screen am I?
                     */
			Analytics.CustomEvent("Buy iTunes", new Dictionary<string, object> {
				{ "Home Store", sStore }
			}); ;

			MyAnalytics.Instance.LogEvent("Buy iTunes", "Home Store", "iTunes", 1);

			#if UNITY_ANDROID
				ITUNES = "https://" + ITUNES;
#elif UNITY_IOS
				ITUNES = "itms://" + ITUNES;
#endif


			if (CheckForInternetConnection (ApiUrl.BASE)) {
				Application.OpenURL (ITUNES);
			} else {
				UIHandler.Instance.ShowModalNoInternet (true);
			}

			market.SetActive (false);
			_modal.PreviousScreen ();
		});

		btnAmazon.onClick.AddListener (delegate {

			sStore = "Amazon";
			/* Analytics For Games
                     * Game: Which game screen am I?
                     */
			Analytics.CustomEvent ("Buy Amazon", new Dictionary<string, object> {
				{ "Home Store", sStore }
			});

			MyAnalytics.Instance.LogEvent("Buy Amazon", "Home Store", "Amazon", 1);

			if (CheckForInternetConnection (ApiUrl.BASE)) {
				Application.OpenURL (AMAZON);
			} else {
				UIHandler.Instance.ShowModalNoInternet (true);
			}

			market.SetActive (false);
			_modal.PreviousScreen ();
		});
			
//		_modal.PreviousScreen ();
//		_modal.PreviousScreen ();

		
	}

	public void ShowWeb(string url) {
		if (CheckForInternetConnection (ApiUrl.BASE)) {
			Application.OpenURL (url);
		} else {
			UIHandler.Instance.ShowModalNoInternet (true);
		}
	}

	public static bool CheckForInternetConnection(string url)
	{
		try
		{
			using (var client = new WebClient())
			{
				using (var stream = client.OpenRead(url))
				{
					return true;
				}
			}
		}
		catch
		{
			return false;
		}
	}


}
