﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Models;

public class PuzzleUI : MonoBehaviour {

	const string B_PRINCESS = "PRINCESS APPRENTICE";
	const string B_JEANBOB = "JEANBOB";
	const string B_SPEED = "SPEED";
	const string B_ROTHBART = "ROTHBART";

	[HideInInspector]public ScrollRectScript puzzleHelp;
	[HideInInspector]public GameObject skipText;
	[HideInInspector]public Button btnStartPlaying;
	[HideInInspector]public Button btnTextStartPlaying;

	GameObject modal;
	GameObject helpPuzzleFirstTime;

	RectTransform container;

	int pageCount;

	List<Image> achievement;
	Text badge;
	Image locked;
	GameObject noBadge;
	GameObject withBadge;

	Button btnHelp;

	private List<Models.Puzzles.Question> listQuestions;
	
	void Start () {
		modal = GameObject.Find ("QuitModal");
		container = GameObject.Find ("Selection").GetComponent<ScrollRect> ().content;

		btnHelp = GameObject.Find ("Selection/btnHelp").GetComponent<Button> ();

		helpPuzzleFirstTime = GameObject.Find ("HelpPuzzleFirstTime");

		ShowObject (skipText, false);

		// show loading
		UIHandler.Instance.ShowModalLoading (true);

		// implement the checking of what puzzle are unlock or lock
		Debug.Log( Application.persistentDataPath);
		string puzzleQuestionPath = Application.persistentDataPath + FileName.PUZZLE_QUESTIONS;
		Models.Puzzles.Question question;

		listQuestions = new List<Models.Puzzles.Question> (); 
		// if the file already exist
		if (File.Exists (puzzleQuestionPath)) {
			
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Open (puzzleQuestionPath, FileMode.Open);

			while (file.Position != file.Length) {
				listQuestions.Add ((Models.Puzzles.Question)bf.Deserialize (file));
			}

			file.Close ();

			if (listQuestions.Count >= 3 && listQuestions [listQuestions.Count - 1].GetIsUnlock ()) {
				StartCoroutine (GetNextPuzzle (puzzleQuestionPath, listQuestions [listQuestions.Count - 1].GetId ()));
			} else {
				// hide loading

				UIHandler.Instance.ShowModalLoading (false);
				GenerateItems ();
			}
				
		} 
		// if the file not exist we create default
		else {
			BinaryFormatter bf = new BinaryFormatter ();
			FileStream file = File.Create (puzzleQuestionPath);

			// default puzz 1
			JSONObject puzz1 = new JSONObject ();
			puzz1.AddField ("id", 0);
			puzz1.AddField ("is_unlock", true);
			puzz1.AddField ("badge", "0");
			puzz1.AddField ("grid", "2x3");
			puzz1.AddField ("image_path", "puzzle/puzzleimage1");
			puzz1.AddField ("row", "3");
			puzz1.AddField ("column", "2");
			puzz1.AddField ("answer", "Alise");
			puzz1.AddField ("question", "Who climbs to the top of the pirate ship");
			puzz1.AddField ("audio_path", "");
			puzz1.AddField ("first_badge_message", "Congrats!");
			puzz1.AddField ("second_badge_message", "Wow!");
			puzz1.AddField ("third_badge_message", "Good job!");
			puzz1.AddField ("fourth_badge_message", "So close.");
			puzz1.AddField ("answer_audio_path", "");

			Models.Puzzles.Question puzzleQbj = new Models.Puzzles.Question (puzz1);
			bf.Serialize (file, puzzleQbj);
			listQuestions.Add (puzzleQbj);

			// default puzz 2
			JSONObject puzz2 = new JSONObject ();
			puzz2.AddField ("id", 0);
			puzz2.AddField ("is_unlock", false);
			puzz2.AddField ("badge", "0");
			puzz2.AddField ("grid", "3x4");
			puzz2.AddField ("image_path", "puzzle/puzzleimage2");
			puzz2.AddField ("row", "4");
			puzz2.AddField ("column", "3");
			puzz2.AddField ("answer", "Princess Odette");
			puzz2.AddField ("question", "Who does the swan turned into at night");
			puzz2.AddField ("audio_path", "");
			puzz2.AddField ("first_badge_message", "Congrats!");
			puzz2.AddField ("second_badge_message", "Wow!");
			puzz2.AddField ("third_badge_message", "Good job!");
			puzz2.AddField ("fourth_badge_message", "So close.");
			puzz2.AddField ("answer_audio_path", "");

			Models.Puzzles.Question puzzleQbj2 = new Models.Puzzles.Question (puzz2);
			bf.Serialize (file, puzzleQbj2);
			listQuestions.Add (puzzleQbj2);

			file.Close ();

			// save the downloaded puzzle
//			StartCoroutine (GetNextPuzzle(puzzleQuestionPath, 0));

			UIHandler.Instance.ShowModalLoading (false);
			GenerateItems ();
		}

		// game setup
		pageCount = container.childCount;

		PlayerPrefs.SetInt ("puzzleCount", pageCount);
		PlayerPrefs.SetInt ("allowedToPlay", 1);

		btnHelp.onClick.AddListener (delegate {
			UIHandler.Instance.ShowHelpPuzzleModal (true);
		});

	}

	// Update is called once per frame
	void Update () {
		pageCount = puzzleHelp.GetCurrentPage ();
	}

	/**
	 * Get the next puzzle from the API
	 * 
	 */
	private IEnumerator GetNextPuzzle(string puzzleQuestionPath, float currentId) {
		string url = "";

		url = ApiUrl.PUZZLE_QUESTION_NEXT + "?current_id=" + currentId;

		WWW www = new WWW (url);
		yield return www;

		JSONObject jsonObject = new JSONObject (www.text);

		if (!jsonObject.IsNull) {
			if (jsonObject.GetField("results").Count >= 1) {
				Models.Puzzles.Puzzle puzzle = new Models.Puzzles.Puzzle (jsonObject.GetField("results"));

				// get image
				string imgUrl = Application.persistentDataPath + "/" + puzzle.GetImageFilename ();
				if (!File.Exists(imgUrl)) {
					WWW imgWWW = new WWW (WWW.UnEscapeURL(puzzle.GetImageUrl()));
					yield return imgWWW;

					File.WriteAllBytes(imgUrl, imgWWW.bytes);

					yield return new WaitUntil (() => imgWWW.isDone == true);
				}

				// get audio
				string audioUrl = Application.persistentDataPath + "/" + puzzle.GetAudioFilename ();
				if (!File.Exists(audioUrl)) {
					WWW audioWWW = new WWW (WWW.UnEscapeURL(puzzle.GetAudioUrl()));
					yield return audioWWW;

					File.WriteAllBytes(audioUrl, audioWWW.bytes);

					yield return new WaitUntil (() => audioWWW.isDone == true);
				}

				// get answer audio
				string asnwerAudioUrl = Application.persistentDataPath + "/" + puzzle.GetAnswerAudioFilename();
				if (!File.Exists (asnwerAudioUrl)) {
					WWW answerAudioWWW = new WWW (WWW.UnEscapeURL (puzzle.GetAnswerAudioUrl ()));
					yield return answerAudioWWW;

					File.WriteAllBytes (asnwerAudioUrl, answerAudioWWW.bytes);

					yield return new WaitUntil (() => answerAudioWWW.isDone == true);
				}

				JSONObject question = new JSONObject ();
				question.AddField ("id", puzzle.GetId());
				question.AddField ("is_unlock", false);
				question.AddField ("badge", "0");
				question.AddField ("grid", puzzle.GetGridRow() + "x" + puzzle.GetGridColumn());
				question.AddField ("image_path", imgUrl);
				question.AddField ("row", puzzle.GetGridRow());
				question.AddField ("column", puzzle.GetGridColumn());
				question.AddField ("first_badge_time", puzzle.GetFirstBadgeTime());
				question.AddField ("second_badge_time", puzzle.GetSecondBadgeTime());
				question.AddField ("third_badge_time", puzzle.GetThirdBadgeTime());
				question.AddField ("fourth_badge_time", puzzle.GetFourthBadgeTime());
				question.AddField ("answer", puzzle.GetAnswer ());
				question.AddField ("question", puzzle.GetQuestion ());
				question.AddField ("audio_path", audioUrl);
				question.AddField ("first_badge_message", puzzle.GetFirstBadgeMessage());
				question.AddField ("second_badge_message", puzzle.GetSecondBadgeMessage());
				question.AddField ("third_badge_message", puzzle.GetThirdBadgeMessage());
				question.AddField ("fourth_badge_message", puzzle.GetFourthBadgeMessage());

				question.AddField ("answer_audio_path", asnwerAudioUrl);

				Models.Puzzles.Question questionObj = new Models.Puzzles.Question (question);
				listQuestions.Add (questionObj);

				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (puzzleQuestionPath, FileMode.Append);

				bf.Serialize (file, questionObj);
				file.Close();
				Debug.Log ("total " + listQuestions.Count);
			}

		}

		// hide loading
		UIHandler.Instance.ShowModalLoading (false);
		GenerateItems ();

	}

	/**
	 * Generate the game selection
	 * 
	 */ 
	private void GenerateItems() {
		// create the scrollrect script
		GameObject scrollRect = GameObject.Find ("PuzzleGameSelectionUI/Selection") as GameObject;
		ScrollRectScript scrollRectScript = scrollRect.AddComponent<ScrollRectScript> ();
		scrollRectScript.nextButton = GameObject.Find ("PuzzleGameSelectionUI/Selection/arrowRight") as GameObject;
		scrollRectScript.prevButton = GameObject.Find ("PuzzleGameSelectionUI/Selection/arrowLeft") as GameObject;

		for (int x = 2; x < listQuestions.Count; x++) {
			// copy the item
			GameObject copyItem = Instantiate(GameObject.Find("PuzzleGameSelectionUI/Selection/Container/Puzzle_1")) as GameObject;
			copyItem.name = "Puzzle_" + x;

			// change image
			Texture2D tex = null;
			byte[] fileData;
			string imagePath = listQuestions [x].GetImagePath ();
			if (File.Exists(imagePath))     {
				fileData = File.ReadAllBytes(imagePath);
				tex = new Texture2D(2, 2);
				tex.LoadImage(fileData);
			}

			Sprite sprite = Sprite.Create (tex, 
				new Rect (0, 0, tex.width, tex.height),
				new Vector2(0.5f, 0.5f));
			Image[] image = copyItem.GetComponentsInChildren<Image> ();
			for (int imageX = 0; imageX < image.Length; imageX++) {
				if (image[imageX].name.Contains("Image")) {
					image [imageX].sprite = sprite;
				} else if (image[imageX].name.Contains("LOCKED")) {
					image[imageX].sprite = sprite;
				}
			}


			copyItem.transform.SetParent (GameObject.Find ("PuzzleGameSelectionUI/Selection/Container").transform, false);
		}

		SelectImage ();
		ShowAchievements ();
	}

	void SelectImage () {
		List<Button> arrayButton = new List<Button> ();
		Button[] imgButton = new Button[listQuestions.Count];
		Button[] imgLocked = new Button[listQuestions.Count];
		Debug.Log (listQuestions.Count);
		for (int x = 0; x < listQuestions.Count; x++) {
			string puzzle_str = "Selection/Container/Puzzle_" + x + "/Image";
			imgButton[x] = GameObject.Find (puzzle_str).GetComponent<Button> ();

			string locked = "Selection/Container/Puzzle_" + x + "/LOCKED";
			imgLocked [x] = GameObject.Find (locked).GetComponent<Button> ();

			string str = imgButton[x].image.sprite.name;
			int imageIndex = x;

			arrayButton.Add (imgButton[x]);

			imgLocked [x].onClick.AddListener (delegate {
				UIHandler.Instance.ShowUnlockLevel (true);
			});

			arrayButton[x].onClick.AddListener (delegate {
				Debug.Log ("You clicked here " + str);

				if (PlayerPrefs.GetInt ("IsFirstTime") == 0) {
					PlayerPrefs.SetInt ("IsFirstTime", 1);

					StartCoroutine (ShowSkip ());

					btnStartPlaying.onClick.AddListener (delegate {
						CheckPlaying (imageIndex);
					});

					btnTextStartPlaying.onClick.AddListener (delegate {
						CheckPlaying (imageIndex);
					});

					UIHandler.Instance.HelpPuzzleModal ("first_time_player");

				} else {
					CheckPlaying (imageIndex);
				}
			

			});	
		}
	}

	public void ShowQuitModal (bool show) {
		modal.SetActive (show);
	}

	void ShowObject (GameObject obj, bool show) {
		obj.SetActive (show);
	}

	void ShowAchievements () {
		achievement = new List<Image> ();
		Image[] img = new Image[listQuestions.Count];
		Image[] b_active = new Image[4];
		Image[] b_inactive = new Image[4];
		GameObject[] badge_active = new GameObject[4];
		GameObject txtComment = null;
		Sprite _badge = null;

		int puzzle = -1;
		string b = "";

		for (int i = 0; i < listQuestions.Count; i++) {

//			if (PlayerPrefs.HasKey ("puzzle_index_" + i))
//				puzzle = PlayerPrefs.GetInt ("puzzle_index_" + i);
//
//			if (PlayerPrefs.HasKey ("badge_puzzle_" + i))
//				b = PlayerPrefs.GetInt ("badge_puzzle_" + i);


			string str = "Selection/Container/Puzzle_" + i + "/ACHIEVEMENTS";
			noBadge = GameObject.Find (str + "/no_badge_image");
			withBadge = GameObject.Find (str + "/with_badge");

			// get the badge in the questions
			string badgeString = listQuestions [i].GetBadge ();

			// show the comment
			txtComment = GameObject.Find (str + "/Comment");

//			Debug.Log (badgeString);

			// set star image
			for (int a = 3; a >= 0; a--) {
				b_active [a] = GameObject.Find (str + "/" + (a + 1) + "/Active").GetComponent<Image> ();
				b_active [a].gameObject.SetActive(false);

				badge_active [a] = GameObject.Find (str + "/with_badge/" + (a + 1));
				ShowObject (badge_active[a], false);
			}

			if (listQuestions [i].GetIsUnlock ()) {
				GameObject lockedGame = GameObject.Find ("Selection/Container/Puzzle_" + i + "/LOCKED");
				lockedGame.SetActive (false);

				img [i] = GameObject.Find (str).GetComponent<Image> ();

				string bdge = str + "/BADGE";
//				badge = GameObject.Find (bdge).GetComponent<Text> ();


				if (!badgeString.Equals ("0")) {
					img [i].gameObject.SetActive (true);
				}

				string desc = "", _badgeIndex = "";

				switch (badgeString) {
				case "1":
					desc = B_PRINCESS;
					_badge = Resources.LoadAll<Sprite> ("puzzle/puzzle_badge") [0];
					break;

				case "2":
					desc = B_JEANBOB;
					_badge = Resources.LoadAll<Sprite> ("puzzle/puzzle_badge") [1];
					break;

				case "3":
					desc = B_SPEED;
					_badge = Resources.LoadAll<Sprite> ("puzzle/puzzle_badge") [3];
					break;

				case "4":
					desc = B_ROTHBART;
					_badge = Resources.LoadAll<Sprite> ("puzzle/puzzle_badge") [2];
					break;

				default:
					desc = "";
					break;
				}

//				badge.text = desc;


				if (!badgeString.Equals ("0")) {

					ShowObject (badge_active[int.Parse (badgeString) - 1], true);

					ShowObject (withBadge, true);

					for (int a = 3; a >= int.Parse (listQuestions [i].GetBadge ()) - 1; a--) {
						b_active [a].gameObject.SetActive(true);
					}
				} else {
					ShowObject (withBadge, false);
				}

			} else {
				ShowObject (withBadge, false);

			}

			ShowObject (txtComment, int.Parse (badgeString) > 3 || int.Parse (badgeString) == 0);
				
				
		}

	}

	public void CheckPlaying (int imageIndex) {
		PlayerPrefs.SetInt("imageIndex",imageIndex);
		SceneManager.LoadScene("Puzzle");
	}

	IEnumerator ShowSkip () {

		yield return new WaitForSeconds (3);
		skipText.SetActive (true);
		yield return new WaitForSeconds (8.2f);
		skipText.SetActive (false);

	}
}
