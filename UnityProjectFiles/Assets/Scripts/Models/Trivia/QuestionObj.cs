using UnityEngine;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.Collections.Generic;
using System.IO;

namespace Models.Trivia
{
	[Serializable]
	public class QuestionObj  {

		public string question;
		public bool status;
		public List<AnswerObj> answers;
		public char correctAnswer;
		public string audioUrl;
		public string audioFilename;

		/**
		 * Return the list of trivia question from a file or local file
		 * 
		 */
		public static List<QuestionObj> GetQuestions()
		{
			List<QuestionObj> questionList = new List<QuestionObj> ();
			if (File.Exists (Application.persistentDataPath + FileName.TRIVIA_QUESTION)) {
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (Application.persistentDataPath + FileName.TRIVIA_QUESTION, FileMode.Open);

				while (file.Position != file.Length) {
					questionList.Add ((QuestionObj)bf.Deserialize (file));
				}

				file.Close ();
			}
			//if cant fetch from API use the preloaded file
			else {
				TextAsset json = (TextAsset) Resources.Load<TextAsset> ("Files/triviaQuestions");

				JSONObject jsonResponse = new JSONObject(json.text);
				questionList = FormatQuestions (jsonResponse);
			}

			// Knuth shuffle algorithm :: courtesy of Wikipedia :)
			for (int t = 0; t < questionList.Count; t++ )
			{
				QuestionObj questions = (QuestionObj)questionList[t];

				int r = UnityEngine.Random.Range (t, questionList.Count);
				questionList[t] = questionList[r];
				questionList[r] = questions;

				// random answers
				List <AnswerObj> answerLists = questionList[t].answers;
				for (int ctrAns = 0; ctrAns < answerLists.Count; ctrAns++) {
					AnswerObj answer = (AnswerObj)answerLists[ctrAns];
					int ansRandom = UnityEngine.Random.Range (ctrAns, answerLists.Count);
					answerLists[ctrAns] = answerLists[ansRandom];
					answerLists[ansRandom] = answer;

					if (answerLists[ctrAns].isCorrect) {
						questionList[t].correctAnswer = QuestionObj.FormatAnswer(ctrAns + 1);
					}

				}
				questionList [t].answers = answerLists;
			}
			
			return questionList;
		}

		/**
		 * Returns a formated question / answer objects
		 * 
		 */
		public static List<QuestionObj> FormatQuestions(JSONObject obj)
		{
			List<QuestionObj> questionList = new List<QuestionObj> ();

			for (int x = 0; x <  obj.GetField("results").Count; x++) {
				QuestionObj questionIns = new QuestionObj ();
				List<AnswerObj> answerList = new List<AnswerObj> ();
				questionIns.question = obj.GetField ("results") [x].GetField ("question").str;
				questionIns.audioUrl = obj.GetField ("results") [x].GetField ("audio_url").str;
				questionIns.audioFilename = obj.GetField ("results") [x].GetField ("audio_filename").str;

				for (int i = 0; i < obj.GetField ("results") [x].GetField ("answers").Count; i++) {
					AnswerObj answerIns = new AnswerObj ();

					answerIns.answer = obj.GetField ("results") [x].GetField ("answers") [i].GetField ("answer").str;

					answerIns.isCorrect = (obj.GetField ("results") [x].GetField ("answers") [i].GetField ("is_correct").str == "1"
						|| obj.GetField ("results") [x].GetField ("answers") [i].GetField ("is_correct").n == 1f) ? true : false;
					answerIns.audioUrl = obj.GetField ("results") [x].GetField ("answers") [i].GetField ("audio_url").str;
					answerIns.audioFilename = obj.GetField ("results") [x].GetField ("answers") [i].GetField ("audio_filename").str;

					if (answerIns.isCorrect)  {
						questionIns.correctAnswer = QuestionObj.FormatAnswer(i + 1);
					}

					answerList.Add (answerIns);
				}
				questionIns.answers = answerList;
				questionList.Add (questionIns);
			}

			return questionList;
		}

		/**
		 * Return a formated answer in letter 
		 * 
		 */
		public static char FormatAnswer(int num) {
			char answer = 'A';

			switch (num) {
			case 1:
				answer = 'A';
				break;
			case 2:
				answer = 'B';
				break;
			case 3:
				answer = 'C';
				break;
			case 4:
				answer = 'D';
				break;
			default:
				break;
			}

			return answer;
		}
	}

}

