﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class MemoryGameController : MonoBehaviour {

	Enum.MemoryScreenUI screenShowing;
	List <Canvas> _canvasList = new List<Canvas> ();


	//Variables
	Enum.TriviaScreenUI _screenShowing;
	Canvas _modal;
	//Canvas _gameOver;

	Button[] cards;
	List<string> tagList = new List<string> { "1", "2", "3","4", "5", "6","1", "2", "3","4", "5", "6" };
	private bool isMatched;
	private int clickCtr = 0;
//	private int prevCardTag = 0;
	private Button firstCardBtn;
	private Button secondCardBtn;
	private int correct = 0;
	private int wrong = 0;
	private Text correctText;
	private Text wrongText;
	private Text timerText;
	private Text pointVal;
	//private Text minusVal;


	public Text titleText;
	public Text messageText;


	private FadeInOut fadeOut;
	private int flagVal=0;
	private bool flagTimeIsUp;
	private bool isPause;

	public GameObject confettiAsset; 


	float timeLeft = 181;
	float point = 5000.00f;

	// back cover image
	private Sprite backCardImg;

	// the playing random cards
	private Sprite[] randomSprites;

	private AudioSource audioSource;
	public AudioClip cardOpenSound;
	public AudioClip cardMatchSound;
	public AudioClip cardWrongSound;
	public AudioClip gameOverSound;
	public AudioClip gameOverFailSound;
	public Text yourScoreVal;
	public Text highScoreVal;

	public AudioClip[] correctSounds;
	public AudioClip[] wrongSounds;

	Animator cardFlipAnimator;

	private GridLayoutGroup cardContainer;

	public AnimationClip minusScoreAnim;
	public AudioClip tap_button;

	// the minus -200
	private Text minusScoreText;
	private Text minusScoreText2;

	private Image glowCard;
	private Image glowCard2;

	private bool pauseScore = false;

	private Canvas _loading;

	// cards positions
	private Vector3 cardOnePosition;
	private Vector3 cardTwoPosition;

	public void Start () {
//		PlayerPrefs.SetString ("memoryBestTime", null); 
//		PlayerPrefs.Save ();
		confettiAsset.SetActive(false);
		LoadCanvas ();

		_loading = GameObject.Find ("ModalLoading").GetComponent<Canvas> ();

		cardContainer = GameObject.Find ("GamePlayUI/Cards").GetComponent<GridLayoutGroup> ();
		cardContainer.hideFlags = HideFlags.HideAndDontSave;

		timerText = GameObject.Find ("CountdownTimer").GetComponent <Text>();
		pointVal = GameObject.Find ("pointsVal").GetComponent <Text>();
		//minusVal = GameObject.Find ("minusVal").GetComponent <Text>();
		//		titleText = GameObject.Find ("Title").GetComponent <Text>();
		//		messageText = GameObject.Find ("Message").GetComponent <Text>();


		// hide the MinusScore text
		minusScoreText = GameObject.Find("GamePlayUI/MinusScore").GetComponent<Text> ();
		minusScoreText.gameObject.SetActive (false);
		minusScoreText2 = GameObject.Find("GamePlayUI/MinusScore2").GetComponent<Text> ();
		minusScoreText2.gameObject.SetActive (false);

		// hide glowcards
		glowCard = GameObject.Find("GamePlayUI/Glow").GetComponent<Image> ();
		glowCard.gameObject.SetActive (false);
		glowCard2 = GameObject.Find("GamePlayUI/Glow2").GetComponent<Image> ();
		glowCard2.gameObject.SetActive (false);

		// set score
		correctText = GameObject.Find ("Correct").GetComponent <Text>();
		correctText.text = correct.ToString();

		// set score
		wrongText = GameObject.Find ("Wrong").GetComponent <Text>();
		wrongText.text = wrong.ToString();

		// back cover card
		backCardImg = Resources.LoadAll <Sprite> ("MemoryGame/card_Asset")[0];

		// set audio
		audioSource = GetComponent<AudioSource> ();

		AssignTags ();
		InvokeRepeating ("GameTimer", 0, 1.0f / 1000);
		ShowModalLoading (false);
	}

	// Use this for initialization
	public void LoadCanvas() {
		//		_canvasList = new List<Canvas> ();

		_canvasList.Add (GameObject.Find ("GamePlayUI").GetComponent<Canvas> ());

		_canvasList.Add (GameObject.Find ("GameOverUI").GetComponent<Canvas> ());
		//_canvasList.Add (GameObject.Find ("ModalUI").GetComponent<Canvas> ());
		_modal = GameObject.Find ("ModalUI").GetComponent<Canvas> ();
		_modal.gameObject.SetActive (false);

		ShowScreen (Enum.MemoryScreenUI.GamePlayUI);

	}


	// Update is called once per frame
	void Update () {
//		if (!isPause) {
//			GameTimer ();
//		}

		#if UNITY_ANDROID

		if (Input.GetKeyDown(KeyCode.Escape)) 
			_modal.gameObject.SetActive(true);

		#endif

	}

	void ShowScreen(Enum.MemoryScreenUI canvasEnum)
	{
		for (int x = 0; x < _canvasList.Count; x++) {

			if (_canvasList [x].gameObject.activeSelf) {
				flagVal = x;
			}
			_canvasList [x].gameObject.SetActive (false);
		}

		_canvasList[(int)canvasEnum].gameObject.SetActive (true);
	}

	void AssignTags(){
		cards = GameObject.Find ("Cards").GetComponentsInChildren <Button> ();

		List<string> randomArr= RandomizeArray (tagList);

		// set random cards
		randomSprites = RandomSprites ();

		for (var x = 0; x < randomArr.Count; x++) {
			//Debug.Log("cards name " + cards[x].name + " card tag "+cards[x].tag);
			cards [x].tag = randomArr[x].ToString();

		}
	}
	//
	private void ShowModalLoading (bool show) {
		_loading.gameObject.SetActive (show);
	}

	IEnumerator WaitBeforeExit() {
		ShowModalLoading(true);

		yield return new WaitForSeconds (1.5f);
		PlayerPrefs.SetInt ("toShowScreen",1);
		SceneManager.LoadScene("Main");
	}

	//Cancel Exit
	public void cancelExit(Button btn){
		_modal.gameObject.SetActive(false);
	}
	//Show Modal
	public void LeaveGame(Button btn){
		StartCoroutine (WaitBeforeExit());

		string sGameName = "Memory Game";
		Analytics.CustomEvent ("InGameCloseClick", new Dictionary<string, object> {
			{ "Game", sGameName },
			{ "BounceTime", Time.timeSinceLevelLoad}
		}
		);
		MyAnalytics.Instance.LogEvent("InGameCloseClick", $"Game-{sGameName}", $"BounceTime-{Time.timeSinceLevelLoad}", 1);
	}
	//Show Modal
	public void ExitGame(Button btn){
		//ShowScreen (Enum.MemoryScreenUI.ModalUI);
		_modal.gameObject.SetActive(true);
	}

	//Play Again
	public void OnPlayAgainClick(Button btn){

		string sGameName = "Memory Game";
		/* Analytics For Games
					 * Game: Which game screen am I?
					 */
		Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
			{ "Game", sGameName }
		}
		);
		MyAnalytics.Instance.LogEvent("PlayClick", "Game", $"{sGameName}", 1);
		SceneManager.LoadScene ("Memory");
	}

	//Card Tap
	public void OnButtonClick(Button btn){


		if (firstCardBtn != null && secondCardBtn != null) {
//			Debug.Log ("WAIT!!!!");
		}else if (btn.Equals(firstCardBtn)) {
//			Debug.Log ("Already Click");
		} else {
			if (clickCtr == 0) {
				// assign first card
				firstCardBtn = btn;

				iTween.RotateTo (firstCardBtn.gameObject,  new Vector3 (0, 180, 0), 1);

				StartCoroutine(SetCardFace(firstCardBtn, randomSprites [Int32.Parse (firstCardBtn.tag) - 1]));

				// on sound
				audioSource.PlayOneShot(cardOpenSound, 1f);

				clickCtr++;
			} else {
				// assign second card
				secondCardBtn = btn;

				iTween.RotateTo (secondCardBtn.gameObject,  new Vector3 (0, 180, 0), 1);

				StartCoroutine(SetCardFace(secondCardBtn, randomSprites [Int32.Parse (secondCardBtn.tag) - 1]));

				// on sound
				audioSource.PlayOneShot(cardOpenSound, 1f);

				// show the 2nd card for awhile
				StartCoroutine(WaitToExecute(1f));

			}
		}


		bool bCardClick = true;
		/* Analytics for the card clicks on Characters, Movies, Store, What's New and Play Time
					 * Who: Which card was clicked
					 * ButtonClick: If the user clicked the button or the Card
					 * Where: Which tab am I? 
					 */
		Analytics.CustomEvent ("Card Click", new Dictionary<string, object> {
			{ "Who", btn.tag},
			{ "ButtonClick", bCardClick},
			{ "Where", "Playtime" }
		}
		);
		MyAnalytics.Instance.LogEvent($"Card Click-{btn.tag}", $"ButtonClick-{bCardClick}", "Where-Playtime", 1);
	}


	//Set Best Score
	void BestScore()
	{

		string score = PlayerPrefs.GetString("memoryBestScore");

		if (score.Length != 0) {

			float scoreInt = float.Parse(score);
//			Debug.Log("point" + point);
//			Debug.Log("scoreInt" + scoreInt);

			if (point >= scoreInt) {

				int pointInt= Int32.Parse(Mathf.Ceil(point).ToString()) ;
				titleText.text = "You got the best score";
				yourScoreVal.text = pointInt.ToString();
				string pointStr = pointInt.ToString ();

				PlayerPrefs.SetString ("memoryBestScore", pointStr); 
				PlayerPrefs.Save ();


			} else {
				if (point <= 0) 
				{
					titleText.text = "YOUR SCORE" ;
					yourScoreVal.text=  "0";//Mathf.Ceil(scoreInt).ToString();
					messageText.text ="High Score";
					highScoreVal.text=Mathf.Ceil(scoreInt).ToString();
				}else
				{
//					titleText.text = "High score is \n" +  Mathf.Ceil(scoreInt).ToString();
//					messageText.text = "Your score is \n " +  Mathf.Ceil(point).ToString();

					titleText.text = "YOUR SCORE" ;
					yourScoreVal.text = Mathf.Ceil (point).ToString ();
					messageText.text ="High Score";
					highScoreVal.text=Mathf.Ceil(scoreInt).ToString();
				}

			}
			int iTouchCount = 0;
			bool bDidWin = true;
			/* Analytics For Games
					 * Game: Which game screen am I?
					 * TouchCount: How many screen touches on screen we had?
					 * bDidWin: Did the player won the game?
					 */
			Analytics.CustomEvent ("GameOver", new Dictionary<string, object> {
				{ "Game", "Memory" },
				{ "TouchCount", iTouchCount},
				{ "DidHeWin", bDidWin}
			});
			if (bDidWin)
				MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Won", 1);
			else MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Lost", 1);
		} else {
			int pointInt= Int32.Parse(Mathf.Ceil(point).ToString()) ;

			if (pointInt <= 0)
			{
				titleText.text = "YOUR SCORE";
				yourScoreVal.text = "0";
				int iTouchCount = 0;
				bool bDidWin = false;
				/* Analytics For Games
					 * Game: Which game screen am I?
					 * TouchCount: How many screen touches on screen we had?
					 * bDidWin: Did the player won the game?
					 */
				Analytics.CustomEvent ("GameOver", new Dictionary<string, object> {
					{ "Game", "Memory" },
					{ "TouchCount", iTouchCount},
					{ "DidHeWin", bDidWin}
				});
				if (bDidWin)
					MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Won", 1);
				else MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Lost", 1);
			}
			else{

				titleText.text = "You got the best score";
				yourScoreVal.text = Mathf.Ceil (point).ToString ();
				string pointStr = point.ToString ();
				PlayerPrefs.SetString ("memoryBestScore", pointStr); 
				PlayerPrefs.Save ();
				int iTouchCount = 0;
				bool bDidWin = true;
				/* Analytics For Games
					 * Game: Which game screen am I?
					 * TouchCount: How many screen touches on screen we had?
					 * bDidWin: Did the player won the game?
					 */
				Analytics.CustomEvent ("GameOver", new Dictionary<string, object> {
					{ "Game", "Memory" },
					{ "TouchCount", iTouchCount},
					{ "DidHeWin", bDidWin}
				}
				);
				if (bDidWin)
					MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Won", 1);
				else MyAnalytics.Instance.LogEvent("Game Over", "Memory Game", "Lost", 1);
			}
		}
	}

	void MatchCards(bool flag) {
		if (flag) {

			firstCardBtn.transform.SetAsLastSibling ();
			secondCardBtn.transform.SetAsLastSibling ();

			//pause first the score coutdown
			pauseScore = true;

			// save card positions
			cardOnePosition = firstCardBtn.transform.position;
			cardTwoPosition = secondCardBtn.transform.position;

			// animate
			Hashtable matchHashtable = new Hashtable();
			matchHashtable.Add ("x", -20f);
			matchHashtable.Add ("y", 0.01f);
			matchHashtable.Add ("time", 1.5f);
			matchHashtable.Add ("onupdate", "OnUpdateCardMove");
			matchHashtable.Add ("onupdatetarget", this.gameObject);
			matchHashtable.Add ("oncomplete", "OnCompleteCardMove");
			matchHashtable.Add ("oncompletetarget", this.gameObject);
			iTween.MoveTo(firstCardBtn.gameObject, matchHashtable);

			Hashtable matchHashtableSecond = new Hashtable();
			matchHashtableSecond.Add ("x", 20f);
			matchHashtableSecond.Add ("y", 0.01f);
			matchHashtableSecond.Add ("time", 1.5f);
			matchHashtableSecond.Add ("onupdate", "OnUpdateCardMoveSecond");
			matchHashtableSecond.Add ("onupdatetarget", this.gameObject);
			matchHashtableSecond.Add ("oncomplete", "OnCompleteCardMoveSecond");
			matchHashtableSecond.Add ("oncompletetarget", this.gameObject);
			iTween.MoveTo(secondCardBtn.gameObject, matchHashtableSecond);

			clickCtr = 0;

			secondCardBtn.enabled = false;
			firstCardBtn.enabled = false;

			// set correct score
			correct++;
			correctText.text = correct.ToString ();
		} else {
			// reset click counter to 0
			clickCtr = 0;

			//pause first the score coutdown
			pauseScore = true;

			// show - score
			minusScoreText.transform.position = firstCardBtn.transform.position;
			minusScoreText.gameObject.SetActive(true);
			minusScoreText.color = new Color (
				minusScoreText.color.r, 
				minusScoreText.color.g, 
				minusScoreText.color.b, 
				1);
			
			minusScoreText2.transform.position = secondCardBtn.transform.position;
			minusScoreText2.gameObject.SetActive(true);
			minusScoreText2.color = new Color (
				minusScoreText.color.r, 
				minusScoreText.color.g, 
				minusScoreText.color.b, 
				1);


			Hashtable tweenParams = new Hashtable();
			tweenParams.Add("position", new Vector3(
				minusScoreText.transform.position.x, 
				minusScoreText.transform.position.y + 20,
				minusScoreText.transform.position.z));
			tweenParams.Add("time", 1);
			tweenParams.Add ("onupdate", "OnUpdateMoveMinusScore");
			tweenParams.Add ("onupdatetarget", this.gameObject);
			tweenParams.Add ("oncomplete", "OnCompleteMoveMinusScore");
			tweenParams.Add ("oncompletetarget", this.gameObject);

			iTween.MoveTo (minusScoreText.gameObject, tweenParams);
			iTween.MoveTo (minusScoreText2.gameObject, new Vector3(
				minusScoreText2.transform.position.x, 
				minusScoreText2.transform.position.y + 20, 
				minusScoreText2.transform.position.z), 1.5f);

			// wiggle card
			iTween.ShakeRotation(firstCardBtn.gameObject, iTween.Hash("amount", new Vector3(1, 1, 20), "time", 1, "oncomplete", "OnCompleteShakeCardWrong", "oncompletetarget", this.gameObject));
			iTween.ShakeRotation(secondCardBtn.gameObject, new Vector3(1, 1, 20), 1);

			// on sound			
			int r = UnityEngine.Random.Range(0, wrongSounds.Length-1);
			audioSource.PlayOneShot(wrongSounds[r], 1f);

			// set wrong score
			wrong++;
			wrongText.text = wrong.ToString ();
			//minusVal.text = "-400";

			point = point - 400;
			pointVal.text = Mathf.Ceil(point).ToString();


		}
	}

	/**
	 * Callback function on after the show score animation
	 * 
	 */ 
	void OnCompleteMoveMinusScore() {
		minusScoreText.gameObject.SetActive (false);
		minusScoreText2.gameObject.SetActive (false);
	}

	/**
	 * Callback function on every update of the show score animation
	 * 
	 */ 
	void OnUpdateMoveMinusScore() {
		minusScoreText.color = new Color (
			minusScoreText.color.r, 
			minusScoreText.color.g, 
			minusScoreText.color.b, 
			minusScoreText.color.a - 0.03f);

		minusScoreText2.color = new Color (
			minusScoreText.color.r, 
			minusScoreText.color.g, 
			minusScoreText.color.b, 
			minusScoreText.color.a - 0.03f);
	}

	/**
	 * Callback function on every update the card move to the center
	 * 
	 */ 
	void OnUpdateCardMove() {
		firstCardBtn.transform.localScale = new Vector3 (firstCardBtn.transform.localScale.x + 0.007f, 
			firstCardBtn.transform.localScale.y + 0.007f, 1);
	}

	/**
	 *  Callback on after the card move complete
	 */ 
	void OnCompleteCardMove() {
		Hashtable cardFadeOutHash = new Hashtable ();
		cardFadeOutHash.Add ("scale", new Vector3(1,1,1));
		cardFadeOutHash.Add ("time", 1f);
		cardFadeOutHash.Add ("onupdate", "OnUpdateCardFadeOut");
		cardFadeOutHash.Add ("onupdatetarget", this.gameObject);
		cardFadeOutHash.Add ("oncomplete", "OnCompleteCardFadeOut");
		cardFadeOutHash.Add ("oncompletetarget", this.gameObject);

		iTween.ScaleTo (firstCardBtn.gameObject, cardFadeOutHash);

	}

	/**
	 *  Callback on update second card move
	 */ 
	void OnUpdateCardMoveSecond() {
		secondCardBtn.transform.localScale = new Vector3 (secondCardBtn.transform.localScale.x + 0.007f, 
			secondCardBtn.transform.localScale.y + 0.007f, 1);
	}

	/**
	 * Callback after the card move 2nd card move
	 */ 
	void OnCompleteCardMoveSecond() {
		Hashtable cardFadeOutHash = new Hashtable ();
		cardFadeOutHash.Add ("scale", new Vector3(1,1,1));
		cardFadeOutHash.Add ("time", 1f);
		cardFadeOutHash.Add ("onupdate", "OnUpdateCardFadeOut");
		cardFadeOutHash.Add ("onupdatetarget", this.gameObject);
		cardFadeOutHash.Add ("oncomplete", "OnCompleteCardFadeOut");
		cardFadeOutHash.Add ("oncompletetarget", this.gameObject);
	
		iTween.ScaleTo (secondCardBtn.gameObject, cardFadeOutHash);

	}

	void OnUpdateCardFadeOut() {
		Image firstImage = firstCardBtn.GetComponent<Image> ();
		firstImage.color = new Color (1, 1, 1, firstImage.color.a - 0.03f);

		Image secondImage = secondCardBtn.GetComponent<Image> ();
		secondImage.color = new Color (1, 1, 1, secondImage.color.a - 0.03f);
	}

	void OnCompleteCardFadeOut() {

		// move back the card position
		if (firstCardBtn != null) {
			firstCardBtn.transform.position = cardOnePosition;
		}
		if (secondCardBtn != null) {
			secondCardBtn.transform.position = cardTwoPosition;
		}

		// reset card
		firstCardBtn = null;
		secondCardBtn = null;

		// start the score countdown
		pauseScore = false;

		// if score is now its winning dance!
		if (correct == 6) {
			isPause = true;
			CancelInvoke();
			StartCoroutine (showConfetti ());

		}
	}

	/**
	 * Callback function on complete card shake wrong
	 */ 
	void OnCompleteShakeCardWrong() {
		// reset back the card
		iTween.RotateTo (firstCardBtn.gameObject, new Vector3(0, 1, 0), 1);
		iTween.RotateTo (secondCardBtn.gameObject, new Vector3(0, 0, 0), 1);

		StartCoroutine(SetCardFace(firstCardBtn, backCardImg));
		StartCoroutine(SetCardFace(secondCardBtn, backCardImg));

		firstCardBtn = null;
		secondCardBtn = null;

		// start the score countdown
		pauseScore = false;
	}

	/**
	 * Callback function after the glow cards match
	 */
	void OnCompleteMatchAnimation() {
		glowCard.gameObject.SetActive (false);
		glowCard2.gameObject.SetActive (false);
	}

	List<string> RandomizeArray(List<string> arr)
	{
		for (int t = 0; t < arr.Count ; t++ )
		{
			string tmp = arr[t];
			int r = UnityEngine.Random.Range(t, arr.Count);
			arr[t] = arr[r];
			arr[r] = tmp;
		}
		return arr;
	}

	void GameTimer()
	{
		timeLeft -= Time.deltaTime;

		if (point <= 0) {
//			flagTimeIsUp = true;
//			CancelInvoke();
//			ShowScreen (Enum.MemoryScreenUI.GameOverUI);
//			BestScore ();
			pointVal.text = "0";

			// on sound
			audioSource.PlayOneShot(gameOverFailSound, 1f);

			flagTimeIsUp = true;
			CancelInvoke();
			StartCoroutine (showGameOverScreen ());

		} else {

			if (!pauseScore) {
				point = point - 0.016f;

				pointVal.text = Mathf.Ceil(point).ToString();
			}

		}
	}


	Sprite[] RandomSprites()
	{
		// Load all cards
		Sprite[] goodGuySprites =  Resources.LoadAll <Sprite> ("MemoryGame/goodGuys_Asset");
		Sprite[] badGuySprites =  Resources.LoadAll <Sprite> ("MemoryGame/badGuys_Asset");

		// random the cards
		for (int t = 0; t < goodGuySprites.Length ; t++ )
		{
			Sprite tmp = goodGuySprites[t];
			int r = UnityEngine.Random.Range(t, goodGuySprites.Length);
			goodGuySprites[t] = goodGuySprites[r];
			goodGuySprites[r] = tmp;
		}

		for (int t = 0; t < badGuySprites.Length ; t++ )
		{
			Sprite tmp = badGuySprites[t];
			int r = UnityEngine.Random.Range(t, badGuySprites.Length);
			badGuySprites[t] = badGuySprites[r];
			badGuySprites[r] = tmp;
		}

		Sprite[] combineSprites = new Sprite[6];

		System.Array.Resize (ref goodGuySprites, 6);

		System.Array.Copy (goodGuySprites, combineSprites, 6);
		combineSprites [1] = badGuySprites [1];

		return combineSprites;
	}

	IEnumerator WaitToExecute(float time) {
		yield return new WaitForSeconds(time);

		if (firstCardBtn.tag == secondCardBtn.tag) {
			// on sound

			audioSource.PlayOneShot (cardMatchSound, 1f);
			yield return new WaitForSeconds (0.2f);

			int r = UnityEngine.Random.Range(0, correctSounds.Length-1);
			audioSource.PlayOneShot(correctSounds[r], 1.5f);

			MatchCards (true);
		} else {
			MatchCards (false);
		}
	}

	// Do shuffle first
	IEnumerator WaitToExecuteToPlay(float time) {
		yield return new WaitForSeconds (time);
	
		LoadCanvas ();
	}

	// Do shuffle first
	IEnumerator showConfetti() {

		confettiAsset.SetActive(true);
		audioSource.PlayOneShot(gameOverSound, 1f);

		// do the winning card dance
		for (int x = 0; x < cards.Length; x++) {
			cards [x].gameObject.GetComponent<Image> ().color = new Color (1, 1, 1, 1);
		}
		yield return new WaitForSeconds(1.0f);

		for (int x = 0; x < cards.Length; x++) {
			iTween.RotateBy (cards [x].gameObject, new Vector3(1, 0, 0), 1f);
			StartCoroutine(SetCardFaceBack(cards [x], cards [x].image.sprite));

			yield return new WaitForSeconds(0.2f);
		}
			
		yield return new WaitForSeconds(2.0f);
		confettiAsset.SetActive(false);
		ShowScreen (Enum.MemoryScreenUI.GameOverUI);
		BestScore ();

	}

	// Do shuffle first
	IEnumerator showGameOverScreen() {
		yield return new WaitForSeconds(2.0f);
		ShowScreen (Enum.MemoryScreenUI.GameOverUI);
		BestScore ();

	}

	// Show Card Face
	IEnumerator SetCardFace(Button button, Sprite sprite) {
		yield return new WaitForSeconds (0.12f);
		button.image.sprite = sprite;
	}

	// show back card then back to card face
	IEnumerator SetCardFaceBack(Button button, Sprite sprite) {
		yield return new WaitForSeconds (0.12f);
		button.image.sprite = backCardImg;
		yield return new WaitForSeconds (0.1f);
		button.image.sprite = sprite;
	}

	// Play SFX
	private void PlaySFX(AudioClip clip) {

		if (audioSource.isPlaying)
			audioSource.Stop ();

		audioSource.PlayOneShot (clip, 1f);
	}

	public void TapButton () {
		PlaySFX (tap_button);
	}

}
