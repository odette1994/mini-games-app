using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using Models.Trivia;
using System;
using UnityEngine.Analytics;

public class TriviaController : MonoBehaviour 
{
	public Animator 			clockAnimator;
	public Text 				countdown;
	public Slider 				slider;
	public GameObject 			elements;
	public Text 				questionText;
	public Text 				question_Static;
	public List<Text> 			btnAnswer;
	public GameObject 			modal;
	public List<Button>			answers; 
	public List<Image>			iconFrame;
	public List<Button>			icon;
	public Animator[] 			animButton;
	public Text[] 				messages;
	public AudioClip 			audMatch;
	public AudioClip 			audTryAgain;
	public GameObject 			matchScreen;
	public GameObject 			timeEndScreen;
	public AudioClip 			gameover;
	public Text 				txtscore;
	public Button 				playAgain;
	public GameObject 			confetti;
	public Animator 			question_fade;
	public GameObject 			wrongAnsScreen;
	public AudioClip 			showButton;
	public AudioClip 			audioBuzzer;
	public AudioClip 			highScore;
	public AudioClip 			lowScore;
	public AudioClip			tick_sound;
	public AudioClip			tap_button;
	public AudioClip			endingBellAudio;

	// Constants
	public const float 			ANIMATION_SPEED = 0.0125f;
	private const float 		SLIDER_SPEED = 1.0f;

	[SerializeField] private GameObject 				loading;
	private AudioSource 		source;
	public List<AudioClip> 	wrongAud;
	public List<AudioClip> 		correctAud;
	public bool 				hasStarted = false;	
	public bool 				isPaused = false;
	public bool 				isGameOver = false;
	public int 					score;	
	public bool 				hasClicked;
	private int 				clickCounter;
	private float 				progress;
	private float 				maxVal;
	private float 				minVal;
	private List<QuestionObj> 	questionList;
	private int 				questionCtr;
	private bool 				isMatched;
	[SerializeField] private TriviaUI			trivia;
	[SerializeField] private FadeInOut 			fadeOut;
	private char 				correctAnswer;
	private Sprite 				wrongChoice;
	private float				audSize;
	float 						timer;
	
	private bool 				hasExited;
	bool 						hasIcon;
	bool						hasAudio;
	string 						message;
	[SerializeField] Button						btnClose;

	// Use this for initialization

	void Awake () 
	{
		Input.multiTouchEnabled = false;

		wrongAud = new List<AudioClip> ();
		correctAud = new List<AudioClip> ();

		AddSFX ("SFX/Trivia_wrong", wrongAud);
		AddSFX ("SFX/Trivia_correct", correctAud);

		source = GetComponent<AudioSource> ();
		
		wrongChoice = Resources.LoadAll<Sprite> ("Trivia/trivia_new_asset")[5];

		btnClose.onClick.AddListener (delegate { _SetActive (modal, true); TapButton(); });

		ShowButtons (true);

		answers [0].onClick.AddListener ( delegate { OnClickButton(answers[0]); });
		answers [1].onClick.AddListener ( delegate { OnClickButton(answers[1]); });
		answers [2].onClick.AddListener ( delegate { OnClickButton(answers[2]); });
		answers [3].onClick.AddListener ( delegate { OnClickButton(answers[3]); });

		// hide answer btn
		ShowButtons (false);

		// Hide
		_SetActive (confetti, false);
		_SetActive (modal, false);
		_SetActive (wrongAnsScreen, false);
		_SetActive (matchScreen, false);
		_SetActive (countdown, false);
		_SetActive (elements, false);
		_SetActive (timeEndScreen, false);
	}


	public void Run() 
	{
		StartCoroutine(Init ());
	}

	// Update is called once per frame
	void Update () {

		CheckPause ();
		SliderTimer ();

		#if UNITY_ANDROID

		if (Input.GetKeyDown(KeyCode.Escape) && !loading.activeInHierarchy) 
		_SetActive(modal, true);

		#endif
	}

	IEnumerator Init() 
	{
		message = "";

		//get and set trivia timer setting
		minVal = 0f;
		score = 0;
		timer = 0f;
		clickCounter = 0;
		isMatched = false;
		hasExited = false;
		
		SetMaxTime (TriviaSetting.Timer());
		//SetMaxTime(0.3f);
		GetSliderValue ();
		trivia.ShowCrown (false);

		ShowGameElements (false);
		countdown.gameObject.SetActive (false);

		// Load the trivia questions
		questionList = QuestionObj.GetQuestions();
		questionCtr = 0;

		yield return new WaitForSeconds (2f);

		countdown.gameObject.SetActive (true);
		StartCoroutine (Countdown());
		InvokeRepeating ("CheckGameOver",0, 1.0f / GetMaxTime());
	}

	void AddSFX (string location, List<AudioClip> list) {

		foreach (object aud in Resources.LoadAll(location)) {
			list.Add (aud as AudioClip);
		}
	}

	IEnumerator Countdown() {

		int time = 3;

		while (time >= 0) {

			if (time > 0)
				countdown.text = time.ToString ();

			else {
				StopCoroutine (Countdown ());
				countdown.gameObject.SetActive (false);
				hasStarted = true;
				ShowGameElements (true);
				StartCoroutine(SetQuestions ());
			}
			time--;
			yield return new WaitForSeconds (1f);
		}
	}

	IEnumerator PlayGame() {

		//Countdown ();

		if (hasStarted && !isPaused) {

			bool showNxt = false;

			// Checking the answer and counting clicks
			if (isMatched || clickCounter >= 2) {

				timer += Time.deltaTime;

				if (isMatched) {
					hasExited = false;

					yield return new WaitForSeconds (0.5f);
					ShowMatchScreen (true);

					yield return new WaitForSeconds (1.5f);
					ShowButtons (false);
					ShowMatchScreen (false);
					ShowIcons (false);
					ScreenOverlay (false, false);
					showNxt = !showNxt;

				} else if (clickCounter == 2 && !isMatched) {
					ShowTryAgainScreen (true);
					hasExited = false;

					yield return new WaitForSeconds (1.5f);
					ShowButtons (false);
					ShowIcons (false);
					ShowTryAgainScreen (false);
					ScreenOverlay (false, false);
					showNxt = !showNxt;

				} else {
					ShowButtons (false);
					ShowIcons (false);
					ScreenOverlay (false, false);
					showNxt = !showNxt;

				}

				if (showNxt) {
					//animationController.hasEnded = false;
					// Reset value
					NextQuestion ();
					//ShowButtons(true);
					//					PlaySFX(showButton);
					EnableButtons (true);
					ShowMatchScreen (false);
					SetButton ();
					clickCounter = 0; 
					isMatched = false;
					hasExited = false;
					timer = 0f;
					question_Static.text = "";
					//isPaused = false;
					showNxt = !showNxt;
				}


			}
			string sGameName = "Trivia Game";
			/* Analytics For Games
					 * Game: Which game screen am I?
					 */
			Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
				{ "Game", sGameName }
			}
			);
				MyAnalytics.Instance.LogEvent("PlayClick", "Game", "Trivia Game", 1);
		}


	}

	void CheckGameOver () {

		AnimateClock ();

		if (slider.value <= 0.0f) 
			StartCoroutine(GameOver ());//isGameOver = true;

	}

	void AnimateClock() {

		// Animate clock if it's 10 secs left

		if (slider.value <= 10.0f) {
			clockAnimator.SetBool ("isRunning", true);
			source.PlayOneShot (tick_sound, 1f);

			if (slider.value <= 0.1f) {

			}

		}

	}

	void CheckPause() {

		// Check if ModalUI is active

		if (modal.activeInHierarchy) {
			isPaused = true;
		} else {
			isPaused = false;
		}

	}

	void SliderTimer() {

		if(!isPaused && hasExited)
			slider.value = Mathf.MoveTowards(slider.value, minVal, SLIDER_SPEED * Time.deltaTime);

	}

	void SetMaxTime(float seconds) {
		this.maxVal = seconds;
	}

	float GetMaxTime() {
		return maxVal;
	}

	float GetSliderValue() {
		slider.maxValue = GetMaxTime () * 60.0f;

		return slider.value = slider.maxValue;
	}

	IEnumerator GameOver() {
		// stop the invoke repeat
		CancelInvoke ();
		PlaySFX (endingBellAudio);
		_SetActive (timeEndScreen, true);
		yield return new WaitForSeconds (1);
		_SetActive (timeEndScreen, false);
		hasClicked = true;
		hasStarted = false;
		isPaused = true;
		ShowGameElements (false);
		trivia.ShowCrown (true);
		StartCoroutine (fadeOut.FadeToClear());
		trivia.ShowScreen ("GameOver");
		playAgain = GameObject.Find ("GameOverUI/btnReplay").GetComponent<Button> ();

		_SetActive (playAgain, false);

		playAgain.onClick.AddListener (delegate {
			Replay();
			TapButton();
		});

		messages = new Text[2];

		messages [0] = GameObject.Find ("GameOverUI/Title").GetComponent<Text> ();
		messages [1] = GameObject.Find ("GameOverUI/Message").GetComponent<Text> ();

		//StartCoroutine (AnimateScore(GetScore(),4f));
		StartCoroutine( ShowScore () );

	}

	/**
	 * Play the audio question
	 * 
	 */
	IEnumerator PlayAudio(string audioSource) {
		WWW www = new WWW ("file://" + audioSource);
		//WWW www = new WWW("http://swanprincessgameapp.s3.amazonaws.com/uploaded/trivia/audio/question-17.mp3");
		yield return www;

		if (www !=null && www.isDone) {
			AudioClip audioClip = www.GetAudioClip(false,false) as AudioClip;
			//AudioSource audioSourceU = GetComponent<AudioSource> ();
			source.clip = audioClip;
			source.loop = false;
			source.volume = 1f;
			source.PlayOneShot (audioClip);

			//if (audioClip.loadState == AudioDataLoadState.Loaded)


		}
	}



	IEnumerator SetQuestions() {
		questionText.color = Color.white;
		hasIcon = false;
		hasAudio = false;
		hasClicked = false;

		int len, index = -1;
		string str_question = "", str_answer = "", filename ="";

		str_question = questionList [questionCtr].question;
		len = str_question.Length;
		correctAnswer = questionList [questionCtr].correctAnswer;

		// Check if it contains :trivia
		if (str_question.Contains (":tri")) {
			string temp = str_question.Remove (len - 9);
			questionText.text = (!temp.Contains (":")) ? temp : str_question.Remove (len - 9);
			filename = "Trivia/" + str_question.Remove (0, len - 8);
			hasIcon = true;
		} else if (str_question.Contains ("&trivia")) {
			string temp = str_question.Remove (len - 7);
			questionText.text = (!temp.Contains ("&")) ? temp : str_question.Remove (len - 7);
			hasIcon = false;
		} else
			questionText.text = str_question;


		string tempName = "";	

		for (int i = 0; i < btnAnswer.Count; i++) {

			str_answer = questionList [questionCtr].answers [i].answer;

			if (str_answer.Contains (":") && hasIcon) {
				btnAnswer [i].text = StringToTitleCase (str_answer.Remove (str_answer.Length - 2));
				try {
					tempName = str_answer.Remove (0, str_answer.Length - 2);
					index = Int32.Parse (tempName);

				} catch(FormatException e) {
					Debug.Log (e.ToString());
				} finally {

					if (tempName.Length == 2) {
						tempName = str_answer.Remove (0, str_answer.Length - 2);
					} else {
						tempName = str_answer.Remove (0, str_answer.Length - 1);
					}

					index = Int32.Parse (tempName);
				}

				if (btnAnswer[i].text.Contains(":"))
					btnAnswer [i].text = StringToTitleCase (str_answer.Remove (str_answer.Length - 3));
				else
					btnAnswer [i].text = StringToTitleCase (str_answer.Remove (str_answer.Length - 2));

				icon [i].enabled = true;
				icon[i].image.sprite = Resources.LoadAll<Sprite> (filename)[index];
			} else {
				btnAnswer [i].text = StringToTitleCase (str_answer);
				icon [i].enabled = false;
			}

			//			Debug.Log (btnAnswer[i].text);

		}

		// test play audio
		if (File.Exists (Application.persistentDataPath + "/" + questionList [questionCtr].audioFilename)) {
			hasAudio = true;
			//			Debug.Log (Application.persistentDataPath + "/" + questionList [questionCtr].audioFilename);
		} else {
			hasAudio = false;
		}

		if (hasAudio) {

			if (!modal.activeInHierarchy)
				StartCoroutine (PlayAudio(Application.persistentDataPath + "/" + questionList [questionCtr].audioFilename));

			yield return new WaitForSeconds(3);
			question_fade.SetBool ("isFaded",true);

			yield return new WaitForSeconds(0.7f);
			questionText.gameObject.SetActive (false);
			question_Static.text = questionText.text;
			ShowButtons (true);
			ShowIcons (hasIcon);
			hasExited = true;


			// play choices audio
			yield return new WaitForSeconds(0.5f);
			for (int i = 0; i < 4; i++) {

				//				if (File.Exists (Application.persistentDataPath + "/" + questionList [questionCtr].answers [i].audioFilename) && !hasClicked && !modal.activeInHierarchy) {
				//					StartCoroutine (PlayAudio (Application.persistentDataPath + "/" + questionList [questionCtr].answers [i].audioFilename));
				//					answers [i].animator.SetTrigger ("Fade");
				//					icon [i].animator.SetTrigger ("Fade");
				//					yield return new WaitForSeconds (1.3f);
				//				} 


			}


		} else {
			yield return new WaitForSeconds(1.9f);
			question_fade.SetBool ("isFaded",true);

			yield return new WaitForSeconds(1f);
			questionText.gameObject.SetActive (false);
			question_Static.text = questionText.text;
			ShowButtons (true);
			ShowIcons (hasIcon);
			hasExited = true;
		}


		Debug.Log (correctAnswer);	
	}

	void ShowIcons(bool hasIcons) {
		for (int i = 0; i < 4; i++) {

			if (!hasIcons)
				icon [i].image.sprite = null;

			icon [i].gameObject.SetActive(hasIcons);
			iconFrame [i].gameObject.SetActive(hasIcons);
		}

	}

	void SetClear() {
		for(int i = 0; i < btnAnswer.Count; i++)
			btnAnswer[i].text = "";

		questionText.text = "";
	}

	void ShowButtons(bool enable) {

		for (int i = 0; i < answers.Count; i++) {

			answers [i].gameObject.SetActive(enable);

			if (enable)
				answers [i].GetComponentInChildren<Text> ().color = Color.black;

		}

		if (!enable) {
			questionText.text = "";
			//ScreenOverlay (false,false);
		}

	}

	void EnableButtons(bool enable) {

		for (int i = 0; i < btnAnswer.Count; i++) {

			btnAnswer [i].enabled = enable;
			answers [i].interactable = enable;
			answers [i].enabled = enable;
		}

	}

	void NextQuestion() {

		if (questionCtr < questionList.Count) {
			SetClear ();
			questionText.gameObject.SetActive (true);


			//			questionCtr = (questionCtr == questionList.Count - 1) ? 1 : questionCtr++;

			if (questionCtr == questionList.Count - 1)
				questionCtr = 1;
			else
				questionCtr++;

			StartCoroutine(SetQuestions ());
		}




	}

	void ShowGameElements(bool showElements) {

		if (elements != null) 
			elements.SetActive (showElements);
	}	

	void SaveScore() {

		if(score >= PlayerPrefs.GetInt("earned_score"))
			PlayerPrefs.SetInt ("earned_score",score);

		int iTouchCount = 0;
		bool bDidWin = true;
		/* Analytics For Games
					 * Game: Which game screen am I?
					 * TouchCount: How many screen touches on screen we had?
					 * bDidWin: Did the player won the game?
					 */
		Analytics.CustomEvent ("GameOver", new Dictionary<string, object> {
			{ "Game", "Trivia" },
			{ "TouchCount", iTouchCount},
			{ "DidHeWin", bDidWin}

		}
		);
		if (bDidWin)
			MyAnalytics.Instance.LogEvent("Game Over", "Trivia Game", "Won", 1);
		else MyAnalytics.Instance.LogEvent("Game Over", "Trivia Game", "Lost", 1);
	}

	int GetScore() {

		if(score <= PlayerPrefs.GetInt("earned_score"))
			return PlayerPrefs.GetInt ("earned_score");

		return score;
	}

	IEnumerator ShowScore() {

		int range = PlayerPrefs.GetInt ("earned_score") - Mathf.RoundToInt((PlayerPrefs.GetInt ("earned_score") / 2));
		string title = "";
		string message = "";

		if (score >= PlayerPrefs.GetInt ("earned_score") && score > 0) {
			StartCoroutine(showConfetti () );
			title = "Whoah!";
			message = "Your new high score is ";
			PlaySFX (highScore);
		} else if (score >= range && score <= PlayerPrefs.GetInt ("earned_score") && score > 0) {
			StartCoroutine(showConfetti () );
			title = "Good job!";
			message = "You scored ";
			PlaySFX (gameover);
		} else {
			title = "Try again.";
			message = "You scored ";
			PlaySFX (lowScore);
		}

		messages [0].text = title;

		timer += Time.deltaTime;

		SaveScore ();
		//txtscore = trivia.crown.GetComponentInChildren<Text> ();
		//if (timer > 0.5f && timer < 0.6f)
		yield return new WaitForSeconds(0.6f);
		messages [1].text = message + score + ".";

		//if (timer > 0.7f && timer < 0.8f) 
		yield return new WaitForSeconds(0.8f);
		txtscore.text = GetScore ().ToString ();

		//if (timer > 0.9f && timer < 1.0f)
		yield return new WaitForSeconds(1.0f);
		_SetActive (playAgain, true);

		int iTouchCount = 0;
		bool bDidWin = false;
		/* Analytics For Games
					 * Game: Which game screen am I?
					 * TouchCount: How many screen touches on screen we had?
					 * bDidWin: Did the player won the game?
					 */
		Analytics.CustomEvent ("GameOver", new Dictionary<string, object> {
			{ "Game", "Trivia Game" },
			{ "TouchCount", iTouchCount},
			{ "DidHeWin", bDidWin}

		}
		);
		if(bDidWin)
			MyAnalytics.Instance.LogEvent("Game Over", "Trivia Game", "Won", 1);
		else MyAnalytics.Instance.LogEvent("Game Over", "Trivia Game", "Lost", 1);
	}


	string StringToTitleCase(string text) {
		TextInfo textInfo = new CultureInfo("en-US", false).TextInfo;
		text = textInfo.ToTitleCase(text);

		return text;
	}


	// Buttons

	void SetButton () {
		for (int i = 0; i < 4; i++)
			answers[i].GetComponent<Image>().sprite = Resources.LoadAll<Sprite> ("Trivia/trivia_new_asset")[3];
	}

	void SetButtonWhen (int index, bool value) {

		if (value) {
			answers [index].GetComponent<Image> ().sprite = Resources.LoadAll<Sprite> ("Trivia/trivia_new_asset")[2];
			btnAnswer [index].GetComponent<Text> ().color = Color.white;

			for (int i = 0; i < 4; i++) {

				//				if (i != index) {
				//
				if (btnAnswer [i].color == Color.black)
					btnAnswer [i].color = Color.black;

				answers [i].interactable = false;
				answers [i].enabled = false;

				//				}

			}

		} else {
			answers[index].GetComponent<Image> ().sprite = wrongChoice;
			btnAnswer [index].GetComponent<Text> ().color = Color.white;
		}


	}

	public void OnClickButton(Button button) {

		// answers[0].transform.localPosition.x == -206f && answers[1].transform.localPosition.x == 535f && answers[2].transform.localPosition.x == -206f && answers[3].transform.localPosition.x == 535f
		if (clickCounter <= 2 && slider.value > 0.0f) {

			switch (button.name) {

			case "btnA":
				if (correctAnswer == 'A') {
					//match.transform.localPosition = new Vector3 (button.transform.localPosition.x - 30f, button.transform.localPosition.y + 119f, 0f);
					isMatched = true;
					ScreenOverlay (false, false);
					//DisplayMessage ();
					OnPlaySFX (correctAud);
					SetButtonWhen (0, true);
					score++;
				} else {
					//try_again.transform.localPosition = new Vector3 (button.transform.localPosition.x - 30f, button.transform.localPosition.y + 119f, 0f);
					ScreenOverlay (false, false);
					OnPlaySFX (wrongAud);
					SetButtonWhen (0, false);
					isMatched = false;

				}

				DisableAfterClick (0);

				break;

			case "btnB":
				if (correctAnswer == 'B') {
					//match.transform.localPosition = new Vector3 (button.transform.localPosition.x + 30f, button.transform.localPosition.y + 119f, 0f);
					isMatched = true;
					OnPlaySFX (correctAud);
					ScreenOverlay (false, false);
					//DisplayMessage ();
					SetButtonWhen (1, true);
					score++;
				} else {
					//try_again.transform.localPosition = new Vector3 (button.transform.localPosition.x + 30f, button.transform.localPosition.y + 119f, 0f);
					ScreenOverlay (false, false);
					OnPlaySFX (wrongAud);
					SetButtonWhen (1,false);
					isMatched = false;
				}

				DisableAfterClick (1);

				break;

			case "btnC":
				if (correctAnswer == 'C') {
					//match.transform.localPosition = new Vector3 (button.transform.localPosition.x - 30f, button.transform.localPosition.y + 119f, 0f);
					isMatched = true;
					ScreenOverlay (false, false);
					OnPlaySFX (correctAud);
					//DisplayMessage ();
					SetButtonWhen (2, true);
					score++;
				} else {
					//try_again.transform.localPosition = new Vector3 (button.transform.localPosition.x - 30f, button.transform.localPosition.y + 119f, 0f);
					ScreenOverlay (false, false);
					OnPlaySFX (wrongAud);
					SetButtonWhen (2, false);
					isMatched = false;
				}

				DisableAfterClick (2);

				break;

			case "btnD":
				if (correctAnswer == 'D') {
					//match.transform.localPosition = new Vector3 (button.transform.localPosition.x + 30f, button.transform.localPosition.y + 119f, 0f);
					isMatched = true;
					ScreenOverlay (false, false);
					OnPlaySFX (correctAud);
					//DisplayMessage ();
					SetButtonWhen (3, true);
					score++;
				} else {
					//try_again.transform.localPosition = new Vector3 (button.transform.localPosition.x + 30f, button.transform.localPosition.y + 119f, 0f);
					ScreenOverlay (false, false);
					OnPlaySFX (wrongAud);
					SetButtonWhen (3, false);
					isMatched = false;
				}

				DisableAfterClick (3);

				break;

			default:
				break;


			}

			hasClicked = true;
			clickCounter++;	
		}

		StartCoroutine( PlayGame () );
		//ShowIcons (false);
	}

	public void OnLoadMovies() {
		//StartCoroutine(fadeOut.FadeToClear ());
		StartCoroutine(WaitBeforeExit());
	}

	IEnumerator WaitBeforeExit () {
		ShowModalLoading (true);

		yield return new WaitForSeconds (1.5f);

		PlayerPrefs.SetInt ("toShowScreen",1);
		SceneManager.LoadScene("Main");
	}

	void ShowModalLoading(bool show) {
		loading.gameObject.SetActive(show);
		//
		//		if (firstload) {
		//			countdown.gameObject.SetActive (false);
		//			yield return new WaitForSeconds (timedelay);
		//			loading.gameObject.SetActive (false);
		//			//countdown.gameObject.SetActive (true);
		//		} else
		//			yield return new WaitForSeconds (timedelay);
	}


	public void Replay() {
		StartCoroutine(fadeOut.FadeToClear ());
		SceneManager.LoadScene ("Trivia");

		string sGameName = "Trivia Game";
		/* Analytics For Games
					 * Game: Which game screen am I?
					 */
		Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
			{ "Game", sGameName }
		}
		);
		MyAnalytics.Instance.LogEvent("PlayClick", "Game", "Trivia Game", 1);
	}

	private void ScreenOverlay(bool _match, bool _try_again) {
		//		match.SetActive (_match);
		//		try_again.SetActive (_try_again);
	}

	private void PlaySFX(AudioClip clip) {

		if (source.isPlaying)
			source.Stop ();

		source.PlayOneShot (clip, 1f);
	}

	private void OnPlaySFX (List<AudioClip> list) {

		Debug.Log (list);

		int index  = UnityEngine.Random.Range (0, list.Count);
		PlaySFX (list [index]);

		if (isMatched)
			DisplayMessage (list [index].name + "!");
		else
			message = list [index].name;


	}

	private void DisableAfterClick(int index) {

		answers[index].enabled = false;
		answers[index].interactable = false;
		//btnAnswer [index].color = Color.black;
	}

	private void ShowMatchScreen(bool show) {
		matchScreen.SetActive (show);
	}

	private void ShowTryAgainScreen(bool show) {

		wrongAnsScreen.SetActive (show);

		if (show) {
			Text msg = GameObject.Find ("WrongUI/Message").GetComponent<Text> ();
			msg.text = message + "!";
		}
	}

	private void DisplayMessage(string message) {
		//int ctr = (int)UnityEngine.Random.Range (0, arr_message.Length);
		matchScreen.GetComponentInChildren<Text> ().text = message;
	}

	public void TapButton () {
		PlaySFX (tap_button);
	}

	IEnumerator showConfetti()
	{
		confetti.SetActive (true);
		yield return new WaitForSeconds(7.0f);
		confetti.SetActive (false);

	}

	private void _SetActive (GameObject go, bool active) {
		go.SetActive (active);

		if (go.name.Equals ("ModalUI") && active) {
			Button yes = GameObject.Find ("ModalUI/Window/btnYes").GetComponent<Button> ();
			Button no = GameObject.Find ("ModalUI/Window/btnNo").GetComponent<Button> ();

			yes.onClick.AddListener (delegate { OnLoadMovies (); TapButton(); });
			no.onClick.AddListener (delegate { TapButton(); });
		}
	}

	private void _SetActive (Text text, bool active) {
		text.gameObject.SetActive (active);
	}

	private void _SetActive (Button button, bool active) {
		button.gameObject.SetActive (active);
	}

}
