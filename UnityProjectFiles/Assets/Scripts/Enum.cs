﻿using UnityEngine;
using System.Collections;

public class Enum : MonoBehaviour {

	public enum ScreenUI{
		MainMenuUI,
		CharactersUI,
		PlaytimeUI,
		ShareUI
	};

	public enum TriviaScreenUI{
		MainGameUI,
		GameOverUI
	};

	public enum MemoryScreenUI {
		GamePlayUI,
		GameOverUI
	};
}
