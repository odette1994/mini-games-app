﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class MainMenuButtonEvent : MonoBehaviour
{
    public AudioHandler _audio;
    public CharactersUI _char;

    private UIHandler _uiHandler;

    [SerializeField] private Toggle _homeToggle;


    // Use this for initialization
    private void Start()
    {
        _uiHandler = UIHandler.Instance;
    }

    // Update is called once per frame
    private void Update()
    {
    }

    private void ChangeScreen(Enum.ScreenUI newScreen)
    {
        if (newScreen == _uiHandler.CurrentScreen) return;

        if (newScreen == Enum.ScreenUI.ShareUI)
        {
            UIHandler.Instance.ShowConfirmAdultModal(true, () => 
            { 
                _homeToggle.isOn = true;
                _uiHandler.HideConfirmAdultModal();
            });
        }

        UIHandler.Instance.MenuID = GetMenuID(newScreen);
        Analytics.CustomEvent("MenuClick", new Dictionary<string, object>
            {
                {"Button", UIHandler.Instance.MenuID}
            }
        );
        MyAnalytics.Instance.LogEvent("MenuClick", $"Button", $"{UIHandler.Instance.MenuID}", 1);

        _uiHandler.ShowCanvas(newScreen);
        _char.StopEvents();
    }

    public void ToggleHome()
    {
        ChangeScreen(Enum.ScreenUI.MainMenuUI);
    }

    public void ToggleCharacters()
    {
        ChangeScreen(Enum.ScreenUI.CharactersUI);
    }
    public void TogglePlaytime()
    {
        ChangeScreen(Enum.ScreenUI.PlaytimeUI);
    }
    public void ToggleShare()
    {
        ChangeScreen(Enum.ScreenUI.ShareUI);
    }

    private string GetMenuID(Enum.ScreenUI screen)
    {
        switch (screen)
        {
            case Enum.ScreenUI.MainMenuUI:
                return "Home Click";
            case Enum.ScreenUI.CharactersUI:
                return "Home Characters";
            case Enum.ScreenUI.PlaytimeUI:
                return "Home Playtime";
            case Enum.ScreenUI.ShareUI:
                return "Home Share";
            default:
                throw new ArgumentOutOfRangeException(nameof(screen), screen, null);
        }
    }

    private void ResetVolume(bool change)
    {
        var vol = 0f;

        if (change)
            vol = 0.2f;
        else
            vol = 1f;

        _audio.source.volume = vol;
    }
}