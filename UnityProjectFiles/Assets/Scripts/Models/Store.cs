﻿using System;

namespace Models
{
	[Serializable]
	public class Store
	{
		private string name;
		private string storeUrl;
		private string iconUrl;
		private string code;

		public Store(JSONObject jsonObject) {
			this.name = jsonObject.GetField ("name").str;
			this.storeUrl = jsonObject.GetField ("store_url").str;
			this.code = jsonObject.GetField ("code").str;
			this.iconUrl = jsonObject.GetField ("icon_url").str;
		}

		public string GetName() {
			return this.name;
		}

		public string GetStoreUrl() {
			return this.storeUrl;
		}

		public string GetIconUrl() {
			return this.iconUrl;
		}

		public string GetCode() {
			return this.code;
		}
	}
}

