﻿using UnityEngine;
using System;
using UnityEngine.UI;

public class ManifestInfo : MonoBehaviour
{
    public Action OnVersionUpdated;
    public string BuildVersion = "NotAssigned";
    public string CommitId;

    [System.Serializable]
    public class UnityCloudBuildManifestData
    {
        public string scmCommitId;
        public string scmBranch;
        public string buildNumber;
        public string buildStartTime;
        public string projectId;
        public string bundleId;
        public string unityVersion;
        public string xcodeVersion;
        public string cloudBuildTargetName;
    }

    public void Start()
    {
        var manifest = (TextAsset)Resources.Load("UnityCloudBuildManifest.json");
        if (manifest != null)
        {
            var data = JsonUtility.FromJson<UnityCloudBuildManifestData>(manifest.text);
            if (data != null)
            {
                BuildVersion = data.buildNumber;
                CommitId = data.scmCommitId;
                OnVersionUpdated?.Invoke();
            }
        }
    }
}