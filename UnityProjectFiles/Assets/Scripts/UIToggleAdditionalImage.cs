﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIToggleAdditionalImage : MonoBehaviour
{
    [SerializeField] private Sprite _toggleOn;
    [SerializeField] private Sprite _toggleOff;
    [SerializeField] Image _image;

    private Toggle _myToggle;

    private void Awake()
    {
        _myToggle = GetComponent<Toggle>();
    }

    private void Start()
    {
        _myToggle.onValueChanged.AddListener(OnToggleValueChanged);
        OnToggleValueChanged(_myToggle.isOn);
    }

    private void OnToggleValueChanged(bool newValue)
    {
        if(newValue)
        {
            _image.enabled = true;
            _image.preserveAspect = true;
            _image.sprite = _toggleOn;
        }
        else
        {
            if (_toggleOff != null)
            {
                _image.sprite = _toggleOff;
            }
            else
                _image.enabled = false;
        }
    }
}
