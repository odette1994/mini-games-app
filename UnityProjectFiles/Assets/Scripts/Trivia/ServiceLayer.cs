using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Models;
using Models.Trivia;
using System.Net;

public class ServiceLayer : MonoBehaviour {
	public static ServiceLayer service;
	private bool ret = false;
	private bool isSaveToCache = false;
	[SerializeField] Canvas loading;
	[SerializeField] TriviaController triviaController ;

	public ServiceLayer() {}

	public static ServiceLayer Instance
	{
		get 
		{
			if (service == null)
			{
				service = new ServiceLayer();
			}
			return service;
		}
	}

	void Start()
	{
		SetCanvas (loading, false);
		triviaController.Run ();
	}

	/**
	 * Fetch questions and settings at once
	 * 
	 */
	public IEnumerator FetchQuestion () {

		ret = false;

		string baseURL = ApiUrl.TRIVIA;
		HTTP.Request requestIns = new HTTP.Request ("get",baseURL);
		requestIns.Send((request) =>{
			
			JSONObject jsonResponse = new JSONObject(request.response.Text);

			// question
			List <QuestionObj> questionIns = QuestionObj.FormatQuestions(jsonResponse);
//			StartCoroutine(Save(questionIns));
			ret = Save(questionIns);
			//settings
			SaveSettings(jsonResponse.GetField("settings"));
		});
			
	
		Debug.Log ("Done save: " + ret);

		yield return new WaitUntil(() => ret == true);
		Debug.Log ("Done save: " + ret);
		SetCanvas (loading, false);

		triviaController.Run ();
	}
		
	/**
	 * Save trivia question/answer to file
	 * 
	 */
	public Boolean Save(List<QuestionObj> arrList){
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + FileName.TRIVIA_QUESTION);
		int x;
		for (x = 0; x < arrList.Count; x++) {
			QuestionObj questions = (QuestionObj)arrList [x];

			// save the audio
			if (!String.IsNullOrEmpty(questions.audioFilename)) {
				if (!File.Exists (Application.persistentDataPath + "/" + questions.audioFilename)) {
					StartCoroutine (SaveAudioToCache(WWW.UnEscapeURL(questions.audioUrl), questions.audioFilename));
//					yield return new WaitUntil (() => isSaveToCache == true);
				}

			}

			bf.Serialize (file, questions);
		}
		file.Close ();


		if (x >= arrList.Count) {
			ret = true;
		} else {
			ret = false;
		}

		return ret;
	}

	/**
	 * Save trivia settings to file
	 * 
	 */ 
	void SaveSettings(JSONObject jsonSettings)
	{
		BinaryFormatter bf = new BinaryFormatter ();
		FileStream file = File.Create (Application.persistentDataPath + FileName.TRIVIA_SETTING);

		TriviaSetting triviaSettings = new TriviaSetting ();
		triviaSettings.timer = int.Parse(jsonSettings.GetField ("timer").str);
		bf.Serialize (file, (TriviaSetting)triviaSettings);
		file.Close ();
	
	}

	public static bool CheckForInternetConnection(string url)
	{
		try
		{
			using (var client = new WebClient())
			{
				using (var stream = client.OpenRead(url))
				{
					return true;
				}
			}
		}
		catch
		{
			return false;
		}
	}

	/**
	 * Save the question audio in cache
	 * 
	 */ 
	IEnumerator SaveAudioToCache(string audioUrl, string audioFilename) {
		isSaveToCache = false;
		WWW www = new WWW (audioUrl);
		yield return www;

		File.WriteAllBytes(Application.persistentDataPath + "/" + audioFilename, www.bytes);

		Debug.Log (audioFilename + " writing: " + www.isDone);
		isSaveToCache = www.isDone;
		yield return new WaitUntil (() => isSaveToCache == true);

	}

	void SetCanvas(Canvas canvas, bool active) {
		canvas.gameObject.SetActive (active);
	}

}
