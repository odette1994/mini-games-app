﻿using System;
using System.Collections.Generic;
using Models;
using UnityEngine;
using UnityEngine.Analytics;
using UnityEngine.UI;

public class UIHandler : MonoBehaviour
{
    //Singleton
    private static UIHandler instance;

    private IList<Canvas> _canvas;
    private Canvas _comingSoon;
    private Canvas _confirmAdultModal;
    private Canvas _confirmModal;
    private Canvas _email;
    private Canvas _help;
    private Canvas _helpPuzzle;
    private Canvas _helpPuzzleFirstTime;
    [HideInInspector] public ModalManager _m;
    private Canvas _memory;
    [HideInInspector] public ModalManager _mm;
    private Canvas _modalLoading;

    private Canvas _modalNoInternet;
    private Canvas _puzzle;
    private Canvas _puzzleSelection;
    private Canvas _quitModal;
    public Enum.ScreenUI CurrentScreen { get; private set; }
    private AudioClip _tapSound;
    private Canvas _trivia;
    private Canvas _unlocklevel;

    //Variables

    [HideInInspector] public Button btnHome;
    [HideInInspector] public Button btnPlaytime;
    private string CardID;
    public string MenuID;

    //Construct
    private UIHandler()
    {
    }

    public static UIHandler Instance
    {
        get
        {
            if (instance == null)
                instance = FindObjectOfType(typeof(UIHandler)) as UIHandler;

            return instance;
        }
    }

    // Use this for initialization
    private void Awake()
    {
        //DontDestroyOnLoad (this);
        Input.multiTouchEnabled = true;
        Screen.sleepTimeout = SleepTimeout.NeverSleep;

        CurrentScreen = Enum.ScreenUI.MainMenuUI;

        _canvas = new List<Canvas>();

        //NOTE: 
        //Add new canvases here
        //Then update Enum.cs
        _canvas.Add(GameObject.Find("MainMenuUI").GetComponent<Canvas>());
        _canvas.Add(GameObject.Find("CharactersUI").GetComponent<Canvas>());
        _canvas.Add(GameObject.Find("PlaytimeUI").GetComponent<Canvas>());
        _canvas.Add(GameObject.Find("ShareUI").GetComponent<Canvas>());

        //Hide all canvases aside from MainMenuUI
        for (var i = 1; i < _canvas.Count; i++) _canvas[i].gameObject.SetActive(false);


        _comingSoon = GameObject.Find("Modal").GetComponent<Canvas>();
        _trivia = GameObject.Find("TriviaUI").GetComponent<Canvas>();
        _quitModal = GameObject.Find("QuitModal").GetComponent<Canvas>();
        _memory = GameObject.Find("MemoryGameUI").GetComponent<Canvas>();
        _puzzle = GameObject.Find("PuzzleGameUI").GetComponent<Canvas>();
        _email = GameObject.Find("EmailComposer").GetComponent<Canvas>();
        _confirmModal = GameObject.Find("ConfirmModal").GetComponent<Canvas>();
        _help = GameObject.Find("Help").GetComponent<Canvas>();
        _helpPuzzle = GameObject.Find("HelpPuzzle").GetComponent<Canvas>();
        _modalNoInternet = GameObject.Find("ModalNoInternet").GetComponent<Canvas>();
        _modalLoading = GameObject.Find("ModalLoading").GetComponent<Canvas>();
        _confirmAdultModal = GameObject.Find("ConfirmAdultModal").GetComponent<Canvas>();
        _puzzleSelection = GameObject.Find("PuzzleGameSelectionUI").GetComponent<Canvas>();
        _unlocklevel = GameObject.Find("UnlockLevel").GetComponent<Canvas>();
        _helpPuzzleFirstTime = GameObject.Find("HelpPuzzleFirstTime").GetComponent<Canvas>();

        _tapSound = Resources.Load<AudioClip>("SFX/tap_button");

        ShowModal(_comingSoon, false);
        ShowModal(_trivia, false);
        ShowModal(_quitModal, false);
        ShowModal(_memory, false);
        ShowModal(_email, false);
        ShowModal(_confirmModal, false);
        ShowModal(_helpPuzzle, false);
        ShowModal(_help, false);
        ShowModal(_modalNoInternet, false);
        ShowModal(_modalLoading, false);
        ShowModal(_confirmAdultModal, false);
        ShowModal(_puzzle, false);
        ShowModal(_puzzleSelection, false);
        ShowModal(_unlocklevel, false);
        ShowModal(_helpPuzzleFirstTime, false);

        OnLoadShowMoviesOrCharacters();

        // rate me
        // check if we gonna show the review
        // if the app version has change then we need to show again the review pop up
        if (PlayerPrefs.HasKey("appVersion"))
        {
            if (PlayerPrefs.GetString("appVersion").Equals(Config.AppVersion))
            {
                // TODO
            }
            else
            {
                PlayerPrefs.SetInt("canShowReview", 1);
                PlayerPrefs.SetInt("reviewCtr", 1);
            }
        }
        else
        {
            PlayerPrefs.SetInt("canShowReview", 1);
        }

        PlayerPrefs.SetString("appVersion", Config.AppVersion);

        if (PlayerPrefs.HasKey("canShowReview"))
        {
            if (PlayerPrefs.GetInt("canShowReview") == 1)
            {
                if (PlayerPrefs.HasKey("reviewCtr"))
                {
                    if (PlayerPrefs.GetInt("reviewCtr") >= 3)
                    {
                        Debug.Log("Rate PLUGIN DISABLED. Not compatible with ARM64");
                        /*var ratePopUp = new MobileNativeRateUs("Enjoying The Swan Princess?",
                            "If you love our app take a moment to rate it in the App Store. Thanks for your support!",
                            "Rate it Now",
                            "Remind Me Later",
                            "No, Thanks");

                        ratePopUp.SetAppleId("1109569858");
                        ratePopUp.SetAndroidAppUrl("market://details?id=" + Application.identifier);

                        ratePopUp.OnComplete += OnRatePopUpClose;

                        ratePopUp.Start();*/
                    }

                    PlayerPrefs.SetInt("reviewCtr", PlayerPrefs.GetInt("reviewCtr") + 1);
                }
                else
                {
                    PlayerPrefs.SetInt("reviewCtr", 1);
                }
            }
        }
        else
        {
            PlayerPrefs.SetInt("canShowReview", 1);
        }

        // end rate me
    }

    /**
	 * Call back for rate me
	 */
    /*private void OnRatePopUpClose(MNDialogResult result)
    {
        //parsing result
        switch (result)
        {
            case MNDialogResult.RATED:
                PlayerPrefs.SetInt("canShowReview", 0);
                PlayerPrefs.SetInt("reviewCtr", 1);
                break;
            case MNDialogResult.REMIND:
                PlayerPrefs.SetInt("reviewCtr", 1);
                break;
            case MNDialogResult.DECLINED:
                PlayerPrefs.SetInt("canShowReview", 0);
                PlayerPrefs.SetInt("reviewCtr", 1);
                break;
        }
    }*/

    public void ShowCanvas(Enum.ScreenUI canvasEnum)
    {
        if (CurrentScreen != canvasEnum)
        {
            _canvas[(int) canvasEnum].gameObject.SetActive(true);

            _canvas[(int) CurrentScreen].gameObject.SetActive(false);
            CurrentScreen = canvasEnum;
        }
    }

    private void OnLoadShowMoviesOrCharacters()
    {
        if (PlayerPrefs.GetInt("toShowScreen").ToString() == "1")
        {
            btnHome.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Common/menu_home");
            btnPlaytime.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Common/menu_playtime_active");
            //btnCharacters.GetComponentInChildren<Image> ().sprite = Resources.Load<Sprite> ("Common/menu_characters");
            ShowCanvas(Enum.ScreenUI.PlaytimeUI);
            SetShowMovie(0);
        }

        if (PlayerPrefs.GetInt("toShowScreen").ToString() == "2")
        {
            btnHome.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Common/menu_home");
            btnPlaytime.GetComponentInChildren<Image>().sprite = Resources.Load<Sprite>("Common/menu_playtime_active");
            ShowCanvas(Enum.ScreenUI.PlaytimeUI);
            ShowModal(_puzzleSelection, true);
            SetShowMovie(0);
        }
    }

    private void SetShowMovie(int value)
    {
        PlayerPrefs.SetInt("toShowScreen", value);
    }

    public void ShowModal(Canvas canvas, bool show)
    {
        canvas.gameObject.SetActive(show);
    }

    public void ShowComingSoon(bool toShow)
    {
        _comingSoon.gameObject.SetActive(toShow);
    }

    public void ShowEmail(bool toShow)
    {
        _email.gameObject.SetActive(toShow);
    }

    public void ShowPuzzleSelection(bool show)
    {
        _puzzleSelection.gameObject.SetActive(show);
    }

    public void ShowUnlockLevel(bool show)
    {
        _unlocklevel.gameObject.SetActive(show);
    }

    public void ShowConfirmModal(bool show)
    {
        _confirmModal.gameObject.SetActive(show);

        if (!show)
            _mm.BackToFirstPage();
    }
    
    public void HelpPuzzleModal(string name)
    {
        if (name.Equals("puzzle")) ShowHelpPuzzleModal(true);

        if (name.Equals("first_time_player")) ShowHelpPuzzleFirstTime(true);
    }

    public void ShowHelpPuzzleFirstTime(bool show)
    {
        _helpPuzzleFirstTime.gameObject.SetActive(show);
    }

    public void HelpModal(string name)
    {
        var title = "HELP";
        var subtitle = "";

        var txtTitle = _help.transform.Find("Title").GetComponent<Text>();
        var txtSubtitle = txtTitle.transform.Find("Subtitle").GetComponentInChildren<Text>();


        if (name.Equals("trivia"))
            subtitle = "(1) The game shows you a question and four possible answers."
                       + "\n(2) Pick the right one to score points and continue playing."
                       + "\n(3) Fail, and you'll lose points!";
        else if (name.Equals("memory"))
            subtitle = "(1) Tap the cards to flip them."
                       + "\n(2) The goal of the game is to match all cards."
                       + "\n(3) Try to do it with the lowest number of fails.";

        //if (txtTitle.name.Equals("Title"))
        txtTitle.text = title;


//		if (txtSubtitle.name.Equals ("Subtitle"))
        txtSubtitle.text = subtitle;

        ShowHelpModal(true);


        title = "";
        subtitle = "";
    }

    public void ShowHelpPuzzleModal(bool show)
    {
        _helpPuzzle.gameObject.SetActive(show);
    }

    public void ShowHelpModal(bool show)
    {
        _help.gameObject.SetActive(show);
    }

    public void ShowModalNoInternet(bool show)
    {
        _modalNoInternet.gameObject.SetActive(show);
    }

    public void ShowModalLoading(bool show)
    {
        _modalLoading.gameObject.SetActive(show);
    }

    public void ShowConfirmAdultModal(bool show, Action onClose)
    {
        _confirmAdultModal.gameObject.SetActive(show);
        _confirmAdultModal.GetComponent<AdultModal>().OnNoPressed = onClose;
    }

    public void HideConfirmAdultModal()
    {
        _confirmAdultModal.gameObject.SetActive(false);
    }

    public void PlaySFX()
    {
        var source = GameObject.Find("Controls").GetComponent<AudioSource>();

        source.PlayOneShot(_tapSound, 1f);
    }

    /**
	 * public function to call when setting the menu from anywhere in the app
	 * 
	 */
    public void SetButtonActive(Enum.ScreenUI canvasEnum)
    {
        var imageList = GameObject.Find("Panel/MenuPanel").GetComponentsInChildren<Image>();

        for (var x = 0; x < imageList.Length; x++) //Debug.Log (imageList[x].name);
            if (imageList[x].name == "BtnCharacters")
            {
                if (canvasEnum == Enum.ScreenUI.CharactersUI)
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_characters_active");
                else
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_characters");
            }
            else if (imageList[x].name == "BtnShare")
            {
                if (canvasEnum == Enum.ScreenUI.ShareUI)
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_share_active");
                else
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_share");
            }
            else if (imageList[x].name == "BtnPlaytime")
            {
                if (canvasEnum == Enum.ScreenUI.PlaytimeUI)
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_playtime_active");
                else
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_playtime");
            }
            else if (imageList[x].name == "BtnHome")
            {
                if (canvasEnum == Enum.ScreenUI.MainMenuUI)
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_home_active");
                else
                    imageList[x].sprite = Resources.Load<Sprite>("Common/menu_home");
            }
    }
}