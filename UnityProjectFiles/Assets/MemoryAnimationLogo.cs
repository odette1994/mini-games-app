﻿using UnityEngine;
using System.Collections;

public class MemoryAnimationLogo : MonoBehaviour {

	// Use this for initialization
	void Start () {
		

	}

	void AnimateStar()
	{
		iTween.ScaleFrom (GameObject.Find ("MemoryGameUI/Logo/MemoryLogoStar"), new Vector3 (1, 1, 1),10.0f);
		StartCoroutine (AnimateSecondStar ());
	}

	IEnumerator AnimateSecondStar()
	{
		yield return new WaitForSeconds(20.0f);
		iTween.ScaleFrom (GameObject.Find ("MemoryGameUI/Logo/MemoryLogoStar2"), new Vector3 (1, 1, 1),10.0f);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnDisable() {
		CancelInvoke ();
	}

	void OnEnable() {
		InvokeRepeating ("AnimateStar",0, 15.0f);
	}
}
