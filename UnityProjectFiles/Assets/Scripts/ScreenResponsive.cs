﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScreenResponsive : MonoBehaviour {

	// Use this for initialization
	void Start () {
		float f = (float)Screen.width / (float)Screen.height;
		int i = 0;
		while(true){
			i++;
			if(System.Math.Round(f * i, 2) == Mathf.RoundToInt(f * i))
				break;
		}
		Vector2 aspectRatio = new Vector2((float)System.Math.Round(f * i, 2), i);

		if (aspectRatio.x != 4 && aspectRatio.y !=3) {
			RectTransform container = gameObject.GetComponent<RectTransform> ();
			container.offsetMax = new Vector2 (3, -0.5160761f);

			Image[] children = container.GetComponentsInChildren<Image> ();

			for (var x = 0; x < children.Length; x++) {
				
				//menuBtn [x].rectTransform.offsetMax = new Vector2 (oldWidth, 250);
				if (children [x].name == "BtnCharacters"
					|| children [x].name == "BtnShare"
					|| children [x].name == "BtnPlaytime"
					|| children [x].name == "BtnHome") {

					float oldWidth = children [x].rectTransform.offsetMax.x;
					children [x].rectTransform.offsetMax = new Vector2 (oldWidth, 280);
				}

				if (children [x].name == "Vines") {
					float oldWidth = children [x].rectTransform.offsetMin.x; 
					//float oldRight = children [x].rectTransform.offsetMax.x;
					//float oldHeight = children [x].rectTransform.offsetMax.y;

					children [x].rectTransform.localPosition = new Vector2 (oldWidth, 105);
					//children [x].rectTransform.offsetMax = new Vector2 (oldRight, oldHeight);
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
