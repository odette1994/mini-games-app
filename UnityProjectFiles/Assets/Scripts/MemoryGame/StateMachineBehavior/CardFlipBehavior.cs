﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CardFlipBehavior : StateMachineBehaviour {

	public Button button;
	public Sprite image;

	private Button clone;
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		
	}
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		button.image.sprite = image;

	}
}
