﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class AnimationController : MonoBehaviour
{

    [HideInInspector] public bool hasEnded;
    [HideInInspector] public bool hasAnimEnded;
    public Animator logo;
    public Animator buttonClose;
    public Animator headerBar;

    void Awake()
    {
        hasEnded = false;
        hasAnimEnded = false;
    }

    public IEnumerator WriteText(Text obj, string text, float speed)
    {
        if (!hasEnded)
        {
            string gradualText = "";
            for (int i = 0; i < text.Length; i++)
            {
                gradualText += text[i];
                obj.text = gradualText + " ";
                yield return new WaitForSeconds(speed);
            }
            obj.text = text;
            hasEnded = true;
        }

    }

    public IEnumerator MoveButtons(Animator[] anim, bool start, float speed)
    {
        for (int i = 0; i < anim.Length; i++)
        {
            anim[i].SetBool("hasStarted", start);
            yield return new WaitForSeconds(speed);
        }
    }

    public void OnAnimatorTrigger()
    {
        bool active = logo.GetBool("activate");
        logo.SetBool("activate", !active);
        buttonClose.SetBool("activate", !active);
    }
}
