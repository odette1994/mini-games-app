﻿using System;

namespace Models.Puzzles
{
	[Serializable]
	public class Question
	{
		private float id;
		private bool isUnlock;
		private string badge;
		private string grid;
		private string imagePath;
		private string row;
		private string column;
		private string answer;
		private string audioPath;
		private string question;
		private string answerAudioPath;

		private float firstBadgeTime;
		private float secondBadgeTime;
		private float thirdBadgeTime;
		private float fourthBadgeTime;

		private string firstBadgeMessage;
		private string secondBadgeMessage;
		private string thirdBadgeMessage;
		private string fourthBadgeMessage;

		public Question (JSONObject jsonObject)
		{
			this.id = jsonObject.GetField ("id").n;
			this.isUnlock = jsonObject.GetField ("is_unlock").b;
			this.badge = jsonObject.GetField ("badge").str;
			this.grid = jsonObject.GetField ("grid").str;
			this.imagePath = jsonObject.GetField ("image_path").str;
			this.row = jsonObject.GetField ("row").str;
			this.column = jsonObject.GetField ("column").str;
			this.audioPath = jsonObject.GetField ("audio_path").str;
			this.answer = jsonObject.GetField ("answer").str;
			this.question = jsonObject.GetField ("question").str;

			if (!jsonObject.GetField("answer_audio_path").IsNull) {
				this.answerAudioPath = jsonObject.GetField ("answer_audio_path").str;
			}

			if (jsonObject.HasField ("first_badge_time")) {
				this.firstBadgeTime = jsonObject.GetField ("first_badge_time").n;
			} else {
				this.firstBadgeTime = 60;
			}

			if (jsonObject.HasField ("second_badge_time")) {
				this.secondBadgeTime = jsonObject.GetField ("second_badge_time").n;
			} else {
				this.secondBadgeTime = 70;
			}

			if (jsonObject.HasField ("third_badge_time")) {
				this.thirdBadgeTime = jsonObject.GetField ("third_badge_time").n;
			} else {
				this.thirdBadgeTime = 75;
			}

			if (jsonObject.HasField ("fourth_badge_time")) {
				this.fourthBadgeTime = jsonObject.GetField ("fourth_badge_time").n;
			} else {
				this.fourthBadgeTime = 80;
			}

			this.firstBadgeMessage = jsonObject.GetField ("first_badge_message").str;
			this.secondBadgeMessage = jsonObject.GetField ("second_badge_message").str;
			this.thirdBadgeMessage = jsonObject.GetField ("third_badge_message").str;
			this.fourthBadgeMessage = jsonObject.GetField ("fourth_badge_message").str;
		}

		public float GetId() {
			return this.id;
		}

		public bool GetIsUnlock() {
			return this.isUnlock;
		}

		public string GetBadge() {
			return this.badge;
		}

		public void SetIsUnlock(bool isUnlock) {
			this.isUnlock = isUnlock;
		}

		public void SetBadge(string badge) {
			this.badge = badge;
		}

		public string GetGrid() {
			return this.grid;
		}

		public string GetImagePath() {
			return this.imagePath;
		}

		public string GetAudioPath() {
			return this.audioPath;
		}

		public string GetAnswer() {
			return this.answer;
		}

		public string GetQuestion() {
			return this.question;
		}

		public string GetGridRow() {
			return this.row;
		}

		public string GetGridColumn() {
			return this.column;
		}

		public float GetFirstBadgeTime() {
			return this.firstBadgeTime;
		}

		public float GetSecondBadgeTime() {
			return this.secondBadgeTime;
		}

		public float GetThirdBadgeTime() {
			return this.thirdBadgeTime;
		}

		public float GetFourthBadgeTime() {
			return this.fourthBadgeTime;
		}

		public string GetFirstBadgeMessage() {
			return this.firstBadgeMessage;
		}

		public string GetSecondBadgeMessage() {
			return this.secondBadgeMessage;
		}

		public string GetThirdBadgeMessage() {
			return this.thirdBadgeMessage;
		}

		public string GetFourthBadgeMessage() {
			return this.fourthBadgeMessage;
		}

		public string GetAnswerAudioPath() {
			return this.answerAudioPath;
		}
	}
}

