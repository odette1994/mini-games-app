﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;
using Models;
using System.Net;
using System.Text.RegularExpressions;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class MiniGameController : MonoBehaviour 
{
	[HideInInspector]public AudioHandler sound;
	[HideInInspector]public Canvas trivia;
	[HideInInspector]public GameObject crown;
	[HideInInspector]public GameObject modal;
	[HideInInspector]public Canvas memory;
	[HideInInspector]public Canvas email;
	[HideInInspector]public Canvas puzzle;
	[HideInInspector]public Canvas puzzleSelection;

	[HideInInspector]public InputField emailField;
	[HideInInspector]public InputField nameField;
	[HideInInspector]public Text msgField;
	[HideInInspector]public InputField friendsNameField;

	[HideInInspector]public Text errorMsg;
	[HideInInspector]public Text errorText1;
	[HideInInspector]public Text errorText2;
	[HideInInspector]public Text errorText3;

	[HideInInspector]public Button sendBtn;
	[HideInInspector]public Button closeBtn;
	[HideInInspector]public Image loading;
	[HideInInspector]public Image msgLoading;

	public AudioClip sentSound;
	private AudioSource audioSource;

	string sGameName;

	Text score;

	FadeInOut fade;	
	UIHandler _uiHandler;

	private const string MatchEmailPattern =
		@"^(([\w-]+\.)+[\w-]+|([a-zA-Z]{1}|[\w-]{2,}))@"
            + @"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\."
              + @"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\.([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
            + @"([a-zA-Z]+[\w-]+\.)+[a-zA-Z]{2,4})$";

	void Awake() {

		bool firstTimer;

		firstTimer = (PlayerPrefs.GetInt ("earned_score") <= 0) ? true : false;

		crown.SetActive (!firstTimer);

		score = crown.GetComponentInChildren<Text> ();
		score.text = PlayerPrefs.GetInt ("earned_score").ToString();

	}

	void Start() {

		_uiHandler = UIHandler.Instance;

		fade = GameObject.Find ("Fade").GetComponent<FadeInOut> ();
		
		// set audio
		audioSource = GetComponent<AudioSource> ();
	}
		
	void GetMovies() {

	}

	public void OnSelectMenu(string game) {

		Canvas selected = null;

		StartCoroutine (fade.FadeToClear());
		//sound.SetMute (true);

		switch(game) {

		case "trivia":
			sound.PlayMainMenuMusic (false);

			sGameName = "Trivia Game";
			/* Analytics For Games
					 * Game: Which game screen am I?
					 */
			Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
				{ "Game", sGameName }
			});
			MyAnalytics.Instance.LogEvent("PlayClick", "Game", $"{sGameName}", 1);
			selected = trivia;
			break;

		case "puzzle":
			sound.PlayMainMenuMusic (false);

			sGameName = "Puzzle Game";
			/* Analytics For Games
					 * Game: Which game screen am I?
					 */
			Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
				{ "Game", sGameName }
			}
			);
			MyAnalytics.Instance.LogEvent("PlayClick", "Game", $"{sGameName}", 1);
			selected = puzzle;
			break;

		case "memory":
			sound.PlayMainMenuMusic (false);

			sGameName = "Memory Game";
			/* Analytics For Games
					 * Game: Which game screen am I?
					 */
			Analytics.CustomEvent ("PlayClick", new Dictionary<string, object> {
				{ "Game", sGameName }
			}
			);
			MyAnalytics.Instance.LogEvent("PlayClick", "Game", $"{sGameName}", 1);
			selected = memory;
			break;

		case "email":
//			sound.PlayMainMenuMusic (true);
			selected = email;
			errorMsg.gameObject.SetActive (false);
			errorText1.gameObject.SetActive (false);
			errorText2.gameObject.SetActive (false);
			errorText3.gameObject.SetActive (false);

			emailField.interactable = true;
			nameField.interactable = true;
//			msgField.interactable = true;
			friendsNameField.interactable = true;

			sendBtn.GetComponent<Button> ().interactable = true;
			sendBtn.GetComponentInChildren<Text> ().text = "Send";		

			emailField.text = "";
			nameField.text = "";
			friendsNameField.text = "";
			//msgField.text="";
			msgLoading.gameObject.SetActive (true);
			if (CheckForInternetConnection (ApiUrl.BASE)) {
				StartCoroutine (GetFromApi ());
			} 
			else {
				msgLoading.gameObject.SetActive(false);
				msgField.text = "Be a part of the adventure of being a Princess or a Prince in the Swan Princess Series. Share it with your friends and families!";
			}
			loading.gameObject.SetActive (false);
			break;
		default:
			break;

		}

		_uiHandler.ShowModal (selected,true);

	}

	public void OnLoadGame(string sceneName) {
//		StartCoroutine (fade.FadeToClear());
		SceneManager.LoadScene (sceneName);
	}


	public void ShowScreen(bool show) {
		modal.SetActive (show);
	}

	public void HideCanvas() {
		StartCoroutine (fade.FadeToClear());

		ShowScreen (false);
		string sGameName;
		Canvas canvas = null;
		sGameName = "";

		if (trivia.gameObject.activeInHierarchy) {
			canvas = trivia;
			sGameName = "Trivia Game";
		}

		if (memory.gameObject.activeInHierarchy) {
			canvas = memory;
			sGameName = "Memory Game";
		}

		if (email.gameObject.activeInHierarchy) {
			canvas = email;
			sGameName = "email";
		}

		if (puzzle.gameObject.activeInHierarchy) {
			canvas = puzzle;
			sGameName = "Puzzle Game";
		}

		if (puzzleSelection.gameObject.activeInHierarchy) {
			puzzleSelection.gameObject.SetActive (false);
			canvas = puzzle;
		}


		Analytics.CustomEvent ("CloseClick", new Dictionary<string, object> {
			{ "Game", sGameName }
		}
		);
		MyAnalytics.Instance.LogEvent("CloseClick", "Game", $"{sGameName}", 1);

		_uiHandler.ShowModal (canvas,false);
	}

	public void sendEmailPress() {

		if (CheckForInternetConnection (ApiUrl.BASE)) {
			errorMsg.gameObject.SetActive (false);

			sendEmail();
		} else {
			UIHandler.Instance.ShowModalNoInternet (true);
		}
	}
		
	public static bool IsEmail(string email)
	{
		if (email != null) return Regex.IsMatch(email, MatchEmailPattern);
		else return false;
	}	


//	IEnumerator sendEmail(){
	public void sendEmail(){
		
		if (emailField.text.Length != 0
			&& nameField.text.Length != 0 
//			&& msgField.text.Length != 0
			&& friendsNameField.text.Length!= 0) {
			
			if (IsEmail (emailField.text)) {
				errorText1.gameObject.SetActive (false);
				errorText2.gameObject.SetActive (false);
				errorText3.gameObject.SetActive (false);

				emailField.interactable = false;
				nameField.interactable = false;
//				msgField.interactable = false;
				friendsNameField.interactable = false;

				sendBtn.GetComponent<Button>().interactable = false;
				closeBtn.GetComponent<Button>().interactable = false;

				sendBtn.GetComponentInChildren<Text>().text = "Sending";		
				loading.gameObject.SetActive (true);

				WWWForm form = new WWWForm ();
				form.AddField ("to_email", emailField.text);
				form.AddField ("from_name", nameField.text);
				form.AddField ("message", msgField.text);
				form.AddField ("to_name", friendsNameField.text);


//				HTTP.Request someRequest = new HTTP.Request( "post", ApiUrl.EMAIL, form );
//				someRequest.Send( ( request ) => {
//					// parse some JSON, for example:
//					JSONObject jsonResponse = new JSONObject(request.response.Text);
//					string resultStr=jsonResponse.GetField("message").str;
//					Debug.Log("resultStr" + resultStr);
//
//					if(resultStr=="Mail sent")
//					{
//						Debug.Log ("Success");
//						closeBtn.GetComponent<Button>().interactable = true;
//						Debug.Log ("After Success");
//						
//						errorMsg.gameObject.SetActive (true);
//						errorMsg.text="Email Sent";
//						audioSource.PlayOneShot(sentSound, 2f);
//
//						bool bFields=true;
//						/* Analytics For SMS
//					 * AllFieldsFilled: Whas that a valid message sent?
//					 */
//						Analytics.CustomEvent ("MessageSentClick", new Dictionary<string, object> {
//							{ "AllFieldsFilled", bFields }
//						}
//						);
//
//						loading.gameObject.SetActive (false);
//						StartCoroutine (showSentMsg ());
//
//					}else
//					{
//						closeBtn.interactable = true;
//
//					}
//				
//				});

				// submit form
				StartCoroutine (SubmitEmailInvite(form));

			} else {
				Debug.Log ("invalid email");

				errorMsg.gameObject.SetActive (true);
				errorMsg.text="Email Invalid Format";
			}


		} else {
			Debug.Log ("empty");
			if (emailField.text.Length != 0) {
				errorText1.gameObject.SetActive (false);
			} else {
				errorText1.gameObject.SetActive (true);
			}

			if (nameField.text.Length != 0) {
				errorText2.gameObject.SetActive (false);
			} else {
				errorText2.gameObject.SetActive (true);
			}

			if (friendsNameField.text.Length != 0) {
				errorText3.gameObject.SetActive (false);
			} else {
				errorText3.gameObject.SetActive (true);
			}


		}
	}

	/**
	 * Submit the invitation form
	 * 
	 */ 
	IEnumerator SubmitEmailInvite(WWWForm form) {
		loading.gameObject.SetActive (true);

		WWW w = new WWW (ApiUrl.EMAIL, form);
		yield return w;

		loading.gameObject.SetActive (false);

		if (!string.IsNullOrEmpty (w.error)) {

			emailField.interactable = true;
			nameField.interactable = true;
//			msgField.interactable = true;
			friendsNameField.interactable = true;

			sendBtn.GetComponent<Button>().interactable = true;
			closeBtn.GetComponent<Button>().interactable = true;

			sendBtn.GetComponentInChildren<Text>().text = "Send";

			UIHandler.Instance.ShowModalNoInternet (true);
		} else {
			
			closeBtn.GetComponent<Button>().interactable = true;

			errorMsg.gameObject.SetActive (true);
			errorMsg.text = "Email Sent";
			audioSource.PlayOneShot(sentSound, 2f);
			bool bFields = true;

			/* Analytics For SMS
			 * AllFieldsFilled: Whas that a valid message sent?
			 */
			Analytics.CustomEvent ("MessageSentClick", new Dictionary<string, object> {
					{ "AllFieldsFilled", bFields }
				}
			);
			MyAnalytics.Instance.LogEvent("MessageSentClick", "AllFieldFilled", $"{bFields}", 1);

			StartCoroutine (showSentMsg ());
		}
	}

	public static bool CheckForInternetConnection(string url)
	{
		try
		{
			using (var client = new WebClient())
			{
				using (var stream = client.OpenRead(url))
				{
					return true;
				}
			}
		}
		catch
		{
			return false;
		}
	}

	IEnumerator showSentMsg()
	{
		yield return new WaitForSeconds(2.0f);

		_uiHandler.ShowEmail(false);
	}

	/**
	 * Get and construct the qoutes msg
	 */ 
	private IEnumerator GetFromApi() { 
		string url =  ApiUrl.EMAIL_MESSAGE; //"http://swan.local/api/stores";

//		RectTransform loadingPanel = GameObject.FindGameObjectWithTag ("STORE_LOADING").GetComponent<RectTransform>();
//		loadingPanel.gameObject.SetActive (true);

		WWW www = new WWW(url);
		yield return www;

		JSONObject fetch = new JSONObject(www.text);
	
		if (!fetch.IsNull) {
			msgLoading.gameObject.SetActive(false);
			EmailMsg emailMsgIns = new EmailMsg(fetch.GetField ("results"));
			msgField.text = emailMsgIns.GetQoute();
		}

	
	}
}
