﻿using System;
using UnityEngine;
using System.Collections;

namespace Models
{
	public class Puzzle
	{
		private string id;
		private string name;
		private Vector2 position;
		private GameObject gameObject;

		public Puzzle (GameObject gameObject)
		{
			this.name = gameObject.name;
			RectTransform rectTransform = gameObject.GetComponent<RectTransform> ();

//			this.position = rectTransform.localPosition;
			this.position = gameObject.transform.position;
			this.gameObject = gameObject;
		}

		public string GetName() {
			return this.name;
		}

		public Vector2 GetPosition() {
			return this.position;
		}

		public GameObject GetGameObject() {
			return this.gameObject;
		}

	}
}

