﻿using UnityEngine;
using System.Collections;

public class AnimationManager : MonoBehaviour {

	public Animator gearImage;
	public Animator contentPanel;

	public void ToggleSetting() {
		bool isHidden = contentPanel.GetBool ("isHidden");
		contentPanel.SetBool ("isHidden",!isHidden);
		gearImage.SetBool ("isHidden",!isHidden);
	}
}
