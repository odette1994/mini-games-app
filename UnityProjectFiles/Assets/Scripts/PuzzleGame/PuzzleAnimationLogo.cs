﻿using UnityEngine;
using System.Collections;

public class PuzzleAnimationLogo : MonoBehaviour {

	// Use this for initialization
	void Start () {


	}

	void AnimateStar()
	{
		iTween.ScaleFrom (GameObject.Find ("PuzzleGameUI/Logo/PuzzleLogoStar"), new Vector3 (1, 1, 1),10.0f);
		StartCoroutine (AnimateSecondStar ());
	}

	IEnumerator AnimateSecondStar()
	{
		yield return new WaitForSeconds(20.0f);
		iTween.ScaleFrom (GameObject.Find ("PuzzleGameUI/Logo/PuzzleLogoStar2"), new Vector3 (1, 1, 1),10.0f);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnDisable() {
		CancelInvoke ();
	}

	void OnEnable() {
		InvokeRepeating ("AnimateStar",0, 15.0f);
	}
}
