﻿using System;
using UnityEngine;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Models.Trivia
{
	[Serializable]
	public class TriviaSetting
	{
		public int timer;

		public TriviaSetting ()
		{
		}

		public static int Timer() 
		{
			int timer = 3;
			if (File.Exists (Application.persistentDataPath + FileName.TRIVIA_SETTING)) {
				BinaryFormatter bf = new BinaryFormatter ();
				FileStream file = File.Open (Application.persistentDataPath + FileName.TRIVIA_SETTING, FileMode.Open);
				TriviaSetting settings = (TriviaSetting) bf.Deserialize (file);
				file.Close ();

				timer = settings.timer;
			}

			return timer;
		}
	}
}

