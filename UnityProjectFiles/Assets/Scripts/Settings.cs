﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;


public class Settings : MonoBehaviour 
{
	public Button btnSoundOff;
	public Button btnSoundOn;
	public Button btnSetting;

	private AudioHandler audioHandler;

	private Animator contentPanel;
	private Animator gear;


	private bool isHidden;

	void Start()
	{
		audioHandler = GameObject.Find("AudioHandler").GetComponent<AudioHandler> ();

		contentPanel = GameObject.Find ("Panel_Content").GetComponent<Animator> ();
		gear = GameObject.Find ("Img_Settings").GetComponent<Animator> ();

		btnSoundOff = contentPanel.transform.Find ("BtnMusicOff").GetComponent<Button> ();
		btnSoundOn = contentPanel.transform.Find ("BtnMusicOn").GetComponent<Button> ();
		btnSetting = contentPanel.GetComponentInParent<Button> ();

		btnSoundOn.gameObject.SetActive (false);

		//Buttons
		if (btnSoundOn)
			btnSoundOn.GetComponent<Button> ().onClick.AddListener (() => { OnSetMute (false); });

		if (btnSoundOff)
			btnSoundOff.GetComponent<Button> ().onClick.AddListener (() => { OnSetMute (true); });

		if (btnSetting) 
			btnSetting.GetComponent<Button> ().onClick.AddListener (() => { ToggleMenu (); });

		ShowButton ();

	}

	void AnimateButton() {

		if (contentPanel != null && gear != null) {
			isHidden = contentPanel.GetBool ("isHidden");
			contentPanel.SetBool ("isHidden",!isHidden);
			gear.SetBool ("isHidden",!isHidden);
		}

	}

	private void ToggleMenu() {

		AnimateButton ();

	}

	void ShowButton() {

		bool mute = audioHandler.ConvertToBool(PlayerPrefs.GetString("mute"));

		if (btnSoundOn != null && btnSoundOff != null) {

			if (mute) { 
				btnSoundOff.gameObject.SetActive (false);
				btnSoundOn.gameObject.SetActive (true);
			} 
			else{
				btnSoundOn.gameObject.SetActive (false);
				btnSoundOff.gameObject.SetActive (true);
			}

		}

	}

	private void OnSetMute(bool value)
	{
		audioHandler.SetMute (value);
		//IsHidden ();
		ShowButton();

	}
}
