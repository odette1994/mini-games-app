﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class FadeInOut : MonoBehaviour
{
	public Image overlay;
	float fadeTime = 1.0f;


	void Awake()
	{
		overlay.transform.localScale = new Vector2(Screen.width, Screen.height);

		StartCoroutine (FadeToClear());
	}


	public IEnumerator FadeToClear()
	{
		overlay.gameObject.SetActive (true);
		overlay.color = Color.black;

		float rate = 0.5f / fadeTime;

		float progress = 0.0f;

		while (progress < 1.0f) 
		{
			overlay.color = Color.Lerp (Color.black,Color.clear,progress);

			progress += rate * Time.deltaTime;

			yield return null;
		}

		overlay.color = Color.clear;
		overlay.gameObject.SetActive (false);
	}


}   