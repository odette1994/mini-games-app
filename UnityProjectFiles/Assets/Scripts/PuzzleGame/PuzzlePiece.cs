﻿using UnityEngine;
using UnityEngine.UI;

public class PuzzlePiece : MonoBehaviour
{
    [SerializeField] private RectTransform _middleContainer;
    [SerializeField] private Button _button;
    [SerializeField] private Image _image;
    [SerializeField] private RectTransform _imageTransform;

    private RectTransform _myRectTransform;

    public void InitializePiece(string name, Sprite imageSprite, PuzzleMatchingController controller,
        float pieceWidth, float pieceHeight, int xPos, int yPos, int xMax, int yMax)
    {
        _myRectTransform = GetComponent<RectTransform>();
        _imageTransform = _image.GetComponent<RectTransform>();

        this.name = name;
        _image.sprite = imageSprite;
        _image.SetNativeSize();

        _myRectTransform.sizeDelta = new Vector2(pieceWidth, pieceHeight);

        _myRectTransform.anchoredPosition = new Vector2(xPos * pieceWidth - (pieceWidth * xMax / 2), yPos * pieceHeight - (pieceHeight * yMax / 2));

        _imageTransform.anchoredPosition = new Vector2(xPos * pieceWidth * -1, yPos * pieceHeight * -1);
        _button.onClick.AddListener(delegate { StartCoroutine(controller.CheckMatch(gameObject)); });
    }
}
