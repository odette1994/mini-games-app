﻿using System;

namespace Models
{
	public class FileName
	{
		public const string TRIVIA_QUESTION = "/triviaQuestion1.dat";
		public const string TRIVIA_SETTING = "/triviaSettings.dat";

		public const string TRIVIA_AUDIO_FOLDER = "/trivia/audios/";

		public const string PUZZLE_USER_SCORES = "/puzzle-user-scores.dat";
		public const string PUZZLE_QUESTIONS = "/puzzle-questions.dat";

		public FileName ()
		{
		}
	}
}

