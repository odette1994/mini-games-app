﻿using UnityEngine;
using System.Collections.Generic;

public class CharactersUI : MonoBehaviour {

	public AudioHandler audioHandler;

	private List<AudioClip> charSFX = new List<AudioClip> ();
	private AudioSource _source;
	private List<int> counter = new List<int> ();
	private GameObject button;
	private float ctrFade = 0.3f;

	void Start () {
		_source = GetComponents<AudioSource> ()[1];
		AddSFX ();
	}

	void AddSFX () {

		foreach (object aud in Resources.LoadAll("SFX/Characters")) {
			charSFX.Add (aud as AudioClip);
		}

	}

	void OnEnable() {
		InvokeRepeating ("FadeInBackground", 0, 1.0f);
	}

	void OnDisable() {
		CancelInvoke ();
	}

	public void SetName(string name) {

		MakeSound (name);
		//Debug.Log (clickCounter++);
	}

	private void PlaySFX(int index) {

		if (_source.isPlaying) {
			StopEvents ();
		}

		ctrFade = 0.3f;
		_source.PlayOneShot (charSFX [index], 1.0f);
		audioHandler.SetAudioLevel (0.1f);
	}

	private void MakeSound(string charName) {

		for (int i = 0; i < charSFX.Count; i++) {

			if (charSFX[i].name.Contains(charName)) {
				counter.Add (i);
			}
		}
		PlaySFX (counter[Random.Range(0, counter.Count)]);
		counter.Clear ();

	}

	public void StopEvents () {
		_source.Stop ();
		//Add animation
	}

	void FadeInBackground() {
		if (!_source.isPlaying) {
			if (ctrFade < 1f){
				audioHandler.SetAudioLevel (ctrFade);
			}

			ctrFade += 0.1f;
		}
	}
}
