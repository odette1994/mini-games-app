﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using UnityEngine.Analytics;
using System.Collections.Generic;

public class ButtonController : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler {

	public Button[] answers;

	Text text;

	void Start() {

		text = transform.GetComponentInChildren<Text> ();
		text.color = Color.black;
	
	}

	public void OnPointerDown(PointerEventData data) {
		
		if (text.color == Color.white)
			text.color = Color.white;
		else
			text.color = Color.black;

	}

	public void OnPointerEnter(PointerEventData data) {

		if (text.color == Color.white)
			text.color = Color.white;
		else
			text.color = Color.black;


	}

	public void OnPointerExit(PointerEventData data) {

		if (text.color == Color.white)
			text.color = Color.white;
		else
			text.color = Color.black;


		//string sGameName = "";
		/* Analytics For Games
					 * Game: Which game screen am I?
					 */
		Analytics.CustomEvent ("QuestionClick", new Dictionary<string, object> {
			{ "Game", "Trivia" }
		}
		);
		MyAnalytics.Instance.LogEvent("QuestionClick", "Game", "Trivia", 1);
	}

}
