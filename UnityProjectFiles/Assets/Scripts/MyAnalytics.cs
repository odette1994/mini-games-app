﻿using Firebase;
using Firebase.Analytics;
using Gamelogic.Extensions;
using System.Threading.Tasks;
using UnityEngine;

public class MyAnalytics : Singleton<MyAnalytics>
{
    private Firebase.FirebaseApp app;

    void Awake()
	{
		if (Instance != this) Destroy(gameObject);
		else DontDestroyOnLoad(gameObject);
	}

	private void Start()
	{
		Debug.Log("[Firebase] AnalyticsWrapper.Start");
		PrepareFirebase();
	}

	private void PrepareFirebase()
	{
		Firebase.FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task => {
			var dependencyStatus = task.Result;
			if (dependencyStatus == Firebase.DependencyStatus.Available)
			{
				// Create and hold a reference to your FirebaseApp,
				// where app is a Firebase.FirebaseApp property of your application class.
				app = Firebase.FirebaseApp.DefaultInstance;

				// Set a flag here to indicate whether Firebase is ready to use by your app.
			}
			else
			{
				UnityEngine.Debug.LogError(System.String.Format(
				  "Could not resolve all Firebase dependencies: {0}", dependencyStatus));
				// Firebase Unity SDK is not safe to use here.
			}
		});
	}


	public void LogEvent(string eventCategory, string eventAction, string eventLabel, long value)
	{
		Debug.Log("Log Event: " + eventCategory + "/" + eventAction + "/" + eventLabel + "/" + value);
#if !UNITY_EDITOR
		FirebaseAnalytics.LogEvent($"{eventCategory}_{eventAction}", eventLabel, value);
#endif
	}

	public void LogScreen(string title)
	{
#if !UNITY_EDITOR
		//SetCurrentScreen has been removed from Firebase
		//FirebaseAnalytics.SetCurrentScreen(title, title);
#endif
	}
}
