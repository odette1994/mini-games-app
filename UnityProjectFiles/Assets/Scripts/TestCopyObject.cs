﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TestCopyObject : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	
		GameObject item3 = Instantiate(GameObject.Find("Container/Item_1")) as GameObject;
		item3.name = "Item_3";

		/**
		 * Left = min.x 
		 * Right = max.x
		 * Top = max.y
		 * Bottom min.y
		*/ 
		Image[] cloneImages = item3.GetComponentsInChildren <Image>();
		for (var i = 0; i < cloneImages.Length; i++) {
			if (cloneImages [i].name == "Icon") {
				cloneImages [i].sprite = Resources.Load<Sprite> ("Resources/ShopUI/shop_images_2");
			}
		}

		//item3.Find("Icon").GetComponent<Image>().sprite = Resources.Load<Sprite> ("Resources/ShopUI/shop_images_2");
		item3.transform.SetParent (GameObject.Find("Container").transform ,false);
		item3.transform.SetAsFirstSibling ();

	}
	
	// Update is called once per frame
	void Update () {
		

	}
}
