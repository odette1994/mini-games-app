﻿using System;

namespace Models.Puzzles
{
	[Serializable]
	public class Puzzle
	{
		private float id;
		private string question;
		private string movieId;
		private string answer;

		private string imageFilename;
		private string imageUrl;
		private string audioFilename;
		private string audioUrl;

		private string answerAudioFilename;
		private string answerAudioUrl;

		//mechanics
		private string row;
		private string column;
		private float firstBadgeTime;
		private float secondBadgeTime;
		private float thirdBadgeTime;
		private float fourthBadgeTime;

		private string firstBadgeMessage;
		private string secondBadgeMessage;
		private string thirdBadgeMessage;
		private string fourthBadgeMessage;

		public Puzzle (JSONObject jsonObject)
		{
			this.id = jsonObject.GetField ("id").n;
			this.question = jsonObject.GetField ("question").str;
			this.answer = jsonObject.GetField ("answer").str;

			this.imageFilename = jsonObject.GetField ("image_filename").str;
			this.imageUrl = jsonObject.GetField ("image_url").str;
			this.audioFilename = jsonObject.GetField ("audio_filename").str;
			this.audioUrl = jsonObject.GetField ("audio_url").str;

			if (!jsonObject.GetField ("answer_audio_filename").IsNull) {
				this.answerAudioFilename = jsonObject.GetField ("answer_audio_filename").str;
			}

			if (!jsonObject.GetField ("answer_audio_url").IsNull) {
				this.answerAudioUrl = jsonObject.GetField ("answer_audio_url").str;
			}

			this.row = jsonObject.GetField ("mechanics").GetField("row").str;
			this.column = jsonObject.GetField ("mechanics").GetField("column").str;

			if (jsonObject.GetField ("mechanics").GetField ("first_badge_time").IsNumber) {
				this.firstBadgeTime = jsonObject.GetField ("mechanics").GetField ("first_badge_time").n;
			} else {
				this.firstBadgeTime = float.Parse(jsonObject.GetField ("mechanics").GetField ("first_badge_time").str);
			}

			if (jsonObject.GetField ("mechanics").GetField ("second_badge_time").IsNumber) {
				this.secondBadgeTime = jsonObject.GetField ("mechanics").GetField ("second_badge_time").n;
			} else {
				this.secondBadgeTime = float.Parse(jsonObject.GetField ("mechanics").GetField ("second_badge_time").str);
			}

			if (jsonObject.GetField ("mechanics").GetField ("third_badge_time").IsNumber) {
				this.thirdBadgeTime = jsonObject.GetField ("mechanics").GetField ("third_badge_time").n;
			} else {
				this.thirdBadgeTime = float.Parse(jsonObject.GetField ("mechanics").GetField ("third_badge_time").str);
			}

			if (jsonObject.GetField ("mechanics").GetField ("fourth_badge_time").IsNumber) {
				this.fourthBadgeTime = jsonObject.GetField ("mechanics").GetField ("fourth_badge_time").n;
			} else {
				this.fourthBadgeTime = float.Parse(jsonObject.GetField ("mechanics").GetField ("fourth_badge_time").str);
			}

			this.firstBadgeMessage = jsonObject.GetField ("mechanics").GetField ("first_badge_message").str;
			this.secondBadgeMessage = jsonObject.GetField ("mechanics").GetField ("second_badge_message").str;
			this.thirdBadgeMessage = jsonObject.GetField ("mechanics").GetField ("third_badge_message").str;
			this.fourthBadgeMessage = jsonObject.GetField ("mechanics").GetField ("fourth_badge_message").str;
		}

		public float GetId() {
			return this.id;
		}

		public string GetQuestion() {
			return this.question;
		}

		public string GetMovieId() {
			return this.movieId;
		}

		public string GetAnswer() {
			return this.answer;
		}

		public string GetImageFilename() {
			return this.imageFilename;
		}

		public string GetImageUrl() {
			return this.imageUrl;
		}

		public string GetAudioFilename() {
			return this.audioFilename;
		}

		public string GetAudioUrl() {
			return this.audioUrl;
		}

		public string GetGridColumn() {
			return this.column;
		}

		public string GetGridRow() {
			return this.row;
		}

		public float GetFirstBadgeTime() {
			return this.firstBadgeTime;
		}

		public float GetSecondBadgeTime() {
			return this.secondBadgeTime;
		}

		public float GetThirdBadgeTime() {
			return this.thirdBadgeTime;
		}

		public float GetFourthBadgeTime() {
			return this.fourthBadgeTime;
		}

		public string GetFirstBadgeMessage() {
			return this.firstBadgeMessage;
		}

		public string GetSecondBadgeMessage() {
			return this.secondBadgeMessage;
		}

		public string GetThirdBadgeMessage() {
			return this.thirdBadgeMessage;
		}

		public string GetFourthBadgeMessage() {
			return this.fourthBadgeMessage;
		}

		public string GetAnswerAudioUrl() {
			return this.answerAudioUrl;
		}

		public string GetAnswerAudioFilename(){
			return this.answerAudioFilename;
		}
	}
}

