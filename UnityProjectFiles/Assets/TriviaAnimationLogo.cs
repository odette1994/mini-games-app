﻿using UnityEngine;
using System.Collections;

public class TriviaAnimationLogo : MonoBehaviour 
{
	[SerializeField] private GameObject logoStar1;
	[SerializeField] private GameObject logoStar2;

    private void Awake()
    {
		logoStar1 = GameObject.Find("TriviaUI/Logo/TriviaLogoStar");
		logoStar2 = GameObject.Find("TriviaUI/Logo/TriviaLogoStar2");
	}

    void AnimateStar()
	{
		iTween.ScaleFrom (logoStar1, new Vector3 (1, 1, 1),10.0f);
		StartCoroutine (AnimateSecondStar ());
	}

	IEnumerator AnimateSecondStar()
	{
		yield return new WaitForSeconds(20.0f);
		iTween.ScaleFrom (logoStar2, new Vector3 (1, 1, 1),10.0f);

	}

	// Update is called once per frame
	void Update () {

	}

	void OnDisable() {
		CancelInvoke ();
	}

	void OnEnable() {
		InvokeRepeating ("AnimateStar",0, 15.0f);
	}
}
