﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdultModal : MonoBehaviour
{
    public Action OnNoPressed;

    public void NoPressed()
    {
        OnNoPressed?.Invoke();
    }
}
