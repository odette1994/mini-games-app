using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using UnityEngine.EventSystems;
using UnityEngine.Analytics;

public class ScrollRectScript : MonoBehaviour, IEndDragHandler, IDragHandler {

	// number of pages in container
	private int _pageCount;
	private int _currentPage = 0;

	private ScrollRect _scrollRectComponent;
	private RectTransform _container;

	private bool _lerp;
	private Vector2 _lerpTo;

	// target position of every page
	private List<Vector2> _pagePositions = new List<Vector2>();

	// fast swipes should be fast and short. If too long, then it is not fast swipe
	private int _fastSwipeThresholdMaxLimit;

	// in draggging, when dragging started and where it started
	private bool _dragging;
	private float _timeStamp;
	private Vector2 _startPosition;

	// for showing small page icons
	private bool _showPageSelection;
	private int _previousPageSelectionIndex;

	[Tooltip("Threshold time for fast swipe in seconds")]
	public float fastSwipeThresholdTime = 0.3f;

	[Tooltip("Threshold time for fast swipe in (unscaled) pixels")]
	public int fastSwipeThresholdDistance = 100;

	[Tooltip("Button to go to the previous page (optional)")]
	public GameObject prevButton;

	[Tooltip("Button to go to the next page (optional)")]
	public GameObject nextButton;

	public float decelerationRate = 10f;

	[HideInInspector]public CharactersUI _character;

	void Start() { 
		_scrollRectComponent = GetComponent<ScrollRect>();
		_container = _scrollRectComponent.content;
		_pageCount = _container.childCount;
		_currentPage = 0;

		_lerp = false;

		int width = 0;
		int height = 0;
		int offsetX = 0;
		int offsetY = 0;
		int containerWidth = 0;
		int containerHeight = 0;

		// screen width in pixels of scrollrect window
		width = (int)_scrollRectComponent.GetComponent<RectTransform> ().rect.width;
		// center position of all pages
		offsetX = width / 2;
		// total width
		containerWidth = width * (_pageCount - 1);
		// limit fast swipe length - beyond this length it is fast swipe no more
		_fastSwipeThresholdMaxLimit = width;

		Vector2 newSize = new Vector2(containerWidth, containerHeight);
		_container.sizeDelta = newSize;
		Vector2 newPosition = new Vector2(containerWidth / 2, containerHeight / 2);
		_container.anchoredPosition = newPosition;

		// delete any previous settings
		_pagePositions.Clear();

		// iterate through all container childern and set their positions
		for (int i = 0; i < _pageCount; i++) {
			RectTransform child = _container.GetChild(i).GetComponent<RectTransform>();
			Vector2 childPosition;

			childPosition = new Vector2(i * width - containerWidth / 2, 0f);
			child.anchoredPosition = childPosition;

			_pagePositions.Add(-childPosition);
		}

		// Set the buttons click events
		Button prevBtn = prevButton.GetComponent<Button> ();
		if (prevBtn) {
			prevBtn.onClick.AddListener (delegate {
				PreviousScreen();
			});
		}

		Button nextBtn = nextButton.GetComponent<Button> ();
		if (nextBtn) {
			nextBtn.onClick.AddListener (delegate {
				NextScreen();
			});
		}
		// end button click events


		// Set the next/prev arrow
		SetArrow ();	
	}

	/**
	 *  Set the state of the next/previous arrow
	 * 
	 */
	void SetArrow() {
		
		if (_pageCount <= 1) {
			nextButton.SetActive(false);
			prevButton.SetActive(false);
		}

		if (_currentPage == 0 && _pageCount > 1) {
			prevButton.SetActive(false);
			nextButton.SetActive(true);
		}

		if (_currentPage + 1 == _pageCount && _pageCount != 1) {
			nextButton.SetActive(false);
			prevButton.SetActive(true);
		}

		if (_currentPage + 1 < _pageCount && _currentPage != 0) {
			nextButton.SetActive(true);
			prevButton.SetActive(true);
		}
	}

	void Update() {
		// if moving to target position
		if (_lerp) {
			// prevent overshooting with values greater than 1
			float decelerate = Mathf.Min(decelerationRate * Time.deltaTime, 1f);
			_container.anchoredPosition = Vector2.Lerp(_container.anchoredPosition, _lerpTo, decelerate);
			// time to stop lerping?
			if (Vector2.SqrMagnitude(_container.anchoredPosition - _lerpTo) < 0.25f) {
				// snap to target and stop lerping
				_container.anchoredPosition = _lerpTo;
				_lerp = false;
				// clear also any scrollrect move that may interfere with our lerping
				_scrollRectComponent.velocity = Vector2.zero;


				bool bSwipeCompleted = true;

				SetArrow ();

				//Analytics for swipping. If the swipe changes the page, set true else set false.
				Analytics.CustomEvent ("Screen Swipe", new Dictionary<string, object> {
					{ "Swiped", bSwipeCompleted },
					{ "CurrentTab", UIHandler.Instance.MenuID }
				}
				);
				MyAnalytics.Instance.LogEvent("Screen Swipe", $"Swiped-{bSwipeCompleted}", $"CurrentTab-{UIHandler.Instance.MenuID}", 1);
				
				// update the next/prev arrow

			}

		}
	}

	public void OnBeginDrag(PointerEventData aEventData) {
		// if currently lerping, then stop it as user is draging
		_lerp = false;
		_dragging = true;
	}

	public void OnDrag(PointerEventData aEventData) {
		if (!_dragging) {
//			_lerp = false;
			_startPosition = _container.anchoredPosition;
			_timeStamp = Time.unscaledTime;
			_dragging = true;
		}

	}

	public void OnEndDrag(PointerEventData aEventData) {
		// if not fast time, look to which page we got to
		float difference = _startPosition.x - _container.anchoredPosition.x;

		// test for fast swipe - swipe that moves only +/-1 item
		if (Time.unscaledTime - _timeStamp < fastSwipeThresholdTime &&
			Mathf.Abs(difference) > fastSwipeThresholdDistance &&
			Mathf.Abs(difference) < _fastSwipeThresholdMaxLimit) {
			if (difference > 0) {
				NextScreen();
			} else {
				PreviousScreen();
			}
		} else {
			// if not fast time, look to which page we got to
			LerpToPage(GetNearestPage());
		}

		_dragging = false;
		StopEventsWhenDrag ();

	}

	//------------------------------------------------------------------------
	private int GetNearestPage() {
		// based on distance from current position, find nearest page
		Vector2 currentPosition = _container.anchoredPosition;
		int nearestPage = _currentPage;
		float distance = float.MaxValue;

		for (int i = 0; i < _pagePositions.Count; i++) {
			float testDist = Vector2.SqrMagnitude(currentPosition - _pagePositions[i]);

			if (testDist < distance) {
				distance = testDist;
				nearestPage = i;
			}
		} 
	
		return nearestPage;
	}

	private void LerpToPage(int aPageIndex) { 
		aPageIndex = Mathf.Clamp(aPageIndex, 0, _pageCount - 1);
		_lerpTo = _pagePositions[aPageIndex]; 
		_lerp = true;
		_currentPage = aPageIndex;
	}

	//------------------------------------------------------------------------
	private void NextScreen() {
		LerpToPage(_currentPage + 1);
		StopEventsWhenDrag ();
		bool bRightArrow = true;
		//Analytics for arrow click. If the click if to the right set true else set false.
		Analytics.CustomEvent ("ArrowClicked", new Dictionary<string, object> {
			{ "Swiped", bRightArrow },
			{ "CurrentTab", UIHandler.Instance.MenuID }
		}
		);
		if (bRightArrow)
			MyAnalytics.Instance.LogEvent("ArrowClicked", "SwipedRight", $"CurrentTab-{UIHandler.Instance.MenuID}", 1);
		else MyAnalytics.Instance.LogEvent("ArrowClicked", "SwipedLeft", $"CurrentTab-{UIHandler.Instance.MenuID}", 1);
	}

	//------------------------------------------------------------------------
	private void PreviousScreen() {
		LerpToPage(_currentPage - 1);
		StopEventsWhenDrag ();
		bool bRightArrow = false;
		//Analytics for arrow click. If the click if to the right set true else set false.
		Analytics.CustomEvent ("ArrowClicked", new Dictionary<string, object> {
			{ "Swiped", bRightArrow },
			{ "CurrentTab", UIHandler.Instance.MenuID }
		}
		);
		if(bRightArrow)
			MyAnalytics.Instance.LogEvent("ArrowClicked", "SwipedRight", $"CurrentTab-{UIHandler.Instance.MenuID}", 1);
		else MyAnalytics.Instance.LogEvent("ArrowClicked", "SwipedLeft", $"CurrentTab-{UIHandler.Instance.MenuID}", 1);
	}

	private void StopEventsWhenDrag () {
		if (_character != null) {
			_character.StopEvents ();
		}
	}


	/**
	 * Event to trigger next  
	 * 
	 */
	public void onClickNext() {
//		NextScreen ();
	}

	/**
	 * Event to trigger previous
	 * 
	 */
	public void onClickPrevious() {
//		PreviousScreen ();
	}

	public int GetChildCount() { return _pageCount; }

	public int GetCurrentPage() { return _currentPage; }

}
