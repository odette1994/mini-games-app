﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Models
{
	[Serializable]
	public class EmailMsg
	{
		private string qoute;

		public EmailMsg(JSONObject jsonObject) {
			this.qoute = jsonObject.GetField("quote").str;
		}

		public string GetQoute() {
			return this.qoute;
		}
	}
}

