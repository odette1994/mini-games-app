﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class SlideUpScript : MonoBehaviour, IBeginDragHandler, IEndDragHandler, IDragHandler {

	private ScrollRect scrollRect;
	private RectTransform container;
	private bool isDragging;
	private Vector2 startPosition;
	private float timeStamp;
	private int pageCount;
	private bool lerp;

	// Use this for initialization
	void Start () {
		scrollRect = GetComponent<ScrollRect> ();
		container = scrollRect.content;

		isDragging = false;
		lerp = false;
	}
	
	// Update is called once per frame
	void Update () {

	}

	public void OnDrag(PointerEventData aEventData) {
		if (!isDragging) {
			isDragging = true;
			timeStamp = Time.unscaledTime;
			startPosition = container.anchoredPosition;
		}
	}

	public void OnEndDrag(PointerEventData aEventData) {
		// if not fast time, look to which page we got to
		float difference = startPosition.y - container.anchoredPosition.y;
		int height = (int)scrollRect.GetComponent<RectTransform> ().rect.height;

		// test for fast swipe - swipe that moves only +/-1 item
		if (Time.unscaledTime - timeStamp < 0.3f &&
			Mathf.Abs(difference) > 100 &&
			Mathf.Abs(difference) < height) {

			if (difference < 0) {
				UIHandler.Instance.HideConfirmAdultModal ();
			}


		}
	}

	public void OnBeginDrag(PointerEventData aEventData) {
		#if (UNITY_IOS || UNITY_ANDROID) && !UNITY_EDITOR
		if (Input.touchCount != 2) {
			scrollRect.enabled = false;
			return;
		}

		scrollRect.enabled = true;
		#endif

		isDragging = false;
	}

}
