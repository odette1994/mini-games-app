using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using Models;
using Models.Puzzles;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Puzzle = Models.Puzzle;

public class PuzzleMatchingController : MonoBehaviour
{
    private const int B_PRINCESS = 60;
    private const int B_JEANBOB = 75;
    private const int B_SPEED = 90;
    private GameObject answerContainer;
    private List<AudioClip> answerSFX;
    private List<string> arrayCompare;
    private List<Puzzle> arrayPieces;
    private List<Vector2> arrayPuzzle;
    private List<string> arrayReverse;
    private GameObject b_Alise_active;
    private GameObject b_Alise_inactive;
    private GameObject b_Jeanbob_active;
    private GameObject b_Jeanbob_inactive;

    //Badges
    private GameObject b_Rothbart_active;
    private GameObject b_Rothbart_inactive;
    private GameObject b_Speed_active;
    private GameObject b_Speed_inactive;

    private readonly int badge = 0;
    public GameObject badgeParticle;
    private GameObject boardMonitor;
    private GameObject boardTimer;
    private Button buttonHint;
    private Button buttonNext;
    private Button buttonReplay;

    private List<AudioClip> charSFX;
    private int columns;
    private List<AudioClip> correctSFX;

    private int countMatch;
    private AudioClip effectSFX;

    private string finalAnswer = "";

    private GameObject firstClick;

    private bool firstOkayToClick = true;
    private AudioClip gameOver;
    private GameObject gameoverScreen;
    private bool hasSolved;
    private Text hintNumber;
    private readonly int hintsCount = 3;
    private int hintsCountDecrementing = 3;
    private int hour;

    // holder of the puzzle selected
    private int imageIndex;
    private GameObject imagePreview;
    private List<int> indexUsed;
    private readonly bool isPaused = false;

    // list of puzzles question with score
    private List<Question> listQuestions;
    private GameObject mainMenuScreen;

    private GameObject master;
    private int matchCounter;
    private Text message;
    private int minute;
    private GameObject nextLoading;
    private int nextPlay;
    private GameObject objContainer;
    private GameObject p_Alise;
    private GameObject p_board;
    private GameObject p_Jeanbob;
    private GameObject p_Rothbart;
    private GameObject p_Speed;

    public GameObject particleEffect;
    private GameObject pb_alise;
    private GameObject pb_jbob;
    private GameObject pb_rothbart;
    private GameObject pb_speed;

    private float picWidth, picHeight;
    private GameObject progressbar;
    private GameObject puffin;
    private int puzzleSize;
    private GameObject questionContainer;
    private int rows;
    private int second;
    private GameObject secondClick;
    private bool secondOkayToClick = true;
    private AudioSource sfx;

    private AudioSource source;
    private Sprite sp1;

    private AudioClip tapCard;
    private int time = 1;
    private Text timer;
    private Text title;

    private int trial = 1;
    private GameObject welcomeScreen;
    private GameObject winBackground;
    private List<AudioClip> wrongSFX;

    [SerializeField] private GameObject _puzzlePiecePrefab;

    // Use this for initialization
    private void Awake()
    {
        sfx = gameObject.AddComponent<AudioSource>();
    }

    private void Start()
    {
        // Adding Audioclips
        charSFX = new List<AudioClip>();
        answerSFX = new List<AudioClip>();
        correctSFX = new List<AudioClip>();
        wrongSFX = new List<AudioClip>();
        tapCard = Resources.Load<AudioClip>("SFX/MemoryGame/Card flip");
        effectSFX = Resources.Load<AudioClip>("SFX/dust effect");
        gameOver = Resources.Load<AudioClip>("SFX/game over");

        AddSFX(charSFX, "SFX/Puzzle");
        AddSFX(answerSFX, "SFX/Puzzle_answer");
        AddSFX(correctSFX, "SFX/puzzle_correct");
        AddSFX(wrongSFX, "SFX/puzzle_wrong");

        source = gameObject.AddComponent<AudioSource>();
        arrayReverse = new List<string>();

        gameoverScreen = GameObject.Find("GameOverUI");
        mainMenuScreen = GameObject.Find("MainGameUI");
        title = GameObject.Find("GameOverUI/Title").GetComponent<Text>();
        message = GameObject.Find("GameOverUI/Message").GetComponent<Text>();
        buttonNext = GameObject.Find("GameOverUI/btnNext").GetComponent<Button>();
        buttonReplay = GameObject.Find("GameOverUI/btnReplay").GetComponent<Button>();
        imagePreview = GameObject.Find("MainGameUI/ImagePreview");
        hintNumber = GameObject.Find("Game/Elements/Hint/BadgeNum/HintNumber").GetComponent<Text>();
        boardMonitor = mainMenuScreen.transform.Find("Monitor/Board").gameObject;
        boardTimer = mainMenuScreen.transform.Find("Game/Elements/Timer").gameObject;
        nextLoading = GameObject.Find("GameOverUI/Loading");

        // hide the game over next loading
        nextLoading.SetActive(false);

        hintNumber.text = hintsCountDecrementing.ToString();

        //particle
        particleEffect = GameObject.Find("Star_Particle");
        badgeParticle = GameObject.Find("GameOverUI/BG/badge");
        particleEffect.SetActive(false);

        imagePreview.SetActive(false);

        //badges
        b_Alise_active = GameObject.Find("GameOverUI/Board/1/Active");
        b_Jeanbob_active = GameObject.Find("GameOverUI/Board/2/Active");
        b_Speed_active = GameObject.Find("GameOverUI/Board/3/Active");
        b_Rothbart_active = GameObject.Find("GameOverUI/Board/4/Active");

        b_Alise_inactive = GameObject.Find("GameOverUI/Board/1/Inactive");
        b_Jeanbob_inactive = GameObject.Find("GameOverUI/Board/2/Inactive");
        b_Speed_inactive = GameObject.Find("GameOverUI/Board/3/Inactive");
        b_Rothbart_inactive = GameObject.Find("GameOverUI/Board/4/Inactive");

        progressbar = GameObject.Find("GameOverUI/Board/Progress bar");

        p_Alise = progressbar.transform.Find("1").gameObject;
        p_Jeanbob = progressbar.transform.Find("2").gameObject;
        p_Speed = progressbar.transform.Find("3").gameObject;
        p_Rothbart = progressbar.transform.Find("4").gameObject;

        pb_alise = GameObject.Find("GameOverUI/BG/1");
        pb_jbob = GameObject.Find("GameOverUI/BG/2");
        pb_speed = GameObject.Find("GameOverUI/BG/3");
        pb_rothbart = GameObject.Find("GameOverUI/BG/4");
        p_board = GameObject.Find("GameOverUI/Board");

        ShowGameObject(b_Alise_active, false);
        ShowGameObject(b_Alise_inactive, false);
        ShowGameObject(b_Jeanbob_active, false);
        ShowGameObject(b_Jeanbob_inactive, false);
        ShowGameObject(b_Rothbart_active, false);
        ShowGameObject(b_Rothbart_inactive, false);
        ShowGameObject(b_Speed_active, false);
        ShowGameObject(b_Speed_inactive, false);
        ShowGameObject(progressbar, false);
        ShowGameObject(p_Alise, false);
        ShowGameObject(p_Jeanbob, false);
        ShowGameObject(p_Speed, false);
        ShowGameObject(p_Rothbart, false);
        ShowGameObject(pb_alise, false);
        ShowGameObject(pb_jbob, false);
        ShowGameObject(pb_rothbart, false);
        ShowGameObject(pb_speed, false);
        ShowGameObject(p_board, false);
        ShowGameObject(boardMonitor, false);
        ShowGameObject(badgeParticle, false);

        ShowGameObject(gameoverScreen, false);

        puffin = GameObject.Find("Welcome Screen/Obj/Puffin");
        questionContainer = GameObject.Find("Welcome Screen/Obj/Container");
        objContainer = GameObject.Find("Welcome Screen/Obj");
        answerContainer = mainMenuScreen.transform.Find("Answer").gameObject;
        imageIndex = PlayerPrefs.GetInt("imageIndex");
        master = GameObject.Find("Game");
        welcomeScreen = GameObject.Find("Welcome Screen");
        timer = GameObject.Find("Game/Elements/Timer/Text").GetComponent<Text>();
        buttonHint = GameObject.Find("Game/Elements/Hint").GetComponent<Button>();

//		ShowGameObject (objContainer, false);
        ShowGameObject(answerContainer, false);

        buttonHint.gameObject.SetActive(false);

        buttonHint.onClick.AddListener(delegate
        {
            //buttonHint.GetComponent<Image>().sprite = Resources.LoadAll<Sprite> ("puzzle/puzzle")[1];
            ShowHints();
        });

        boardMonitor.GetComponent<Button>().onClick.AddListener(delegate { StartCoroutine(PuzzleTo("scale up")); });

        StartCoroutine(Welcome());

        RectTransform masterRect = master.GetComponent<RectTransform>();

        RectTransform mainCanvas = gameObject.GetComponent<RectTransform>();

        arrayPieces = new List<Puzzle>();
        arrayPuzzle = new List<Vector2>();

        // need custom data
        // get the question and scores
        GetPuzzleList();

        // default questions
        if (imageIndex < 2)
        {
            sp1 = Resources.Load<Sprite>(listQuestions[imageIndex].GetImagePath());
        }
        // question from the API
        else
        {
            Texture2D texture2d = null;
            byte[] fileData;
            string imagePath = listQuestions[imageIndex].GetImagePath();
            if (File.Exists(imagePath))
            {
                fileData = File.ReadAllBytes(imagePath);
                texture2d = new Texture2D(2, 2);
                texture2d.LoadImage(fileData);
            }

            sp1 = Sprite.Create(texture2d,
                new Rect(0, 0, texture2d.width, texture2d.height),
                new Vector2(0.5f, 0.5f));
        }

        picWidth = sp1.rect.width;
        picHeight = sp1.rect.height;

        int rows = int.Parse(listQuestions[imageIndex].GetGridRow());
        int columns = int.Parse(listQuestions[imageIndex].GetGridColumn());
        
        float pieceWidth = picWidth / columns;
        float pieceHeight = picHeight / rows;

        int ctrName = 0;
        for (int x = 0; x < columns; x++)
        for (int y = 0; y < rows; y++)
        {
            GameObject puzzlePieceObject = Instantiate(_puzzlePiecePrefab, master.transform);
            PuzzlePiece puzzlePiece = puzzlePieceObject.GetComponent<PuzzlePiece>();
            puzzlePiece.InitializePiece(ctrName++.ToString(), sp1, this, pieceWidth, pieceHeight, x, y, columns, rows);

            arrayPieces.Add(new Puzzle(puzzlePieceObject));
        }

        // do random
        indexUsed = new List<int>();
        arrayCompare = new List<string>();
        for (int x = 0; x < arrayPieces.Count; x++)
        {
            if (!indexUsed.Contains(x))
            {
                indexUsed.Add(x);
                int random = GetRandomNumber(0);
                indexUsed.Add(random);

                arrayCompare.Add(x + 1 + " AND " + (random + 1));

                Vector2 tmpPositions = arrayPieces[x].GetGameObject().GetComponent<RectTransform>().localPosition;

                arrayPieces[x].GetGameObject().GetComponent<RectTransform>().localPosition =
                    arrayPieces[random].GetGameObject().GetComponent<RectTransform>().localPosition;
                arrayPieces[random].GetGameObject().GetComponent<RectTransform>().localPosition = tmpPositions;
            }
        }

        // set puzzle's answer
        finalAnswer = listQuestions[imageIndex].GetAnswer();

        Text question = questionContainer.transform.Find("Question").GetComponent<Text>();
        if (listQuestions[imageIndex].GetQuestion().Contains("?"))
            question.text = listQuestions[imageIndex].GetQuestion();
        else
            question.text = listQuestions[imageIndex].GetQuestion() + "?";
    }

    /// <summary>
    ///     Gets the random number.
    /// </summary>
    /// <returns>The random number.</returns>
    private void SetPuzzleSize(int startValue, int endValue, int minimumPieces)
    {
        int random = Random.Range(startValue, endValue);
        int sqrt = random * random;

        while (sqrt % 2 != 0 || sqrt < minimumPieces) random = Random.Range(startValue, endValue);
    }

    private IEnumerator Timer()
    {
//		while (time >= 0 && !hasSolved && !isPaused) {
//
//			if (time > 0)
//				timer.text = time.ToString ();
//
//			else {
//				StopCoroutine (Timer ());
//			}
//			time--;
//			yield return new WaitForSeconds (1f);
//		}

        while (time > 0 && !hasSolved && !isPaused)
        {
            second = time % 60;
            minute = time / 60;
            hour = minute / 60;

            if (minute == 60)
                minute = 0;

            string str_format = "";

            if (hour < 1 && minute >= 1)
                str_format = string.Format("{0:00}:{1:00}", minute, second);
            else if (minute < 1 && hour < 1)
                str_format = string.Format("{0:00}", second);
            else
                str_format = string.Format("{0:00}:{1:00}:{2:00}", hour, minute, second);
            timer.text = str_format;
            time++;
            yield return new WaitForSeconds(1f);
        }
    }

    private void ShowGameObject(GameObject obj, bool show)
    {
        obj.SetActive(show);
    }

    private IEnumerator Welcome()
    {
        master.SetActive(false);
//		iTween.MoveTo (objContainer, new Vector3(0f,0f,0f),0.5f);
        yield return new WaitForSeconds(1);

        if (imageIndex < 2)
        {
            PlaySFX(charSFX[imageIndex]);
        }
        else
        {
            WWW www = new WWW("file://" + listQuestions[imageIndex].GetAudioPath());
            yield return www;

            if (www != null && www.isDone)
            {
                AudioClip audioClip = www.GetAudioClip(false, false);
                PlaySFX(audioClip);
            }
        }

        yield return new WaitUntil(() => source.isPlaying != true);
        welcomeScreen.SetActive(false);
        master.SetActive(true);
        StartCoroutine(Timer());
        StartCoroutine(ShowMonitor(Random.Range(1.0f, 5.0f)));
    }

    private void AddSFX(List<AudioClip> list, string location)
    {
        foreach (object aud in Resources.LoadAll(location)) list.Add(aud as AudioClip);
    }

    private void PlaySFX(AudioClip clip)
    {
        if (source.isPlaying)
            source.Stop();

        source.PlayOneShot(clip, 1f);
    }

    private IEnumerator ShowMonitor(float delay)
    {
        yield return new WaitForSeconds(delay);

        Image image = boardMonitor.transform.Find("Image").GetComponent<Image>();
        image.sprite = sp1;

        ShowGameObject(boardMonitor, true);
        iTween.MoveFrom(boardMonitor, new Vector3(-752, -1232, 0), 1f);

//		yield return new WaitForSeconds (10);
//		StartCoroutine (PuzzleTo("scale up"));
    }

    /// <summary>
    ///     Gets the random number.
    /// </summary>
    /// <returns>The random number.</returns>
    private int GetRandomNumber(int startIndex)
    {
        int random = Random.Range(startIndex, arrayPieces.Count);

        if (indexUsed.Count == arrayPieces.Count) return -1;

        while (indexUsed.Contains(random)) random = Random.Range(startIndex, arrayPieces.Count);

        return random;
    }

    public IEnumerator CheckMatch(GameObject gameObject)
    {
        if (firstClick != null && secondClick != null)
        {
            Debug.Log("STOP CLICKING!!!!");
        }
        else if (firstClick == null)
        {
            sfx.PlayOneShot(tapCard, 1f);

            firstClick = gameObject;

            // highlight
            Image[] fImage = firstClick.GetComponentsInChildren<Image>();
            fImage[2].color = new Color(fImage[2].color.r, fImage[2].color.g, fImage[2].color.b, 0.8f);
        }
        else if (firstClick.Equals(gameObject))
        {
            Debug.Log("SAME");
        }
        else
        {
            sfx.PlayOneShot(tapCard, 1f);

            secondClick = gameObject;

            firstOkayToClick = false;
            secondOkayToClick = false;

            Image[] fImg = secondClick.GetComponentsInChildren<Image>();
            fImg[2].color = new Color(fImg[2].color.r, fImg[2].color.g, fImg[2].color.b, 0.8f);

            Debug.Log("Second " + secondClick.name);
            Vector3 tmpFirst = new Vector3();
            Vector3 tmpSecond = new Vector3();

            tmpFirst = firstClick.transform.position;
            tmpSecond = secondClick.transform.position;

            Vector2 tmpSecondClick = arrayPieces[int.Parse(secondClick.name)].GetGameObject().transform.position;
            Vector2 tmpFirstClick = arrayPieces[int.Parse(firstClick.name)].GetGameObject().transform.position;

            //CheckPuzzle (firstClick, secondClick);

//			if (arrayCompare.Contains (firstClick.name + " AND " + secondClick.name) || arrayCompare.Contains(secondClick.name + " AND " + firstClick.name)) {

            firstClick.transform.SetAsLastSibling();
            secondClick.transform.SetAsLastSibling();


            // flag if it has at least one match
            bool flagMatch = false;

            Debug.Log("1st real " + arrayPieces[int.Parse(firstClick.name)].GetPosition() + " 1st new " +
                      tmpSecondClick);
            Debug.Log("2nd real " + arrayPieces[int.Parse(secondClick.name)].GetPosition() + " 2nd new " +
                      tmpFirstClick);

            // remove highlight
            Image[] fImage = firstClick.GetComponentsInChildren<Image>();
            fImage[2].color = new Color(fImage[2].color.r, fImage[2].color.g, fImage[2].color.b, 1f);
            fImg = secondClick.GetComponentsInChildren<Image>();
            fImg[2].color = new Color(fImg[2].color.r, fImg[2].color.g, fImg[2].color.b, 1f);

            // check first card
            if (arrayPieces[int.Parse(firstClick.name)].GetPosition().normalized == tmpSecondClick.normalized)
            {
                Debug.Log("MATCH FIRST");

                Hashtable matchHashtableFirst = new Hashtable();
                matchHashtableFirst.Add("time", 1f);
                matchHashtableFirst.Add("position", secondClick.transform.position);
                matchHashtableFirst.Add("oncomplete", "OnCompleteMatchFirst");
                matchHashtableFirst.Add("oncompletetarget", this.gameObject);
                iTween.MoveTo(firstClick, matchHashtableFirst);

                firstClick.GetComponent<Button>().interactable = false;

                // remove shadow
                Destroy(firstClick.GetComponent<Shadow>());

                countMatch++;

                flagMatch = true;
                matchCounter++;
            }
            // if not match
            else
            {
                Debug.Log("NOT MATCH FIRST");
                Debug.Log("NOT 1st real " + arrayPieces[int.Parse(firstClick.name)].GetPosition() + " 1st new " +
                          tmpSecondClick);

                Hashtable notMatchHashtableFirst = new Hashtable();
                notMatchHashtableFirst.Add("time", 1f);
                notMatchHashtableFirst.Add("position", secondClick.transform.position);
                notMatchHashtableFirst.Add("oncomplete", "OnCompleteFirst");
                notMatchHashtableFirst.Add("oncompletetarget", this.gameObject);
                iTween.MoveTo(firstClick, notMatchHashtableFirst);
            }


            // check second cards
            if (arrayPieces[int.Parse(secondClick.name)].GetPosition().normalized == tmpFirstClick.normalized)
            {
                Debug.Log("MATCH SECOND");

                Hashtable matchHashtableSecond = new Hashtable();
                matchHashtableSecond.Add("time", 1f);
                matchHashtableSecond.Add("position", firstClick.transform.position);
                matchHashtableSecond.Add("oncomplete", "OnCompleteMatchSecond");
                matchHashtableSecond.Add("oncompletetarget", this.gameObject);
                iTween.MoveTo(secondClick, matchHashtableSecond);

                secondClick.GetComponent<Button>().interactable = false;

                // remove shadow
                Destroy(secondClick.GetComponent<Shadow>());

                countMatch++;

                flagMatch = true;
                matchCounter++;
            }
            // if not match
            else
            {
                Debug.Log("NOT MATCH SECOND");
                Debug.Log("NOT 2nd real " + arrayPieces[int.Parse(secondClick.name)].GetPosition() + " 2nd new " +
                          tmpFirstClick);

                Hashtable notMatchHashtableSecond = new Hashtable();
                notMatchHashtableSecond.Add("time", 1f);
                notMatchHashtableSecond.Add("position", firstClick.transform.position);
                notMatchHashtableSecond.Add("oncomplete", "OnCompleteSecond");
                notMatchHashtableSecond.Add("oncompletetarget", this.gameObject);
                iTween.MoveTo(secondClick, notMatchHashtableSecond);
            }

            Debug.Log("Match counter: " + matchCounter);
            // do the sounds or effects
            if (flagMatch && matchCounter > 0)
            {
                if (matchCounter == 2) StartCoroutine("WaitBeforeShowEffect");

                PlaySFX(correctSFX[Random.Range(0, correctSFX.Count)]);
                matchCounter = 0;
            }
            else
            {
                PlaySFX(wrongSFX[Random.Range(0, wrongSFX.Count)]);
            }


            // if all has been match
            if (countMatch == arrayPieces.Count)
            {
                Debug.Log("GAME OVER");
                hasSolved = true; // timer ends
                winBackground = new GameObject();
                winBackground.name = "WinBg";

                iTween.MoveTo(boardMonitor, new Vector3(-752, -1232, 0), 1f);
                iTween.MoveTo(boardTimer, new Vector3(0, -1119, 0), 1f);

                RectTransform rect = winBackground.AddComponent<RectTransform>();
                rect.sizeDelta = new Vector2(picWidth, picHeight);

                Image image = winBackground.AddComponent<Image>();
                image.color = Color.white;

                StartCoroutine(WaitFor1());

                StartCoroutine(FadeOut());

                yield return new WaitForSeconds(1f);

                StartCoroutine(WaitBeforeGameOver());
            }
        }
    }

    private IEnumerator ShowAnswer()
    {
        Text answer = answerContainer.transform.Find("Text").GetComponent<Text>();
        answer.text = finalAnswer;

        ShowGameObject(answerContainer, true);

        // play answer audio
        if (imageIndex < 2)
        {
            PlaySFX(answerSFX[imageIndex]);
        }
        else
        {
            WWW www = new WWW("file://" + listQuestions[imageIndex].GetAnswerAudioPath());
            yield return www;

            if (www != null && www.isDone)
            {
                AudioClip audioClip = www.GetAudioClip(false, false);
                PlaySFX(audioClip);
            }
        }

        yield return new WaitUntil(() => source.isPlaying != true);
    }

    private IEnumerator WaitBeforeShowEffect()
    {
        GameObject effect;
        effect = Instantiate(particleEffect);
        effect.SetActive(true);
        sfx.PlayOneShot(effectSFX, 1f);
        yield return new WaitForSeconds(2.5f);
        Destroy(effect.gameObject);
    }

    private void CheckPuzzle(GameObject first, GameObject second)
    {
//		for (int i = 0; i < arrayPieces.Count; i++) {
        Debug.Log(first.name + " AND " + second.name);
        if (arrayCompare.Contains(first.name + " AND " + second.name) ||
            arrayCompare.Contains(second.name + " AND " + first.name))
        {
            Debug.Log("ALL IS MATCHED!!!");
        }
        else
        {
            if (arrayReverse.Contains(first.name + " AND ") || arrayReverse.Contains(second.name + " AND "))
                Debug.Log("FIRST IS MATCHED!!!");
            else if (arrayReverse.Contains(" AND " + second.name) || arrayReverse.Contains(" AND " + first.name))
                Debug.Log("SECOND IS MATCHED!!!");
            else
                Debug.Log("NO MATCH!!!");
        }

//		}
    }


    /// <summary>
    ///     Raises the complete first event.
    /// </summary>
    // animation for first click wrong
    private void OnCompleteFirst()
    {
        Hashtable hashTable = new Hashtable();
        hashTable.Add("time", 0.5f);
        hashTable.Add("amount", new Vector3(1, 3, 5));
        hashTable.Add("oncomplete", "OnCompleteShakeFirst");
        hashTable.Add("oncompletetarget", gameObject);
        hashTable.Add("ignoretimescale", true);
        iTween.ShakePosition(firstClick, hashTable);
    }

    /// <summary>
    ///     Raises the complete shake first event.
    /// </summary>
    private void OnCompleteShakeFirst()
    {
//		Hashtable hashTable = new Hashtable ();
//		hashTable.Add ("time", 1f);
//		hashTable.Add ("oncomplete", "OnCompleteMoveBackFirst");
//		hashTable.Add ("oncompletetarget", this.gameObject);
//		hashTable.Add ("x", secondClick.transform.position.x);
//		hashTable.Add ("y", secondClick.transform.position.y);
//		iTween.MoveTo (firstClick, hashTable);
//		firstClick = null;
//		firstOkayToClick = true;

        StartCoroutine(PuzzleWaitSeconds(0.2f, 1));
    }

    /// <summary>
    ///     Raises the complete move back first event.
    /// </summary>
    private void OnCompleteMoveBackFirst()
    {
        firstClick = null;
    }
    // end animation first click wrong

    /// <summary>
    ///     Raises the complete second event.
    /// </summary>
    // animation for second click wrong
    private void OnCompleteSecond()
    {
        Hashtable hashTable = new Hashtable();
        hashTable.Add("time", 0.5f);
        hashTable.Add("amount", new Vector3(1, 3, 5));
        hashTable.Add("oncomplete", "OnCompleteShakeSecond");
        hashTable.Add("oncompletetarget", gameObject);
        hashTable.Add("ignoretimescale", true);
        iTween.ShakePosition(secondClick, hashTable);
    }

    /// <summary>
    ///     Raises the complete shake second event.
    /// </summary>
    private void OnCompleteShakeSecond()
    {
//		Hashtable hashTable = new Hashtable ();
//		hashTable.Add ("time", 1f);
//		hashTable.Add ("oncomplete", "OnCompleteMoveBackSecond");
//		hashTable.Add ("oncompletetarget", this.gameObject);
//		hashTable.Add ("x", firstClick.transform.position.x);
//		hashTable.Add ("y", firstClick.transform.position.y);
//		iTween.MoveTo (secondClick, hashTable);
        StartCoroutine(PuzzleWaitSeconds(0.2f, 2));
//		secondClick = null;
//		secondOkayToClick = true;
    }

    /// <summary>
    ///     Raises the complete move back second event.
    /// </summary>
    private void OnCompleteMoveBackSecond()
    {
        secondClick = null;
    }
    // end animation second click wrong


    // start first match
    private void OnCompleteMatchFirst()
    {
//		firstClick = null;
        StartCoroutine(PuzzleWaitSeconds(0.7f, 1));
    }


    private void OnCompleteMatchSecond()
    {
//		secondClick = null;
        StartCoroutine(PuzzleWaitSeconds(0.7f, 2));
    }

    private int GetRandom(int first)
    {
        int second = Random.Range(1, arrayPieces.Count);
        int value = 0;

        while (first == second) second = Random.Range(1, arrayPieces.Count);

        return second;
    }

    private void ShowHintForImage()
    {
        imagePreview.GetComponent<RectTransform>().sizeDelta = new Vector2(picWidth, picHeight);
        imagePreview.GetComponent<Image>().sprite = sp1;
        StartCoroutine(FadePreviewImage(imagePreview.GetComponent<Image>()));
    }

    private void ShowHints()
    {
        if (trial <= hintsCount)
        {
            hintsCountDecrementing--;
            hintNumber.text = hintsCountDecrementing.ToString();
            //OnShowHints ();
            ShowHintForImage();
            trial++;
        }

        if (trial > hintsCount) buttonHint.enabled = false;

        //buttonHint.GetComponent<Image>().sprite = Resources.LoadAll<Sprite> ("puzzle/puzzle")[2];

        Debug.Log(trial);
    }


    private IEnumerator FadePreviewImage(Image img1)
    {
        imagePreview.SetActive(true);
        yield return new WaitForSeconds(2f);
        img1.CrossFadeAlpha(0.0f, 3.0f, false);
        yield return new WaitForSeconds(3f);
        img1.gameObject.SetActive(false);
    }

    private IEnumerator FadeHint(Image img1, Image img2)
    {
        yield return new WaitForSeconds(1);
        img1.CrossFadeAlpha(0.5f, 0.3f, false);
        img2.CrossFadeAlpha(0.5f, 0.3f, false);
        yield return new WaitForSeconds(1);
        img1.CrossFadeAlpha(1, 0.1f, false);
        img2.CrossFadeAlpha(1, 0.1f, false);
    }

    private IEnumerator WaitBeforeGameOver()
    {
        yield return new WaitForSeconds(3.9f);
        GameOver();
    }

    private void GameOver()
    {
        StopCoroutine(Timer());

        string str_title, str_message = "";
        string badgeString = "";

        if (time > 0 && time <= listQuestions[imageIndex].GetFirstBadgeTime())
        {
            str_title = listQuestions[imageIndex].GetFirstBadgeMessage();
            str_message = "an Alise";
            badgeString = "1";
        }
        else if (time > listQuestions[imageIndex].GetFirstBadgeTime() &&
                 time <= listQuestions[imageIndex].GetSecondBadgeTime())
        {
            str_title = listQuestions[imageIndex].GetSecondBadgeMessage();
            str_message = "a JeanBob";
            badgeString = "2";
        }
        else if (time > listQuestions[imageIndex].GetSecondBadgeTime() &&
                 time <= listQuestions[imageIndex].GetThirdBadgeTime())
        {
            str_title = listQuestions[imageIndex].GetThirdBadgeMessage();
            str_message = "a Speed";
            badgeString = "3";
        }
        else
        {
            str_title = listQuestions[imageIndex].GetFourthBadgeMessage();
            str_message = "a Rothbart";
            badgeString = "4";
        }

        title.text = str_title;
        message.text = "You got " + str_message + " badge!";

        title.gameObject.SetActive(false);
        message.gameObject.SetActive(false);

        // game done overwrite the score
        if (int.Parse(listQuestions[imageIndex].GetBadge()) == 0 ||
            int.Parse(badgeString) <= int.Parse(listQuestions[imageIndex].GetBadge()))
        {
            listQuestions[imageIndex].SetBadge(badgeString);
            if (int.Parse(badgeString) < 4) // unlock the 2nd puzzle
                if (listQuestions.Count - 1 > imageIndex)
                    listQuestions[imageIndex + 1].SetIsUnlock(true);
        }

        // write the file
        string puzzleQuestionPath = Application.persistentDataPath + FileName.PUZZLE_QUESTIONS;
        BinaryFormatter bf = new BinaryFormatter();
        FileStream file = File.Open(puzzleQuestionPath, FileMode.Create);
        for (int x = 0; x < listQuestions.Count; x++) bf.Serialize(file, listQuestions[x]);
        file.Close();

        // Add listeners
        buttonReplay.onClick.AddListener(delegate { SceneManager.LoadScene("Puzzle"); });

        buttonNext.onClick.AddListener(delegate
        {
            if (listQuestions.Count - 1 > imageIndex)
                if (listQuestions[imageIndex + 1].GetIsUnlock())
                {
                    PlayerPrefs.SetInt("imageIndex", imageIndex + 1);
                    SceneManager.LoadScene("Puzzle");
                }
        });

        buttonReplay.gameObject.SetActive(false);
        buttonNext.gameObject.SetActive(false);

        PlayerPrefs.SetInt("allowedToPlay", PlayerPrefs.GetInt("allowedToPlay") + 1);
        SaveAchievements();

        ShowGameObject(gameoverScreen, true);
        sfx.PlayOneShot(gameOver, 0.8f);
        StartCoroutine(ShowGameOver(time, badgeString));
    }

    private IEnumerator ShowGameOver(int time, string badge)
    {
        ShowGameObject(p_board, true);
        iTween.MoveFrom(p_board, new Vector3(337, 1461, 0), 0.5f);
        yield return new WaitForSeconds(1);

//		ShowGameObject (progressbar, true);
        // Showing of inactive badge
        ShowGameObject(b_Rothbart_inactive, true);
//		yield return new WaitForSeconds (0.2f);
        ShowGameObject(b_Speed_inactive, true);
//		yield return new WaitForSeconds (0.2f);
        ShowGameObject(b_Jeanbob_inactive, true);
//		yield return new WaitForSeconds (0.2f);
        ShowGameObject(b_Alise_inactive, true);
        yield return new WaitForSeconds(0.2f);

        title.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.4f);
        message.gameObject.SetActive(true);
        yield return new WaitForSeconds(0.3f);

        int controller = int.Parse(badge);
        string tempPB = "GameOverUI/Board/Progress bar/";
        string tmpStrB = "GameOverUI/Board/";
        GameObject tmp, tmpSB;

        Text t = null, tt = null;

        yield return new WaitForSeconds(0.5f);
        t = GameObject.Find("GameOverUI/Board/Timer/Time").GetComponent<Text>();
        tt = GameObject.Find("GameOverUI/Board/Timer/Text").GetComponent<Text>();
        tt.text = "Your time:";

        for (int i = 4; i >= controller; i--)
        {
            string b = tempPB + i;
            string _badge = "";
            tmp = GameObject.Find(b);

            //			ShowGameObject (tmp, true);

            if (i >= controller)
            {
                sfx.PlayOneShot(effectSFX, 1f);
                _badge = tmpStrB + i + "/Active";
                tmpSB = GameObject.Find(_badge);
                ShowGameObject(tmpSB, true);
            }
            else
            {
                _badge = tmpStrB + i + "/Inactive";
                tmpSB = GameObject.Find(_badge);
                ShowGameObject(tmpSB, true);
            }

            yield return new WaitForSeconds(0.4f);
        }

        string h = "", m = "", s = "";

        for (int i = 0; i <= 60; i++)
        {
            if (i <= hour && hour > 0) h = i + "h ";


            if (i <= second && minute < 1 && hour < 1)
            {
                if (second > 0)
                    s = i + " seconds";
                else
                    s = i + " second";
            }
            else
            {
                if (i <= minute && minute >= 0)
                {
                    if (minute == 60)
                        m = "0m";
                    else
                        m = i + "m ";
                }

                if (i <= second && minute >= 0)
                    s = i + "s";
            }

            t.text = h + m + s;

//			t.text = (hour >= 1) ? i + "h " + i + "m " + i + "s"  : (minute >= 1) ?  (i <= minute) ? i + "m " + i + "s"  : minute + "m " + i + "s" : (second > 1) ? i + " seconds" : i + " second";

            yield return new WaitForSeconds(0.001f);
//			Debug.Log ((hour > 1) ? i + "h " + i + "m " + i + "s"  : (minute >= 1) ?  (i <= minute) ? i + "m " + i + "s"  : minute + "m " + i + "s" : (second > 1) ? i + " seconds" : i + " second");
        }

        GameObject o = null;

        switch (controller)
        {
            case 1:
                pb_alise.GetComponent<Image>().sprite = Resources.LoadAll<Sprite>("puzzle/puzzle_badge")[0];
                o = pb_alise;
                break;

            case 2:
                pb_jbob.GetComponent<Image>().sprite = Resources.LoadAll<Sprite>("puzzle/puzzle_badge")[1];
                o = pb_jbob;
                break;

            case 3:
                pb_speed.GetComponent<Image>().sprite = Resources.LoadAll<Sprite>("puzzle/puzzle_badge")[3];
                o = pb_speed;
                break;

            case 4:
                pb_rothbart.GetComponent<Image>().sprite = Resources.LoadAll<Sprite>("puzzle/puzzle_badge")[2];
                o = pb_rothbart;
                break;
        }

        ShowGameObject(o, true);
//		iTween.MoveFrom (o, new Vector3(1513,-617,0),0.2f);
        yield return new WaitForSeconds(1);
        sfx.PlayOneShot(effectSFX, 1f);
        ShowGameObject(badgeParticle, true);
        yield return new WaitForSeconds(2.1f);

//		StartCoroutine ("WaitBeforeShowEffect");
//		yield return new WaitForSeconds (0.9f);

        buttonReplay.gameObject.SetActive(true);

        // next button
        nextLoading.SetActive(true);
        StartCoroutine(GetNextPuzzle(listQuestions[listQuestions.Count - 1].GetId(), badge));
    }

    /**
     * Get the next puzzle from the API
     */
    private IEnumerator GetNextPuzzle(float currentId, string badge)
    {
        string url = "";

        url = ApiUrl.PUZZLE_QUESTION_NEXT + "?current_id=" + currentId;

        WWW www = new WWW(url);
        yield return www;

        JSONObject jsonObject = new JSONObject(www.text);

        if (!jsonObject.IsNull)
            if (jsonObject.GetField("results").Count >= 1)
            {
                Models.Puzzles.Puzzle puzzle = new Models.Puzzles.Puzzle(jsonObject.GetField("results"));

                // get image
                string imgUrl = Application.persistentDataPath + "/" + puzzle.GetImageFilename();
                if (!File.Exists(imgUrl))
                {
                    WWW imgWWW = new WWW(WWW.UnEscapeURL(puzzle.GetImageUrl()));
                    yield return imgWWW;

                    File.WriteAllBytes(imgUrl, imgWWW.bytes);

                    yield return new WaitUntil(() => imgWWW.isDone);
                }

                // get audio
                string audioUrl = Application.persistentDataPath + "/" + puzzle.GetAudioFilename();
                if (!File.Exists(audioUrl))
                {
                    WWW audioWWW = new WWW(WWW.UnEscapeURL(puzzle.GetAudioUrl()));
                    yield return audioWWW;

                    File.WriteAllBytes(audioUrl, audioWWW.bytes);

                    yield return new WaitUntil(() => audioWWW.isDone);
                }

                // get answer audio
                string asnwerAudioUrl = Application.persistentDataPath + "/" + puzzle.GetAnswerAudioFilename();
                if (!File.Exists(asnwerAudioUrl))
                {
                    WWW answerAudioWWW = new WWW(WWW.UnEscapeURL(puzzle.GetAnswerAudioUrl()));
                    yield return answerAudioWWW;

                    File.WriteAllBytes(asnwerAudioUrl, answerAudioWWW.bytes);

                    yield return new WaitUntil(() => answerAudioWWW.isDone);
                }

                JSONObject question = new JSONObject();
                question.AddField("id", puzzle.GetId());
                question.AddField("is_unlock", false);
                question.AddField("badge", "0");
                question.AddField("grid", puzzle.GetGridRow() + "x" + puzzle.GetGridColumn());
                question.AddField("image_path", imgUrl);
                question.AddField("row", puzzle.GetGridRow());
                question.AddField("column", puzzle.GetGridColumn());
                question.AddField("first_badge_time", puzzle.GetFirstBadgeTime());
                question.AddField("second_badge_time", puzzle.GetSecondBadgeTime());
                question.AddField("third_badge_time", puzzle.GetThirdBadgeTime());
                question.AddField("fourth_badge_time", puzzle.GetFourthBadgeTime());
                question.AddField("answer", puzzle.GetAnswer());
                question.AddField("question", puzzle.GetQuestion());
                question.AddField("audio_path", audioUrl);
                question.AddField("first_badge_message", puzzle.GetFirstBadgeMessage());
                question.AddField("second_badge_message", puzzle.GetSecondBadgeMessage());
                question.AddField("third_badge_message", puzzle.GetThirdBadgeMessage());
                question.AddField("fourth_badge_message", puzzle.GetFourthBadgeMessage());
                question.AddField("answer_audio_path", asnwerAudioUrl);

                Question questionObj = new Question(question);
                listQuestions.Add(questionObj);

                string puzzleQuestionPath = Application.persistentDataPath + FileName.PUZZLE_QUESTIONS;

                BinaryFormatter bf = new BinaryFormatter();
                FileStream file = File.Open(puzzleQuestionPath, FileMode.Append);

                bf.Serialize(file, questionObj);
                file.Close();
                Debug.Log("total " + listQuestions.Count);
            }

        nextLoading.SetActive(false);
        if (listQuestions.Count - 1 <= imageIndex)
            buttonNext.gameObject.SetActive(false);
        else if (!listQuestions[imageIndex + 1].GetIsUnlock())
            buttonNext.gameObject.SetActive(false);
        else if (badge == "4")
            buttonNext.gameObject.SetActive(false);
        else
            buttonNext.gameObject.SetActive(true);
    }

    private void SaveAchievements()
    {
        PlayerPrefs.SetInt("puzzle_index_" + imageIndex, imageIndex);
        PlayerPrefs.SetInt("badge_puzzle_" + imageIndex, badge);

        Debug.Log(
            "Saved value in puzzle_index_" + imageIndex + " = " + PlayerPrefs.GetInt("puzzle_index_" + imageIndex));
        Debug.Log(
            "Saved value in badge_puzzle_" + imageIndex + " = " + PlayerPrefs.GetInt("badge_puzzle_" + imageIndex));
    }

    private IEnumerator FadeOut()
    {
        yield return new WaitForSeconds(1);

        winBackground.GetComponent<Image>().CrossFadeAlpha(0, 3f, true);
    }

    private IEnumerator WaitFor1()
    {
        yield return new WaitForSeconds(1);

        winBackground.transform.SetParent(master.transform, false);

        yield return new WaitForSeconds(1f);

        winBackground.GetComponent<Image>().CrossFadeAlpha(1, 3f, true);

        StartCoroutine("WaitBeforeShowEffect");

        ShowHintForImage();

        StartCoroutine(ShowAnswer());
    }

    private void GetPuzzleList()
    {
        // implement the checking of what puzzle are unlock or lock
        string puzzleQuestionPath = Application.persistentDataPath + FileName.PUZZLE_QUESTIONS;
        Question question;

        listQuestions = new List<Question>();
        // if the file already exist
        if (File.Exists(puzzleQuestionPath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(puzzleQuestionPath, FileMode.Open);

            while (file.Position != file.Length) listQuestions.Add((Question) bf.Deserialize(file));

            file.Close();
        }
    }

    private IEnumerator PuzzleWaitSeconds(float sec, int type)
    {
        yield return new WaitForSeconds(sec);

        if (type == 1) firstClick = null;

        if (type == 2) secondClick = null;
    }

    private IEnumerator PuzzleTo(string direction)
    {
//		Hashtable scale = new Hashtable ();
//		scale.Add ();
        GameObject parent = GameObject.Find("Monitor");
        GameObject obj = Instantiate(boardMonitor);
        obj.transform.parent = parent.transform;

        obj.GetComponent<Button>().enabled = false;

        ShowGameObject(boardMonitor, false);

        if (direction == "scale up")
        {
            iTween.MoveTo(obj, new Vector3(-2, 0, 0), 1f);
            iTween.ScaleTo(obj, new Vector3(0.3f, 0.3f, 0), 1f);

            yield return new WaitForSeconds(3);

            iTween.MoveTo(obj, new Vector3(-7, -4, 0), 1f);
            iTween.ScaleTo(obj, new Vector3(0, 0, 0), 1f);

            yield return new WaitForSeconds(0.5f);

            ShowGameObject(boardMonitor, true);
            Destroy(obj.gameObject);
        }
    }
}