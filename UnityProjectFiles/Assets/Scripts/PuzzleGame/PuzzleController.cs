﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

[RequireComponent(typeof(AudioSource))]
public class PuzzleController : MonoBehaviour {

	AudioSource source;

	AudioClip tapButtionAudio;

	Button closeButton;
	Button noButton;
	Button yesButton;

	Canvas closeModal;
	Canvas loadingModal;
	Canvas mainGameModal;
	//Canvas gameOverModal;

	GameObject confettiParticle;

	RectTransform container;

	int pageCount;
	
	void Start () {
		SetUI ();
	}
	
	void SetUI() {
		source = gameObject.AddComponent<AudioSource> ();

		closeModal = GameObject.Find ("ModalUI").GetComponent <Canvas> ();
		loadingModal = GameObject.Find ("ModalLoading").GetComponent<Canvas> ();
		mainGameModal = GameObject.Find ("MainGameUI").GetComponent<Canvas> ();
		//gameOverModal = GameObject.Find ("GameOverUI").GetComponent<Canvas> ();
		//gameSelection = GameObject.Find ("GameSelectionUI").GetComponent<Canvas> ();

		confettiParticle = GameObject.Find ("Confetti - Star PS");

		closeButton = GameObject.Find ("Panel/btnClose").GetComponent<Button> ();
		noButton = GameObject.Find ("ModalUI/Window/btnNo").GetComponent<Button> ();
		yesButton = GameObject.Find ("ModalUI/Window/btnYes").GetComponent<Button> ();

		tapButtionAudio = Resources.Load<AudioClip> ("SFX/tap_button");

		// Hide modals
		//ShowModal (gameOverModal, false);
		ShowModal (closeModal, false);
		ShowModal (loadingModal, false);
		ShowModal (confettiParticle, false);
		//ShowModal (mainGameModal, false); // gameSelection is up first

		// Add event listener to obj
		closeButton.onClick.AddListener(delegate { 
			ShowModal(closeModal, true); 
			PlaySFX(tapButtionAudio); 
		});

		noButton.onClick.AddListener (delegate { 
			ShowModal(closeModal, false); 
			PlaySFX(tapButtionAudio); 
		});

		yesButton.onClick.AddListener (delegate {
			OnLoadPlaytime();
			PlaySFX(tapButtionAudio);
		});

		//SelectImage ();
	}

	// Mark - User defined functions

	void ShowModal (GameObject obj, bool show) {
		obj.SetActive (show);
	}

	void ShowModal (Canvas obj, bool show) {
		obj.gameObject.SetActive (show);
	}

	void OnLoadPlaytime() {
		StartCoroutine (WaitBeforeExit());
	}

	IEnumerator WaitBeforeExit() {
		ShowModal (loadingModal, true);
		yield return new WaitForSeconds (1.5f);
		PlayerPrefs.SetInt ("toShowScreen",2);
		SceneManager.LoadScene("Main");
	}

	void PlaySFX(AudioClip clip) {
		if (source.isPlaying)
			source.Stop ();

		source.PlayOneShot (clip, 1f);
	}

	/**
	void SelectImage () {
		List<Button> arrayButton = new List<Button> ();
		Button[] imgButton = new Button[pageCount];
		for (int i = 0; i < pageCount; i++) {
			string puzzle_str = "GameSelectionUI/Selection/Container/Puzzle_" + i + "/Image";
			imgButton[i] = GameObject.Find (puzzle_str).GetComponent<Button> ();

			string str = imgButton[i].image.sprite.name;
			int imageIndex = i;

			arrayButton.Add (imgButton[i]);

			arrayButton[i].onClick.AddListener (delegate {
				Debug.Log ("You clicked here " + str);
				//ShowModal (gameSelection, false);
				ShowModal (mainGameModal, true);
				PlayerPrefs.SetInt("imageIndex",imageIndex);
			});
		}
	} **/

}
