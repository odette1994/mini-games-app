﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class CardFlipBackBehavior : StateMachineBehaviour
{
	public Button button;
	public Sprite image;

	private Button clone;

	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		
	}
	override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		
	}

	override public void OnStateUpdate (Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
	{
		if (button.transform.eulerAngles.y <= 90) {
			button.image.sprite = image;
		}

	}
}


