﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ScrollableList : MonoBehaviour
{
    public GameObject itemPrefab;
    public int itemCount = 10, columnCount = 1;
	public int cellDistance;

    void Start()
    {
        RectTransform rowRectTransform = itemPrefab.GetComponent<RectTransform>();
        RectTransform containerRectTransform = gameObject.GetComponent<RectTransform>();

        //calculate the width and height of each child item.
        float width = containerRectTransform.rect.width / columnCount;
        float ratio = width / rowRectTransform.rect.width;
        float height = rowRectTransform.rect.height * ratio;
        //int rowCount = itemCount / columnCount;
        
		Debug.Log ("containerRectTransform.offsetMin.y " + containerRectTransform.offsetMin.y);
		Debug.Log ("containerRectTransform.offsetMax.y " + containerRectTransform.offsetMax.y);

		float scrollWid = itemCount * containerRectTransform.rect.width;
			
		containerRectTransform.sizeDelta = new Vector2 (scrollWid, height);
			
        int j = 0;
        for (int i = 0; i < itemCount; i++)
        {
            //this is used instead of a double for loop because itemCount may not fit perfectly into the rows/columns
            if (i % columnCount == 0)
                j++;

            //create a new item, name it, and set the parent
            GameObject newItem = Instantiate(itemPrefab) as GameObject;
            newItem.name = gameObject.name + " item at (" + i + "," + j + ")";
            newItem.transform.parent = gameObject.transform;

            //move and size the new item
            RectTransform rectTransform = newItem.GetComponent<RectTransform>();
			containerRectTransform.localPosition = new Vector2 (i * containerRectTransform.rect.width, 0);

			float x = (-containerRectTransform.rect.width / 2 + width * i) + cellDistance;
			float y = containerRectTransform.rect.height / 2 - height;
			rectTransform.offsetMin = new Vector2(x, y);

			x = (rectTransform.offsetMin.x + width) - cellDistance;
            y = rectTransform.offsetMin.y + height;
            rectTransform.offsetMax = new Vector2(x, y);
        }


    }

}
